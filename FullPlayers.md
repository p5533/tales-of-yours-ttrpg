
![Title](./img/TOYTItle.jpg)
# <a name="title"></a> ***Tales of Yours***

Player's Guide
Full Version


(Version 2)
The Core Book I of VI

By Harmony “lil” Petersen



A game about life
From the mundane to the heroic
From the realistic to the fantastic

# <a name="contents"></a> TABLE OF CONTENTS

- [Title](#title)
- [Contents](#contents)
- [Credits](#credits)
- [Preface](#preface)
- [Intro](#intro)
- [Abilities](#abilities)
- [Aspects](#aspects)
- [Posessions](#posessions)
- [FAQ](#faq)
- [Glossary](#glossary)

# <a name="credits"></a> CREDITS

(All art rights reserved by artists)
Lead Artists: Wil Whalen, Jeff Zhang
Guest Artists: Hannah Halloway
Weapon Photos supplied by museumreplicas.com (used with permission, modified by Harmony Petersen)


Co-authors/editors: Rodney Petersen, Myra Voshall, Leigh Anne Petersen, Elizabeth Michelle Petersen

Playtesters: Ben Anderson, Kris Brown, Max Chao, Victor Paul Haskins, Emily “Danni” Harris, Jason Heitzman, Matt Hicks, Elizabeth "Katame" Petersen, Hannah “Hanners” Holloway, Harmony Ishikawa, Tony Srimongkolkul, Dyre Steele, David “Professor Science” Troeh, Ashley “Raven” Wheeler, Jason Gevargizian


Help via Random Discussion on Good & Evil: Adam "Zippomage" Holler

Additional Thanks to:
All the open-source creators who made libreoffice (the program this book was written with) and the earlier versions of open office.

All the GMs, creative players, and designers who will make additional content in the system, 
improving it for others.



# <a name="preface"></a>Preface
	 It was a dark and stormy night, and cliches were in the air. A group of seven vaguely ordinary students had gathered around the table, the only light a flickering florescent bulb overhead. They had gathered to create a tale of majesty and glory, in the footsteps of many before them. They were to embark on a journey into an alternate world of the mind, where fantasy became tangible, and adventure lay before them. 

	There was a leader, who would guide the world and its events, and others who would take on the personaes of the great heroes of the world. As they stood around the table, the leader spoke of a great war that was upon the land. He had gathered maps and books that detailed the exploits of the conflict, the tactics that would be used by the enemy and their forces. He had decided the direction the world would take. He knew how any attack done by the heroes would be responded to. He had battle plans and models and statistics on the enemies mightiest warriors. He knew that no matter how many villains the heroes dispatched with their weapons, there would be always be a greater one waiting on the horizon. 

	Announcing he was ready, the leader waited for the others to call their roles for the world. 
	“Elven Archer” said one older guy, well versed in the rules of combat. He expected his character would take down as many enemies as he had arrows until the incoming hordes ended his days.

	“Dwarvern Axe Fighter” said a yet another grizzled veteran, who had fought as many different roles. He would swing until his muscles gave out and he could swing his axe no more, and the uncountable enemy beat him.

	Then the youngest and least experienced looked up. “An upcoming war against an unstoppable enemy... to stay and fight is dumb. My character will be a colonist. We will abandon this doomed land, and start a new home on a distant more defensible shore. Our new civilizations shall prepare. Should the enemy ever arrive there, we will be ready for them when that time comes. If it does not come, we will send word of hope home, with any new research.”

	The leader went pale. He had prepared a war with insurmountable odds, yet he was not prepared for the possibility that the heroes might choose to not fight. He was about to state that this was not within the capability of the rules, there was nothing about building community or technology. 

	However, before he could speak, the fourth player, inspired, spoke, “And I will play a pacifist and a skilled diplomat. The enemy obviously wants land or some other resource. Or it could be religion. An unstoppable enemy could, actually, make a powerful ally. If we allowed their religion in our countries boundaries, or if we removed borders for free trade to the resources they want, or just agreed that the citizens of either land could travel freely into the lands of the other, we may be able to stop the purpose of the war.” The leader paled further as in his system, rules to convince to people were simplistic. It could break the game...

	The fifth followed, “I will be a group of small children who work together as a single unit, to gather information on what is happening, to share with the leaders of the country to help them preserve their people during the peace talks.” The Leader blanched... playing a whole organization? Unheard of...

	The sixth added, “My character will be a prince, and seduce the enemy princess. It will be harder for the war to continue if the kings would have to fight their own families.” Romance? The leader had no rules for that...

	The leader slumped, the old rules and his plans couldn't handle this. He realized it was time for a better system.


Welcome to the TOY (Tales Of Yours) Player's Guide. This is the initial rules & guide book for players of the TOY rpg system.

**Obligatory “What is an RPG?” Section** 

*(hint: skip to “What is TOY” if you're an experienced tabletop RPG gamer)* 

Chances are, if you're picking up this book, you know what an RPG is, but if not, I'll fill you in. RPG stands for "Role Playing Game". Most people understand playing games. It's the "Role-Playing" part that may be confusing for newer people. It's a role like when an actor is on stage. They act out someone other then themselves in a world not their own. For example, people watching the play “Romeo and Juliet,” know that the people they're watching are actors and not actually the characters. When, in the play, Mercutio is mortally wounded, people don't pull out their cell phones and call an ambulance, even if they're attached and get teary-eyed.

(Side-note: And if, by chance, you do think the actors actually are the characters in plays and you call the police when one character harms another during the show, put down this book, step away, and avoid any TV, radio, or books, stay away from any sharp objects, and call up your local emergency hotlines and talk to them about the way you feel. They'll probably be by shortly with a pretty new white “hug-myself” coat for you.)

Similar to the role in acting, Role-Playing is when a person, as part of a role playing game, takes on a persona - a character they act out - and move them through an imaginary world. The primary difference between role playing and acting is that in role-playing, there is no script. You have the world presented by the GM (a person who knows more of the rules and acts as a referee and storyteller) and the knowledge of the character; then you decide what that character would do in the situation. You move the story along with your character in a way that makes sense to you, and the character becomes an extension of your vision.

**What is TOY, and what makes it different than other RPGs?**

*(hint: skip to “So why should I buy this game” if you're new to RPGs.)*
	
The way you play fits how much you know about the game, and it is easy to have beginners and experts playing together seamlessly, with no real headache for the GM. 

A major trait is characters are generally only as complex as you want to get. Although a common mistake for gamers switching over to this system to try and min-max a standard character concept and end up finding their complexity can just keep going and take forever. Instead, come up with your character idea and try to make it in the rules.

A character can specialize on one thing and effectively be a one-trick pony, or can be a maverick and jack-of-all-trades able to do an insane number of things. Your all-around abilities (Being, Body, Mind, Speed, Finesse, Might, Toughness, IQ, Creativity, Memory, Sanity) are decided automatically from the few (or many) Skill abilities you choose. You only need to worry only about your own character to understand how to play. Also, there's two versions of the game. A light easy-to-learn basic game, and a more robust full game. The Basic game is easy for jumping people in quickly to the new system and getting a few basic ideas across, and introducing new players or doing very short games, with the Full game taking more time but creating more balance and allowing more complexity. In a number of ways, for how they play, the Full game could be consider a series of expansions to the Basic game with a few minor changes.

A few design things to keep TOY easier:
- Higher rolls fairly consistantly better.
- The simpler options is always the default (for example, ambidexterity is the default, but having to separate your hands between dominant and secondary is optional.)
- Character traits are all bought using standard character points or gained in gameplay. More complex systems (such as destiny points), are emergent rules that only come about when you buy related things, meaning you only have to learn more complicated rules when you feel ready.
- Money is generic (handled in “Day's Wages” or DW), skipping past complexities like tracking currency types, weight of gold, converting between countries, etc., which lets it easily be adjusted and adapted for different times of history.
- System is designed to be easily expandable, letting you make your own content and rules easily.
- There's generic upgrades to abilities in the Full Game (known as aspects). This means you can design a new skill, and already have many different ways to improve and upgrade it.

Also, the point system of TOY is inherently dynamic. You may have a set number of point character point to start, but without any strange leveling system, it allows fluid adjustments of characters. You may spend or gain character points on various skills and abilities, but circumstances may also adjust your points by storyline beyond your control (such as losing an arm may drop your total points, or an ancient artifact may grant you a new ability), adjusting your point total accordingly. The advantage of this is that it makes it possible for characters to 'slip backwards' as well as constantly 'climb the mountain of success'. Stories that allow both success and failure are usually much more interesting then what has been termed by some as 'level grind'.

Another goal of Tales Of Yours is “modularity”, meaning that the rules can easily be moved from one type of game to another, but allow the GM to adapt the game's rules to fit the feeling he or she is going for. TOY can be used for several settings, including hard-core science fiction, fantasy, a modern-world setting, the wild west, high school drama, superheroes, or many others - especially as expansion books designed specifically for additional settings come out.  This also means the rules are designed to adapt to other kinds of situations as well, such as a traditional gaming group in person with a group of friends with a GM, playing in online chat rooms and forums, a GM-less competition between two players (or armies) struggling for dominance, intense high-level tournaments, or playing in the back seat of a car during a long road trip (although we don't suggest including the driver, as it may be distracting, and we would prefer the driver focus on keeping you alive, rather than keeping alive his half-faerie catgirl!) The thing I believe, as I was creating this game, is that if you bought it, you should be able to play it how you want.

And yet another design concept in Tales Of Yours is “density.” I believe you should get your time’s worth, and I don't believe hours of readin is worth twenty-plus pages of minimal variation on combat damage, the only differences being area, damage amounts, and weapons used. As such, abilities can be leveled up, and those levels adjust formulas. Like an attack ability (that lets you choose your weapon) at level X may let you hit something in the face for 'X', and an aspect associated with it may let you increase the number of creatures you hit at once. Doing things this way lets you do with half a page what other gaming systems may take a third of the book to do. TOY may have fewer pages in it's core books than others, but those fewer pages are designed to let you do so much more. In other words, I aimed for quality, not quantity of pages. 

An exception to the density-based design, there are a few times that there is redundant information, (such as the information in the pregenerated characters section of the master's guide book could easily be gotten from hunting through the abilities section in this book and doing all the math) but this is done with the express purpose of making it so you can get to gaming faster, and make things simpler for you. (In playtesting, this difference made it so that a character could be created in as short as 10 seconds when otherwise 'speed building' could easily take much longer. In this case, I suspected the extra pages would be worth it to you.) This is because in building I also had the goal of making it fast for what should be simple, often done things.

Also, unlike most roleplaying systems, Tales Of Yours is a life simulation system as well. The majority of roleplaying games focus on one thing to the exclusion of most (if not all) things else: Combat. Granted, TOY has rules for combat, and can handle an action movie scenario just as well (if not better) than the next roleplaying game. However, TOY has rules for everything from romance, to mixing chemicals, to running an empire, to social manipulation, to breeding creatures to make the best hybrid, to cooking. TOY can just as easily go from simulating an action movie to running a competitive cooking show or a courtroom scene.

Also, unlike some certain other systems that shall go unnamed, the line between good and evil isn't drawn with a cartoonish line that takes deep an interesting personalities and turns them into a monochromatic boxes. When you choose your morality, you choose your goals, your dreams, and what YOU see as right and wrong. Characters judge other characters by their moral choices, and react according to them, instead of just acting on a 'good vs evil' scale. Everyone has their own values they live up to, whether it be family, country, protecting life, their religion, profit, or so much more. Characters have definite agendas, goals, and will often obviously take sides, but those sides are based on ideals and loyalties. Because in the battle of good vs evil, more often than not, both sides consider themselves the good side, and the other side the evil side.


As an added bonus (which comes only from buying the book, rather than just browsing the wiki), there is extra text explaining things throughout the book. Some giving descriptive story-like accounts of how things can be used, others give roleplaying tips on how characters with certain traits are likely to act, and yet others tell you the gritty 'rules behind the rules' in the TMI sections, which let you make stats and information that meet the standards of all of our other stuff in that section, instead of being forced to arbitrarily assign values. Not to mention there is also art to help illustrate various points.

**So why should I game?**
Well, besides the fact that roleplaying games are fun. It's kind of like reading a book with other people where you finally get the characters to make the decisions you think they should make. 

- Tired of all the people who go into the haunted house splitting up all the time? 
- Annoyed by the hysteric parent who is more worried about their child's backtalking than the impending raptor invasion? 
- Frustrated that the bankrobbers just drove faster when ditching the car around a corner and hopping in a taxi would have saved them? 
- Or are you livid when the evil arch-nemisis has a tendency to monologue and everyone just sits around and lets the villain finish? 
- 
This gives you a chance to experiment how you know things should have been done in practically any story. It's great for taking your favorite books, stories, movies, and more, and seeing how things might have turned out different, or how they might continue. It's great for 'what-if's.

On top of that, It's been shown that playing roleplaying games might make you smarter (similar to chess) but with a few crucial differences. Chess, although it often requires more analytical skills, isn't as real-world applicable as a roleplaying game. 

Not only are roleplaying games great for 'what-if' situations for entertainment, but it's also good for a light simulation of 'what-ifs' for real life, both plausible and fantastic. What would you do in case of a zombie apocalypse or alien invasion? What strategies could you come up with to win an election? How might you defend a framed person in court? What might be the best pickup line to use when in a seedy tavern you haven't been in before? Would you survive as infantry in World War II? Exactly how much wood would a wood chuck chuck if a woodchuck could chuck wood?

 Although playing the game may not give exact answers, it can help you think about it, along with others, about the possibilities. And that experience can help you out in your story, your real life story, to help give you that edge to be more than just another face in the crowd. 

# <a name="intro"></a>TALES OF YOURS - The Full Game
> *Okay, my character, a shady looking lady with a limp walked into the pawn shop, and sets her package down on the counter.* 

> After you do that, the establishment's manager looked at her inquisitively and speaks... 

>“You've been coming in pawning stuff a bit. Never asked you your story. Probably should. What's your story?”

>*“So right, I was born an orphan and didn't know my mother, and only discovered who my father was from old newspapers.”*

>“Here I thought I overheard you in the bar yesterday say to someone you were born on Halloween during a full moon, and that there may or may not have been a goat sacrificed at your birth?”

>*“That's only the story when I'm picking up guys in the bar. They love it when a girl's mysterious.”*

>“But I thought you were a lesbian?”

>*“That's only when I'm trying to get a job. Flirting with HR usually works for me.”*

>“But you don't work...”

>*“Only according to my income tax records. You'd be surprised how easy it is to manipulate the books. Although, I don't consider it work, I prefer to think of it as charity.”*
>“Stealing is charity?”

>*“Hey, it's not stealing, it's repossession.”*

>“If you 'repossess' from someone, Isn’t that work? Less work than what went into the getting the original owners it, though.”

>“*Yea, but whose work? They stiff their workers. Here, the workers have a chance to get what they worked for and was stolen by their boss on discount. I'm practically a charity.”*
The pawn manager chuckled, “So what... you donate the profits from what you steal to the poor or something?”

>*“Heck no. A proper repo-man gets paid for his work. Since I have no employer, I keep the profits for myself, like any upstanding small business owner.”*

>“Aren't you afraid of getting caught?”

>*“Heck no. From my first couple repossessions, I put back enough for an awesome lawyer. I get caught once, I use that, and I retire.”*

>“Don't you ever feel guilty about taking stuff that doesn't belong to you?”

>*“No, never. They started it. I used to run a flower stand, then 'Super-mart' moved in with half price discount flowers and there went my honest living out the window. I had to give my pet goldfish, Guppy, to the shelter. He was my only friend.”*

>“You realize shelters don't keep goldfish, right?”

>*“...Huh...”*

>“So, why'd you called it a war? You consider yourself a soldier then?”

>*“Freedom fighter. The rich are oppressive, and the poor are oppressed. The rich hide behind their faceless corporations, and do things that hurt more people than any war, and then act all innocent. Hurtin' peoples' goldfish.”*

>“You do realize most of this stuff you're pawning off on me will probably be replaced by their insurance, right? Basically, you're just getting them new stuff.”

>*“...Damn, I didn't think about that. I can't even beat them this way?”*

>“ 'Fraid not. You want cash, I assume?”

>*“Yea... wait... no.”*

>“What do you want?”

>*“Give me a rifle, with a night vision scope.”*

>“You know if you shoot them, their wills will just pass it all on to their even more spoiled kids, who are, on average, even more morally inept.”

>*“...You really know how to ruin a girl's day, don't you? Nevermind on the rifle then. So, what’s your take? How can I fight back?”*

>“Stop using money altogether? That is the source of their power. I'd be more than glad to take it all off your hands.”

>*“Naw... but I'm going to give thought to more options. There has to be something that can be done to fight back.”*

>“You know... the look on your face, you were joking a lot before, but now… you're serious? You actually want to do something?”

>*“Yea, I've got personal reasons.”*

>“You know, I'm all of a sudden very worried about what would happen if all the other thieves started thinking like you... yet oddly hopeful for what would happen if they did.”

>*“Glad to be the one to reveal some people are deeper than some stereotype.”*

>The pawn manager thought for a moment, and then looked at her, serious, “…That goldfish was a cope wasn’t it? A cover story for what really hurt ya?”

>*“...yea, prefer not to think about it for real. Brother. Died because corporate didn’t want to ‘waste’ fifteen dollars on a safety rail at the meat packing plant. They tried to hide it for weeks… I still feel sick when I think about the dinners I had between the event and learning about it, can’t help but wonder…” she shook her head, chasing away the thought, “...they got of scott free due to their fancy lawyers. I’m going to do something someday, I just need to know how...”*

> Wait, did you really build that into your character?

> *Yea, I took the Chaotic Ideology Morality aspect and everything.* 

If you're looking at this part of the book, there's a good chance it's because you've played the basic game, and want to get on to the full game (either that, or you're just reading through the book randomly, flipping through pages, or ignoring the basic game outright.) Regardless of the reason you're here, welcome. I hope you stay, there might be cookies baked for you later.

The basic game is all fine and dandy if you want an easy and not-that-hard of a game, one that's more story without much in the way of rules to tell you what to do. However, for many players, they want a robust game, able to handle many difficult situations and questions, and solve disputes while allowing characters many options to choose from. Because that's what you'll get from here on out. It should be noted that understanding the basic game is important to playing the full game, however. And if you haven't read it, you should, because many of the same rules apply, and it serves as a good primer to some of the core concepts.

## Making Full Characters
> “So, which end of this annoying piece of uskit'r is supposed to be pointed at the enemy?” 
~ Garwid, Arik Clan investigator of alien weapons - Last words

In most Tabletop RPGs, making characters is generally considered the hardest part for the players (but frequently quite fun!) This step by step guide should make it easier. It should be noted this isn't the ONLY way to make characters, but just a suggestion.

    Step 1: Come up with an idea for a character     This character should preferably one that fits in a setting the GM will describe for you.
    
    Step 2: Ask how many character points you have. The GM will assign this. A typical value for a starting character is 256 points. A more "heroic" start is often around 500. A “rise of an everyman” campaign may even start with 0.
    
    Step 3: Choose the character's background        This includes stuff like family, ethnicity, nationality, etc.
    
    Step 4: Choose Skills – 4th/5th Tier Abilities       You aren't buying yet. Just list the abilities your character would have. One one of the following pages is a table of the default 4th/5th tier abilities (also known as broad and specialized skills) a character is most likely to have. (You can make your own, but there are less rules to support them, and making your own requires permission from the GM.) It's easy to go overboard, so take it easy. A good rule to keep in mind is a 256 point character, with aspects equal to their ability levels, can have 4 broad skills at level 4, and 8 specialized skills at level 2.
    
    Step 5: Look over the aspects, and choose which you like. If you're sticking with the "default build", this means you'll be getting ability skill aspects up to level four for four of your abilities, and up to level 2 of another eight. These make your character better, so pay attention to what they do. Generally, most of your aspects will be generic aspects (these start off with [Ability] Skill) that will be bought for your specific ability, and a few levels with a choice between the generic aspects and an aspect specific to the ability. Since in the aspects section, everything is alphabetized, and brackets come before letters, all the generic aspects are listed first, and afterwards, you can find aspects tied to your ability alphabetically.

    There may also be other aspects you desire... not all aspects are tied to abilities. And some cost negative points (meaning they give you character points!) Which you can then spend on other aspects or abilities.
    
    Step 6: Assign your points
       Now that you have a plan for your aspects and abilities, go ahead and spend the points and buy levels. Broad skill abilities cost 6 points a level, and specialized skill abilities cost 3 points a level. Specialized skills can never have more levels than any of the abilities above them, but also get a bonus from their highest-leveled higher ability.
       
    Step 7: Assign 3rd and 2nd Tier Ability Levels This is identical to steps 6, 7, and 8 of Making Basic Characters.
    
    Step 8: Deal with your money
        You start out with nothing... unless you have aspects that say otherwise. (Such as the steady income aspects, generic ability aspect that lets you get hired, etc.) If your character has family, the GM may let you make social rolls to bum some money off of them for supplies.
        a: Money is calculated in "days wages". This is how much an average person in the culture makes in a day. (So gold, dollars, pesos, barter, or whatever system the culture uses isn't really part of the issue. If you must know, in modern times in first-world countries, at the time of writing, the average daily wage is roughly $80/€60)
        b: Buy stuff. There's a section for possessions you can buy. You can buy stuff not in that section if the GM allows, and the GM is willing to design stats for it and price it. 
    Step 9: Calculate other aspect modifiers
        Some aspects give permanent social modifiers, or give you allies who follow you around. Those aspects give the rules for those abilities. 

## Differences vs Basic Game
1. **Character Points**
Character points are spent to create a character instead of levels. An ability level is “worth” 6 character points now. Not only that, but they can be used to buy other things for the character too (mostly aspects and healing being damage); some more expensive than levels, some cheaper, but it allows your characters to be made noticeably more complex and fleshed out. Also, some GM's will run a hybrid system where you're still given levels instead of character points, but you get all associated ability skill aspects for each level you buy, and a small "allowance" of skill points each level to buy non-skill-related aspects.
2. **Aspects**
Other than spending points instead of levels, probably the biggest change is aspects. Most ability-related aspects, it should be noted, have a level to them. They require an aspect for that ability of the previous level in order to buy them. If you don't see the preceding ability, that more than likely means you need to buy the generic ability aspect for that ability. They're recognizable by having names like, [Ability] Skill 3 - blah blah blah. When you write these on your character sheet, you replace [Ability] with the ability your buying it for. So if you're buying it for Dance, you'd write down Dance Skill 3 - blah blah blah. 
Word of advice, when buying a bunch of these, it saves space to instead of writing "Dance Skill 1 - blah blah blah, Dance Skill 2 - yadda yadda yadda, Dance Skill 3 - walla walla bing bang", instead just write "Generic Dance Skills 1 - 3", and you save a lot of space. However, we suggest even if you're just taking them just because you need them to get something else, that you still read them. Even if they're generic, they still let you use the abilities in new and different ways. Such as [Ability] Skill 5 lets you roll an extra dice, and [Ability] Skill 7 lets you actually earn money using the ability.
Actions
Many aspects will grant actions. Actions are very specific things you can do. These are detailed more in the aspects section.
3. **Intense Time**
Another change is that the full game includes a mechanic called 'Intense Time'. In the basic game, a person just called out what they did and then rolled for it, but when time is of the essence, how long it takes someone to do something, or the order in which people try to do things can become very important, people can't just take their time to do whatever they want, and so intense time has turn orders, which controls what actions happen when, instead of just leaving it up to the GM and players. 

Examples of times in which the GM may call for intense time is when the players are in a life or death situation (such as defusing a bomb, running from an explosion, or engaging in combat), when actions within seconds can change the course of events (such as the last seconds on a clock during a championship match, the final timed test for a witch’s school, or counterhacking hackers from an enemy nation trying to get the database of spies). 

During intense time, time is first broken down into turns. All characters get at least one action during a turn by default (although there are multiple ways to increase or decrease the actions a character has in a turn). However, other than when people contest them (or situations require they check them), players don't get to use their abilities during intense time. This is because they don't get the time to focus and think about their ability that they normally would, no chance to go back to books to answer missing questions, no way to attempt trial and error. The level of understanding to act in such crucial moments is a completely different type of understanding of the topic. 
Most abilities will have aspects associated with them (in fact, the name of those aspects often begin with the name of those abilities, making them easier to find in the book). Purchasing these aspects allows for more nuanced use of abilities. Commonly, these aspects will often have associated actions for use during intense time.
![Fatigue](./img/Fatigue.jpg)

4. **Fatigue**
Most actions (and some aspects without actions) are fueled by the abilities. As abilities are used, they become fatigued. Each ability can be fatigued a number of times equal to its level, and no farther. (Although some aspects make exception to this... albeit at high cost in other areas.) After an ability is fully fatigued, neither it or its lower abilities be used until fatigue is restored. (Usually by a full night's rest.) Any attempt to use them after this point results in an auto-fail.
![Nutshell](./img/nutshell.png)

## Quick Start Character Builds
The typical starting character point around 256 character points (231 to 259). Here are few quick ways to spread the points: 

**This is a more generalist spread:** 

| | |
| :---: | ---: |
| **Skill Points** | 256 |
| **Skill Levels** | 4 at level 4, 8 at level 2 |
| **Skill Aspects**| 1 for each skill at each level |
| **Specializations**| None |

**This is a build focused on one skill:** 

| | |
| :---: | ---: |
| **Skill Points** | 255 |
| **Skills** | 1 |
| **Skill Levels** | 17 |
| **Skill Aspects** | 1 for each level |
| **Specializations** | None |

**This is a build focused on one specialization with one skill parent:** 

| | |
| :---: | ---: |
| **Skill Points** | 255 |
| **Skill Levels** | 15 |
| **Skill Aspects** | None |
| **Specializations** | 1 |
| **Specialization Levels** | 15 |
| **Specialization Aspects** | 1 for each level |

**This is a build focused on one specialization with two skill parents:** 

| | |
| :---: | ---: |
| **Skill Points** | 231 |
| **Skill Levels** | 11 |
| **Skill Aspects** | None |
| **Specializations** | 1 |
| **Specialization Levels** | 11 |
| **Specialization Aspects** | 1 for each level |

**This is a build focused on one specialization with three skill parents:** 

| | |
| :---: | ---: |
| **Skill Points**| 234 |
| **Skills** | 3 |
| **Skill Levels** | 9 |
| **Skill Aspects** | None |
| **Specializations** | 1 |
| **Specialization Levels** | 9 |
| **Specialization Aspects** | 1 for each level |

**This is a build focused on one specialization with a talent (3rd tier) parent:** 

| | |
| :---: | ---: |
| **Skill Points** | 259 |
| **Skills** | 2 |
| **Skill Levels** | One at level 7, one at level 28 |
| **Skill Aspects** | None |
| **Specializations** | 1 |
| **Specialization Levels** | 7 |
| **Specialization Aspects** | 1 for each level |

**This is a build focuses on the politics 6th Tier specialization:** 

| | |
| :---: | ---: |
| **Skill Points** | 237 |
| **Skills** | 2 (Argue, Conversation) |
| **Skill Levels** | Argue 24, Conversation 6 |
| **Skill Aspects** | None |
| **Specializations** | 2 (Inspire & Politics) |
| **Specialization Levels** | 6 |
| **Specialization Aspects** | 1 for each level in Politics |

**This is a build focuses on the romance 6th Tier specialization:** 

| | |
| :---: | ---: |
| **Skill Points** | 237 |
| **Skills** | 3 (Wellness, Conversation, & Other Finesse Skill) |
| **Skill Levels** | Wellness 5, Conversation 5, Other Finesse Skill 20| 
| **Skill Aspects** | None |
| **Specializations** | 2 (Beauty and Romance) |
| **Specialization Levels** | 5 |
| **Specialization Aspects** | 1 for each level in Romance |


Example Character Sheets:
![Full Character Example](./img/FullCharExample1.png)

1. This is the character background portion of the character sheet, and should generally be filled out first. It gives a brief summary of some of the vitals of the character, and most can be chosen however you like. It should be noted that there are aspects for family, but the generally are only used for close family. If family doesn't have much impact on the story, it doesn't use an aspect.
2. In many campaigns, your ethnicity will always be a subcategory of human, and human doesn't need to be noted. However, in campaigns with lots of different species, you will choose your species as part of your ethnicity.
3. Character points - the full game uses character points for purchasing most things. This part shows how much you've spent out of how much you've been given to spend. 
4. Although you can create your own skill abilities if you want to, most abilities you have will likely be pulled from the list of abilities in the book. They cost 6 points to buy one level.
5. When you buy levels in abilities, those abilities have higher abilities. You then designate how much of the level you want to go to each of its higher abilities.
6. Specialized skills can be bought as well. They cost half as many points to level up, but don't boost any higher abilities, but instead are limited in their max level by their higher abilities. (A specialized skill's higher abilities are usually other skill abilities.)
7. All abilities have the ability to be fatigued up to the level in the ability. This allows the ability to be used in special ways. When you've maxed out your fatigue in the ability, however, you can no longer use it until you recover from the fatigue (generally by resting or eating.)
8. You can take negatives in broad skills (but not specialized skills).  Since their bonus to higher tiered abilities is rounded down, this hurts higher abilities more than adding more helps. On the other hand, it does give you extra points to boost what you may feel is an important ability, or give you more points to get aspects you feel you need.
9. Higher abilities get thier levels from their lower abilities. (The broad skills that have them marked as higher abilities). Their level is 1/4th the combined levels designated to them from the lower abilities.
10. Since it's 1/4 the lower abilities, there's often a little left over. Some people chose to marke this as a decimal place, others choose to use the little circles next to the higher abilities to show how many levels are marked towards it that haven't boosted its level. Either is fine, and these values are used as tie breakers. However, be careful using decimals, that you don't include the decimal in your total when calculating Ability Tiers 1 and 2.
11. In addition to the normal ways abilities can be fatigued, 1-3rd tier abilities have special ways to be fatigued that add noticeable effect to gameplay. (Short version: Speed and IQ can be fatigued to gain extra actions. Finesse and Creativity can be fatigued to get re-rolls, Might and Memory can be fatigued to get bonuses, and Toughness and Sanity can be fatigued to reduce damage. Body can do what Speed, Finesse, Might, and Toughness can do. Mind can do what IQ, Creativity, Memory, and Sanity can do. And Being can do what Body and Mind can do. See the end of the Differences vs Basic Game section for more details.)
12. Body and Mind are each equal to 1/4th their lower abilities. 
13. Body, Mind, and Being each can take damage. If you ever reach 100% damage in either Body or Mind, you become unconscious. Also, there is an optional rule that the GM may require a percentile roll whenever you attempt an action, and that roll must pass the amount of damage you have, otherwise your injuries keep you from doing whatever you're attempting. Unless there is anything about your character that notes otherwise, Body and Mind damage heal at 10% per night.  When Being reaches 100% damage, however, the character dies. Being damage does not heal naturally, and must be bought off with character points. (A common way to do this is take phobias, damaged appearance, or other similar aspect negatives to reflect the way your character took the damage in the first place, and buy off the damage.)
14. Being is the highest tier ability. After this, their aren't any higher.
15. Aspects are special things your character can do. The numbers here are a quick reference to how much ability skill aspects cost as a combined total to reach each level (the cost of that ability skill aspect plus every ability skill aspect prerequisite.)
16. Many Aspects directly relate to specific ability skills. There are many ways to note this, such as drawing lines from the ability skill list to the aspect list to show relation between aspects, writing in the name of the ability skill, putting notes in from a broad range of skills and how you selected them, etc.
17. Some aspects have no related ability skill. They just get written in, simple as that.
18. Starting wealth, Salary, and Steady income is determined by the aspects you have and through gameplay.
19. Using your character's value, you can buy possessions for your character. (See the possessions chapter for specifics. If you want to buy something not in the chapter, the TMI portion in possessions gives a way to convert a real-world item's cost into game cost.)
20. Defensive and Assertive social & moral modifiers adjust your character's social actions when dealing with specific groups or issues. Theses can be given both through gameplay or through aspects.
21. Setting up your character's daily routine and how long it takes can be important. It determines how your character prepares, vs what they slack off on. You can estimate the time required for your routine, but note that some aspects in the future and some possessions may have certain time requirements you have to take into account.

## The Generic "Everyman" Character
Tales of Yours has a unique concept among Tabletop RPGs, and that’s “The Generic Character”. While most systems are difficult to run without going through the Character-making process first, Tales Of Yours is completely playable with a blank character sheet. 

The numbers and values on the character sheet how your character deviates from the normal. In other words, the numbers show how they're different from the generic character. In fact, some GMs will start their games without character creation, having them develop their characters through gameplay (for those who find character creation tedious, this can be an exciting way to develop a character! I personally refer to this as “an everyman” campaign.)

So in other words, every **0** (or empty space) on your character sheet means “the generic value”. 

So if you have a 0 in resolution, you can still use the ability (unless it says it requires training).

If the campaign is set in modern time, you could assume the average character doesn't stand out athletically, but isn't handicapped or overly weak in some way. They aren't very intelligent but not horrible (likely pulled average grades in school). They likely do unmemorable jobs that anyone could do but are generally capable of doing all of those kinds of jobs.

In other words, the blank sheet is the most average person you could imagine for the setting.

# <a name="abilities"></a>Abilities

    For a moment, I was seeing stars, not the good kind, like when you're traveling through space, but the annoying kind, like when you've been hit hard in the head by some lug twice as big as you.

    "So, you wanna play rough, big guy? You wouldn't like it when I..." I was cut off, again. The big blue-scaled monster grabbed my by my ankle and sent me flying, the gritty stone below me ripping a layer of skin from my cheek as if it was sandpaper. Hell, it burned. It let go, flinging me to the side and I tumbled. I wobbled, getting back to my feet, the big guy coming towards me again. The crowds cheered, I didn't know if for me, or if for ... well, whatever it was.

    Heck, I didn't want to get pummeled like that again, so I ran. At least that'd give me a chance to look around this place, see what I could use to my advantage. There were the stands, where the aliens watched from. I watched back momentarily. They had thrown me in here, and after I came to their planet seeking alliance.

    The thudding of the thing behind me started to get louder, it was gaining. I continued to look around, there were stone pillars, a couple of long sticks lying around, presumably for weapons, and the chasm, opposite the stands, which lead to a pit that went down further than I could see. Also, scattered around were the remains of others, presumably fell in this same twisted game. 

    Letting myself get carried away by the scenery more than I could afford, I barely was able to dodge as the blue monstrosity's fist came swinging down. As I was standing back up from the roll, however, a glinting in some rubble caught my eye, a metallic spear on one of the corpses with a unique style I thought I recognized. I ran straight for the beast, a move it did not seem to expect, and rolled between its legs, making a beeline towards the corpse. 

    I lifted up the spear, and said a quick word of thanks lasting only a second for the fallen Lakota warrior who had held it, but a second was golden in this situation. The shimmer of the spear had revealed it to be one of spirits' design, and the specific emblem was that of a laughing spider, most likely being Iktomi, I did not know whether to be excited or to curse my luck. 

    As it was the best I had, and a gamble for using one of the trickster's own weapons, I threw the spear at the beast, silently praying the spear would strike true, and not backfire too much.

    A moment later I found myself wishing I had prayed a bit harder. The spear had struck true, alright, and even propelled itself  towards the target with a fancy flash of energy. However, even though I meant to let go of it, it did not let go of me. I found the spear buried into the beast, up to my hand. 

    As if to add insult to injury, there was a hiss, and self-propelled silky tendrils shot out from the spear, catching the beast and I in a quickly constructed net from the chaotic spear. One hand pinned, the other buried in the beast, I bit into net, trying to pull it away. It tasted it was some kind of metal that I could not place. Neither I, nor the beast, could move, and I was firmly planted on the beast's side. I let lose a few curses in the ancestral language, and the way the tendrils shimmered made it feel as if the spirits were laughing at me. If I got the chance to speak to Iktomi face-to-face in the near future, I would be sure to get my mother to give him an earful.

    The aliens swarmed from the stands, making various sounds. It took a bit of guesswork, but apparently I had passed their test. Though apparently there was some disagreement, for none had incapacitated the beast while becoming incapacitated themselves. Some were arguing that I had won, others that I had lost, and yet some insisting it was a draw.

    I stammered, "I meant to do that...", probably one of the worst lines I could have chosen, had I still been among humans. But my translator picked up on it, and apparently, that was a convincing enough argument to the surrounding aliens. Soon, I was unanimously declared the winner, and my reward is that they would listen to my plea for alliance.

    A short time later, I was talking to them about the reason I had come, to seek an alliance against those who threaten both our worlds. They listened closely, and then they agreed under one condition. That condition they emphasized was that I fix my own shortcoming, and be honest about when I or my people need help. I asked what they meant, and then they informed me... while it was my mistake to assume it was an arena. It was a testing grounds where people would see how long they could last. They were not in the stands to just watch me, but to help if I called out for it.

The majority of your character sheet is Abilities, Aspects, Social traits, and things they own. Of these, Abilities are arguably the most important.

Abilities are generalized things about your character, from their health to their ability to cook. Most abilities have certain powerful aspects they give you access too. 

Abilities are divided into Tiers. Each ability is in some way dependent on connected abilities in other tiers. Each tier has its own abilities with their own traits as a group.

## 1st Tier Ability: Being
### Being
**Higher Ability:**
None

**Damage 1%-99% Damage Effect:**
Whenever you take an action where you roll to see if you succeed, also roll a percentile dice. You must roll higher than your percentage damage or the action fails.

**100% Damage effect:**
Death/Destruction

**Healing Being Damage:**

For Characters, healing Being damage requires buying the damage off with Character Points, changing your character’s total character points as a result. If you do not have enough spare character points, to heal the being damage, to heal it you must take aspects with negative point costs and pay it with that.

**Description:**

Being is who you are at your core.

**Some things you can do with Being**

    - Exist
    
**Available Ability Aspects in Full Player’s Guide:** 

Generic Aspects ([Ability] Skill X aspects 0 through 50), [Ability] Skill [X] – Synergy)

Levels there are Ability Specific Aspects at: 0



## 2nd Tier Abilities: Essence
### Body

**Higher Ability:** Being

**Damage 1%-99% Damage Effect (also known as “Body Strain”):**

Whenever you take an action where you roll to see if you succeed, if the related ability is Body or has Body as a higher ability, roll a percentile dice. You must roll higher than your percentage damage or the action fails.

*Flavor & Gameplay Note: Body Damage is called damage just to help people with familiarity with game terms. This type of damage is not directly caused by an external source, but caused over-exertion from avoiding damage (pulling muscles, training limbs, tearing joints, bruises from absorbing impacts, etc.) If your player is restrained in some way, it is possible for the GM to rule that damage bypasses your body and goes straight to Being.*

**100% Damage effect:**

You are incapacitated, and can no longer use abilities that have Body as a higher ability (even if those uses of those abilities don’t require an action).

**Healing Body Damage:**

Generally, characters heal Body Strain using the free Rest aspect. 

**Description:**

Body is the substance of which you are built.
Some things you can do with Body

    - Eat
    - Breath
    - Survive


**Available Ability Aspects in Full Player’s Guide:** 
Generic Aspects ([Ability] Skill X aspects 0 through 50), [Ability] Skill [X] – Synergy)

Levels there are Ability Specific Aspects at: 0

Special: Push past the limit [Body]

### Mind
**Higher Ability:**
Being

**Damage 1%-99% Damage Effect (also known as “Mind Strain”):**

Whenever you take an action where you roll to see if you succeed, if the related ability is Mind or has Mind as a higher ability, roll a percentile dice. You must roll higher than your percentage damage or the action fails.

*Flavor & Gameplay Note: Mind Damage is called damage just to help people with familiarity with game terms. This type of damage is not directly caused by an external source, but caused over-exertion from avoiding damage (over-rationalization, reliance on phantasms, panic responses, falling into fight or flight syndrome, etc.) If your player is severely and unavoidably traumatized in some way, it is possible for the GM to rule that damage bypasses your mind and goes straight to Being.*

**100% Damage effect:**

You are unconscious (or by GM ruling, shell-shocked), and can no longer use abilities that have Mind as a higher ability (even if those uses of those abilities don’t require an action).

**Healing Mind Damage:**

Generally, characters heal Mind Strain using the free Rest aspect. 

**Description:**
Mind is the core of your consciousness and identity
Some things you can do with Mind

    - Think
    - Dream
    - Hope


**Available Ability Aspects in Full Player’s Guide:**
 
Generic Aspects ([Ability] Skill X aspects 0 through 50), [Ability] Skill [X] – Synergy)

Levels there are Ability Specific Aspects at: 0

Special: Push past the limit [Body]

## 3rd Tier Abilities: Talents
### Speed (Spe)
Higher Ability:
Body
Description:
Speed is Body’s Quickness. Speed determines how fast you can move.
Some things you can do with Speed
    • Run quickly
Available Ability Aspects in Full Player’s Guide: 
Generic Aspects ([Ability] Skill X aspects 0 through 50), [Ability] Skill [X] – Synergy)
Levels there are Ability Specific Aspects at: 0

### Finesse (Fin)
**Higher Ability:** Body

**Description:**
Finesse is Body’s flexibility and deftness. 

Some things you can do with Finesse

    • Perform intricate movements

**Available Ability Aspects in Full Player’s Guide:** 

Generic Aspects ([Ability] Skill X aspects 0 through 50), [Ability] Skill [X] – Synergy)

Levels there are Ability Specific Aspects at: 0

### Might (Mig)
**Higher Ability:** Body

**Description:**

Might is Body’s raw power. Might determines how the raw physical force you can bring to bear.

Some things you can do with Might

    • Move heavy things
    
**Available Ability Aspects in Full Player’s Guide:** 

Generic Aspects ([Ability] Skill X aspects 0 through 50), [Ability] Skill [X] – Synergy)

Levels there are Ability Specific Aspects at: 0

### Toughness (Tou)
**Higher Ability:** Body

**Description:**

Toughness is Body’s Durability. Toughness determines how much punishment your body can handle.

Some things you can do with Toughness

    • Hold your breath for longer periods
    
**Available Ability Aspects in Full Player’s Guide:** 

Generic Aspects ([Ability] Skill X aspects 0 through 50), [Ability] Skill [X] – Synergy)

Levels there are Ability Specific Aspects at: 0

### IQ (IQ)
**Higher Ability:**
Mind

**Description:**

IQ is Mind’s Quickness. IQ determines how fast you can think.

Some things you can do with IQ

    • Think quickly
    
**Available Ability Aspects in Full Player’s Guide:** 

Generic Aspects ([Ability] Skill X aspects 0 through 50), [Ability] Skill [X] – Synergy)

Levels there are Ability Specific Aspects at: 0

### Creativity (Cre)
**Higher Ability:**
Mind

**Description:**

Creativity is Mind’s flexibility and deftness. 
Some things you can do with Creativity

    • Come up with unique ideas
    
**Available Ability Aspects in Full Player’s Guide:** 

Generic Aspects ([Ability] Skill X aspects 0 through 50), [Ability] Skill [X] – Synergy)

Levels there are Ability Specific Aspects at: 0

### Memory (Mem)
**Higher Ability:**
Mind

**Description:**

Memory is Mind’s raw power. Memory determines how the brute force memorization you can bring to bear.

Some things you can do with Memory

    • Recite lyrics verbatum
    
**Available Ability Aspects in Full Player’s Guide:** 

Generic Aspects ([Ability] Skill X aspects 0 through 50), [Ability] Skill [X] – Synergy)

Levels there are Ability Specific Aspects at: 0

### Sanity (San)
**Higher Ability:**
Mind

**Description:**

Sanity is Mind’s Durability. Sanity determines how much punishment your mind can handle.

Some things you can do with Sanity

    - Read a book by an insanity god or Godel’s incompleteness Therom and be unaffected
    - Be able to sleep when a house creaks in the night
    - Focus on what needs to be done during a catastrophe
    
Available Ability Aspects in Full Player’s Guide: 

Generic Aspects ([Ability] Skill X aspects 0 through 50), [Ability] Skill [X] – Synergy)

Levels there are Ability Specific Aspects at: 0


## 4th Tier Abilities: Skills

### Accuracy

**Higher Abilities:**
Finesse

**Description:**

Accuracy is the ability of the character to coordinate their body to what they see, allowing them to throw things at a target reliably, to land on a point accurately, to follow complex body movements, and more.

Some things you can do with Accuracy:

    • Skip a rock multiple times across a pond
    • Do well at rhythm games
    • Throw a baseball to a catcher, hitting his mitt
    • Reliably hit high points in a game of darts

**Available Ability Aspects in Full Player’s Guide:** 

Generic Aspects ([Ability] Skill X aspects 0 through 50), [Ability] Skill [X] – Synergy)

Levels there are Ability Specific Aspects at: 1, 8


### Archaeology 

**Higher Abilities:**
Creativity, Memory

**Description:**

Archaeology is the ability to research and understand history.

Some things you can do with Archaeology

    • See the tell-tale signs of an ancient civilization
    • Make educated guesses on the one-time purposes of abandoned buildings
    • Make general educated guesses about past cultures
    • To see how major effects were caused by another and what may have happened in gaps (or what probably will come next)
    
**Available Ability Aspects in Full Player’s Guide:** 

Generic Aspects ([Ability] Skill X aspects 0 through 50), [Ability] Skill [X] – Synergy)

Levels there are Ability Specific Aspects at: 2

### Argue 

**Higher Abilities:**
Creativity, Sanity

**Description:**

Argue (which has no aspects in this book) is merely the ability to successfully convince yourself that you are right and another person is wrong (for convincing others, see debate). Although arguing with others is generally a pointless task, arguing with yourself can help you reason through complicated ideas. It can also be useful for fighting off outside mental influence (such as the reasoning of a loved one, brainwashing, or influence that's not part of your belief system, no matter how well backed.)

Some things you can do with Argue

    • Resist propaganda
    • Not buy anything from a clever salesman
    • Resist intrusive thoughts
    • Move a social or moral modifier closer to zero
    
**Available Ability Aspects in Full Player’s Guide:** 

Generic Aspects ([Ability] Skill X aspects 0 through 50), [Ability] Skill [X] – Synergy)

Levels there are Ability Specific Aspects at: None

### Armor 

**Higher Abilities:**
Might, Toughness

**Description:**

This is your ability to adequately make use of various forms of armor and shields. (For the purpose of using shields, it can be used in place of an associated Arms ability.) It also determines your ability to maintain your armor, how to fit it properly, and how to make use of your armor in the best ways.

Some things you can do with Armor

    • Block an incoming punch with a durable or armored part of your body
    • Block an incoming sword swing with shields
    • Basic knowledge on how to treat a bruise
    • Basic knowledge on fixing dents or scratches in armor
    • Proper stances to brace against heavy attacks with a tower shield
    • How to properly put on and strap in a Kevlar combat suit
    
**Available Ability Aspects in Full Player’s Guide:** 

Generic Aspects ([Ability] Skill X aspects 0 through 50), [Ability] Skill [X] – Synergy)

Levels there are Ability Specific Aspects at: x, 4, 15

### Arms [Fast, Heavy, Melee, or Ranged]

**Higher Abilities:**
[Fast: Speed], [Heavy: Toughness], [Melee: Might], [Ranged: Finesse]

**Description:**

Arms is the ability to control, maintain, aim, and wield a specific category of weapon. Most weapons will belong to at least two different categories. As long as you have a match for one of the categories, you can use that weapon with Arms. It’s worth noting skills can only be used in non-intense times or defensively unless the player has the appropriate aspects, and this still applies to Arms.

Some things you can do with Arms

    • (All) Show off in a weapons expedition
    • (Melee) Perform a Sword Dance
    • (All) Know proper form & care techniques for your weapons
    • (Melee) Use a sword to block an incoming swing from another sword
    • (Ranged) Shoot targets at a range with a rifle
    • (Fast) Perform knife tricks
    • (Heavy) Break through a door
    
**Available Ability Aspects in Full Player’s Guide:** 
Generic Aspects ([Ability] Skill X aspects 0 through 50), [Ability] Skill [X] – Synergy)

Levels there are Ability Specific Aspects at: 1, 5, 8, 10, 12

#### Arms Fast

Arms fast generally applies to light weapons, designed more for finesse than Might, and are usually ones that can easily be used in one hand (although exceptions exist, like some pole-arms with light tips and staves)

**Levels there are Ability Specific Aspects at:** 8

#### Arms Heavy

Arms Heavy generally applies to large might-oriented weapons that take at least two hands to use, although one-handed weapons can sometimes apply if they’re designed less for finesse than brute power, such as a large club or shoulder-mounted cannon.

#### Arms Melee

Arms Melee specifically applies to weapons that are used close  to the opponent, where when your weapon hits, you’re still holding part of what hit them. This applies to everything from swords to whips to just whacking something with a stick. 

**Levels there are Ability Specific Aspects at:** 3

#### Arms Ranged

Arms Ranged specifically applies to weapons that require aim and something leaving your weapon to hit a target. From bows to rifles to laser guns, if part of the goal of the weapon is to be far away of your target, it probably uses Arms Ranged, and there’s a good chance the weapon uses ammo.

### Artistry

**Higher Abilities:**
Creativity

**Description:**

Artistry is the ability to create artwork. Everything from corporate logos to jewelry to lifelike paintings to vivid stories to photography. In this case, it's fairly strongly suggested that you create your own specialization that deals with your chosen art form with Artistry as the only higher ability.
Some things you can do with Artistry

    • Create a painting that causes some people to see it to cry
    • Create a mural that brightens the general mood in an area of city
    • Design a manipulative propaganda poster
    • Draw people in with a vibrant display of fireworks
    
**Available Ability Aspects in Full Player’s Guide:** 

Generic Aspects ([Ability] Skill X aspects 0 through 50), [Ability] Skill [X] – Synergy)

Levels there are Ability Specific Aspects at: 10, 20

### Conversation 

**Higher Abilities:**
Memory, Sanity

**Description:**

Conversation is the ability to talk with people and get information from them.

Some things you can do with Conversation

    • Find out what a guard is guarding
    • Get an unsuspecting spy to spill some vital info
    • Cause a lying suspect to slip up during a trial
    • Embarrass your rival in high school by making them accidentally say something that would be misunderstood.
    
**Available Ability Aspects in Full Player’s Guide:** 

Generic Aspects ([Ability] Skill X aspects 0 through 50), [Ability] Skill [X] – Synergy)

Levels there are Ability Specific Aspects at: 6, 25

Special: Recruit – Skill 12

### Cooking 

**Higher Abilities:**
Creativity

**Description:**

Cooking is the ability to prepare foods for you as well as for others.

Some things you can do with Cooking

    • Create a fancy dinner with quality components
    • Create a healthy and edible food when resources are scares
    • Draw in a crowd of people with delicious-smelling aromas
    • Create comfort food that can ease the tensions of all who eat. 
    
**Available Ability Aspects in Full Player’s Guide:** 

Generic Aspects ([Ability] Skill X aspects 0 through 50), [Ability] Skill [X] – Synergy)

Levels there are Ability Specific Aspects at: 5, 10, 15

### Craft 

**Higher Abilities:**
IQ

**Description:**

Craft is the ability to create complex items from plans and simpler materials. 

Some things you can do with Craft

    • Make paper airplanes or Origami
    • Build a motorcycle from blueprints and standardized parts
    • Knit a sweater
    
**Available Ability Aspects in Full Player’s Guide:** 

Generic Aspects ([Ability] Skill X aspects 0 through 50), [Ability] Skill [X] – Synergy)

Levels there are Ability Specific Aspects at: X, 5

### Dance 

**Higher Abilities:**
Finesse, Creativity

**Description:**

Dance is the ability to move in a coordinated fashion in specific timing, generally with music.

Some things you can do with Dance

    • Perform Modern Dance, Ballroom Dancing, or Sword Dancing
    • Impress Dance connoisseurs
    • Tell a story through your movements
    • Accurately judge another person’s body abilities through dancing as a partner.
    
**Available Ability Aspects in Full Player’s Guide:** 

Generic Aspects ([Ability] Skill X aspects 0 through 50), [Ability] Skill [X] – Synergy)

Levels there are Ability Specific Aspects at: 2

### Debate 

**Higher Abilities:**
Creativity, Memory

**Description:**

Debate is the ability to convince others of things using well planned and thought out concepts and ideas, along with well-placed emotional pleas.

Some things you can do with Debate

    • Discover the logical in someone's speech, or identify propaganda
    • Discover if someone is cleverly lying
    • Confuse or clarify topics for onlookers
    • Demonstrate the folly of an opponent
    
**Available Ability Aspects in Full Player’s Guide:** 

Generic Aspects ([Ability] Skill X aspects 0 through 50), [Ability] Skill [X] – Synergy)

Levels there are Ability Specific Aspects at: 2, 4, 5, 8, 10, 12, 15 & 15

### Deduction

**Higher Abilities:**
Memory, Sanity

**Description:**

Deduction is the ability gain more hidden information from more simplistic information that is available.

Some things you can do with deduction

    • Catch inconsistencies in information to build additional clues
    • Figure out what the next step in a sequences of events likely is
    • Find ways to eliminate incorrect assumptions from what you’re researching
    
**Available Ability Aspects in Full Player’s Guide:** 

Generic Aspects ([Ability] Skill X aspects 0 through 50), [Ability] Skill [X] – Synergy)

Levels there are Ability Specific Aspects at: 10

### Deflection 

**Higher Abilities:**
Speed, Finesse, Might

**Description:**

Deflection is the ability to redirect incoming projectiles and attacks by redirecting them.

Some things you can do with deflection

    • Perform stunts where you catch an arrow
    • Ricochet a bullet off of a metal surface
    • Only get a scratch on a riot shield blocking a rifle shot
    
**Available Ability Aspects in Full Player’s Guide:** 

Generic Aspects ([Ability] Skill X aspects 0 through 50), [Ability] Skill [X] – Synergy)

Levels there are Ability Specific Aspects at: 1, 15

### Design 

**Higher Abilities:**
IQ, Creativity

**Description:**

An ability that often works alongside others, design is the ability to take general theories and/or ideas and turn them into viable plans for later crafting.

Some things you can do with design

    • Create designs for a board-game
    • (If used alongside cooking) Create an easily understood recipe
    • (If used alongside Crafting) Create a reusable mold for an ornate sword
    
**Available Ability Aspects in Full Player’s Guide:** 

Generic Aspects ([Ability] Skill X aspects 0 through 50), [Ability] Skill [X] – Synergy)

Levels there are Ability Specific Aspects at: 5, 6, 9 & 9

### Drive [Vehicle Category]

**Higher Abilities:**
Speed, Sanity

**Description:**

Drive is the ability to control a vehicle of a selected category. 

Categories include: Air, Water, Land, Submersible, Space
Some things you can do with Drive

    • (Air) Fly a plane
    • (Land) Ride a horse
    • (Land) Race a car
    • (Space) Land an Orbital Lander
    • (Water) Catch a wave while surfing
    • (Submersible) Estimate depth safety of a sub by sound
    
**Available Ability Aspects in Full Player’s Guide:** 

Generic Aspects ([Ability] Skill X aspects 0 through 50), [Ability] Skill [X] – Synergy)

Levels there are Ability Specific Aspects at: 2, 6

### Endurance 

**Higher Abilities:**
Toughness

**Description:**

Endurance is the ability to hold against physical exhaustion and exertion and to handle what might otherwise damage your body.

Some things you can do with Endurance

    • Run a marathon
    • Hold heavy weights for a long time
    • Brace against in incoming Impact
    • Hold your breath to avoid a noxious gas
    
**Available Ability Aspects in Full Player’s Guide:** 

Generic Aspects ([Ability] Skill X aspects 0 through 50), [Ability] Skill [X] – Synergy)

Levels there are Ability Specific Aspects at: 5

### Flexibility 

**Higher Abilities:**
Finesse

**Description:**

Flexibility is the ability to bend and contort your body.
Some things you can do with flexibility

    • Fit into small spaces
    • Reach fallen tools in tight spaces
    • Perform many forms of gymnastics
    
**Available Ability Aspects in Full Player’s Guide:** 

Generic Aspects ([Ability] Skill X aspects 0 through 50), [Ability] Skill [X] – Synergy)

Levels there are Ability Specific Aspects at: 8 & 8, 15

### Handling 

**Higher Abilities:**
Might, Memory

**Description:**

Handling is the ability to guide and raise animals. From riding horses, to herding sheep, to feeding pigs, to raising ferrets.

Some things you can do with Handling

    • Ride Horses
    • Get Sheep to follow you
    • Breed pets
    • Get an angry dog to calm down
    
**Available Ability Aspects in Full Player’s Guide:** 

Generic Aspects ([Ability] Skill X aspects 0 through 50), [Ability] Skill [X] – Synergy)

Levels there are Ability Specific Aspects at: 1, 5

### Hold 

**Higher Abilities:**
Might

**Description:**

Hold is the ability to keep something in one place, such as pinning a grappled opponent, keeping a large heavy object from falling, or blocking a door.

Some things you can do with Hold

    • Hold back a boulder from going down a hill
    • Keep a group of enemies from opening a door
    • Brace a section falling roof in a fire
    
**Available Ability Aspects in Full Player’s Guide:** 

Generic Aspects ([Ability] Skill X aspects 0 through 50), [Ability] Skill [X] – Synergy)

Levels there are Ability Specific Aspects at: 3, 7, 9 & 9

### Investigate

**Higher Abilities:**
IQ

**Description:**

Investigate is the ability to study a situation in detail, and to find clues where originally their are none, or finding small overlooked details. (It is often used in tandem with the deduction ability. The investigate ability finds the clues and the deduction ability gets their meaning.)

Some things you can do with Investigate

    • Find a key contact for an underworld mob boss
    • Track someone through the dark web
    • Discover from clues who took the missing forbidden idol
    
**Available Ability Aspects in Full Player’s Guide:** 

Generic Aspects ([Ability] Skill X aspects 0 through 50), [Ability] Skill [X] – Synergy)

Levels there are Ability Specific Aspects at: 2, 12, 17, 25

### Martial Arts

**Higher Abilities:**
Speed, Finesse, Might

**Description:**

Martial arts is the ability to fight without conventional weapons. If you want to get into further details

Some things you can do with Martial Arts

    • Break wood planks or even iron rods in half with your bare hands
    • Dodge an incoming melee attack
    • Fight in a boxing match
    
**Available Ability Aspects in Full Player’s Guide:** 

Generic Aspects ([Ability] Skill X aspects 0 through 50), [Ability] Skill [X] – Synergy)

Levels there are Ability Specific Aspects at: X, 1, 7, 10


### Mathematics

**Higher Abilities:**
IQ, Memory

**Description:**

Mathematics is the ability to understand numbers, trends, formulas, shapes, and highly abstract concepts.

Some things you can do with Mathematics

    • Quickly do arithmetic in your head
    • Perform calculus
    • Create algorithms for computer processing
    
**Available Ability Aspects in Full Player’s Guide:** 

Generic Aspects ([Ability] Skill X aspects 0 through 50), [Ability] Skill [X] – Synergy)

Levels there are Ability Specific Aspects at: 6

### Morality 

**Higher Abilities:**
Memory, Sanity

**Description:**

Morality is the ability to not only have ideas, but to understand the reasoning behind those ideas, and what makes them important. 

Some things you can do with Morality

    • Resist attempts at Inspire or Debate or other social modifiers that go against your values
    • Catch when people are violating shared morals
    • Increase resist to mind control techniques that’d have you act against your values
    
**Available Ability Aspects in Full Player’s Guide:** 

Generic Aspects ([Ability] Skill X aspects 0 through 50), [Ability] Skill [X] – Synergy)

Levels there are Ability Specific Aspects at: X, 2 & 2 & 2, 3 & 3, 5, 9

### Music

**Higher Abilities:**
Speed, Finesse, Creativity

**Description:**

Music is the ability to create audio pieces with sequence, beat, and feel with the purpose to affect its listeners in certain ways. This includes composing, singing, playing instruments, and more. 

Some things you can do with Music

    • Boost someone else’s skill checks (unless they’re stealth based)
    • Combine with inspire to spur on allies
    • Increase someone’s ability to intimidate
    
**Available Ability Aspects in Full Player’s Guide:** 

Generic Aspects ([Ability] Skill X aspects 0 through 50), [Ability] Skill [X] – Synergy)

Levels there are Ability Specific Aspects at: 5 & 5, 8, 12 & 12

### Observe [sense]

**Higher Abilities:**
Finesse, IQ

**Description:**

Observe is the ability to pick out details of your surrounding with one of your sense.

Example categories include: sight, hearing, smell, touch, or taste.

Some things you can do with Observe

    • (sight) Make out someone through heavy rain
    • (hearing) Hear a cry for help in a snowstorm
    • (smell) Sniff out food in a wasteland
    • (touch) Realize you’re about to step on a spike with enough warning to miss it.
    • (taste) Realize, before you swallow, that the drink is poisoned.
    
**Available Ability Aspects in Full Player’s Guide:** 

Generic Aspects ([Ability] Skill X aspects 0 through 50), [Ability] Skill [X] – Synergy)

Levels there are Ability Specific Aspects at: 4


### Personality

**Higher Abilities:**
Creativity, Memory, Sanity

**Description:**

Personality is the ability to present yourself in specific ways. From acting to just getting people to like you.

Some things you can do with Personality

    • Maintain your ideal relationships
    • Avoid being pulled into things that go against your identity
    
**Available Ability Aspects in Full Player’s Guide:** 

Generic Aspects ([Ability] Skill X aspects 0 through 50), [Ability] Skill [X] – Synergy)

Levels there are Ability Specific Aspects at: 1 & 1, 5 & 5

Special: Recruit – Skill 12 

### Procedure 

**Higher Abilities:**
Memory, Sanity

**Description:**

Procedure is the ability to reliably do something over and over again.

Some things you can do with Procedure

    • Excel in an assembly line
    • Perform a specific song you have mastered flawlessly during a tour
    • Perform your daily upkeeps and daily routines quickly and efficiently
    
**Available Ability Aspects in Full Player’s Guide:** 

Generic Aspects ([Ability] Skill X aspects 0 through 50), [Ability] Skill [X] – Synergy)

Levels there are Ability Specific Aspects at: 3


### Regeneration 

**Higher Abilities:**
Toughness

**Description:**

Regeneration is the ability to heal from damage over time
 and to recover after an illness.

Some things you can do with Regeneration

    • Heal from a wound
    • Recover after surgery quickly
    
**Available Ability Aspects in Full Player’s Guide:** 

Generic Aspects ([Ability] Skill X aspects 0 through 50), [Ability] Skill [X] – Synergy)

Levels there are Ability Specific Aspects at: 1

### Resolution 

**Higher Abilities:**
Sanity

**Description:**

Resolution is the ability to keep mental focus on a task when you might otherwise fail, in order to keep pushing yourself forward. (Also known as willpower.)

Some things you can do with Resolution

    • Heal from brain trauma quicker
    • Resist things that reduce sanity
    • Stave off the effects of mind-altering substances
    
**Available Ability Aspects in Full Player’s Guide:** 

Generic Aspects ([Ability] Skill X aspects 0 through 50), [Ability] Skill [X] – Synergy)

Levels there are Ability Specific Aspects at: 1

### Rush

**Higher Abilities:**
Speed, Might

**Description:**

Rush is the ability to do things faster than normal without messing up too badly.

Some things you can do with Rush

    • Grab all your things extra quick to run out of the door in time to catch a ride
    • Gather things quickly before a rain
    • Prepare defenses while an enemy has almost arrived
    
**Available Ability Aspects in Full Player’s Guide:** 

Generic Aspects ([Ability] Skill X aspects 0 through 50), [Ability] Skill [X] – Synergy)

Levels there are Ability Specific Aspects at: 8

### Sneak 

**Higher Abilities:**
Finesse, IQ

**Description:**

Sneak is the ability to move and take actions undetected.

Some things you can do with Sneak

    • Get past a bouncer unnoticed
    • Avoid tripwires
    
**Available Ability Aspects in Full Player’s Guide:** 

Generic Aspects ([Ability] Skill X aspects 0 through 50), [Ability] Skill [X] – Synergy)

Levels there are Ability Specific Aspects at: 3, 6

### Sprint 

**Higher Abilities:**
Speed

**Description**:

This is the ability to move very quickly for a short amount of time.

Some things you can do with Sprint

    • Arrive at a destination by a few minutes earlier
    • Get through a small region of dangerous terrain in fewer turns
    • Take part in a sports compitition
    
**Available Ability Aspects in Full Player’s Guide:** 

Generic Aspects ([Ability] Skill X aspects 0 through 50), [Ability] Skill [X] – Synergy)

Levels there are Ability Specific Aspects at: 1, 2, 4, 8, 18, 26

### Survival 

**Higher Abilities:**
Toughness, Sanity

**Description:**

Survival is the ability to take care of yourself (and possibly others) against rough environmental conditions. This includes foraging, building shelter, scouring trashcans for food, avoiding poisonous substances, tracking game, scaling fish, and other survival techniques.

Some things you can do with Survival

    • Build a fire
    • Set noticeable emergency signals
    • Build impromptu shelter
    • Prioritize survival needs
    
**Available Ability Aspects in Full Player’s Guide:** 

Generic Aspects ([Ability] Skill X aspects 0 through 50), [Ability] Skill [X] – Synergy)

Levels there are Ability Specific Aspects at: 5, 6

### Tactics 

**Higher Abilities:**
IQ, Creativity

**Description:**

Tactics is the ability to plan group actions with maximum efficiency, to capitalize on strengths of the team while minimizing its weaknesses.

Some things you can do with Tactics

    • Plan the defense or offensive siege
    • Prepare an ambush
    • Set signs to convince an enemy general that you are planning an ambush when you aren’t
    • Coordinate hunters to catch large game
    • Play strategy board games at competitive levels
    • Plan activities for a sports team
    
**Available Ability Aspects in Full Player’s Guide:** 

Generic Aspects ([Ability] Skill X aspects 0 through 50), [Ability] Skill [X] – Synergy)

Levels there are Ability Specific Aspects at: 3, 4, 5, 6, 10 & 10, 15 & 15, 20

### Throw

**Higher Abilities:**
Finesse, Might

**Description:**

Throw is the ability to physically propel objects, and can be used in place of Arms [ranged] for the purpose of objects like spears, rocks, ninja stars, and more. Can also be used to throw grappled opponents, or you turning other things into projectiles.

Some things you can do with Throw

    • Throw an opponent while wrestling
    • Send a spear into a target
    • Do well at carnival games
    
**Available Ability Aspects in Full Player’s Guide:** 

Generic Aspects ([Ability] Skill X aspects 0 through 50), [Ability] Skill [X] – Synergy)

Levels there are Ability Specific Aspects at: 1, 15

### Travel 

**Higher Abilities:**
Speed, Toughness

**Description:**

Travel is the ability to endure for long distances of movement, including staying awake on boring roads, to keep from getting lost, and to walk for long distances.

Some things you can do with Travel

    • Plan a long journey
    • Navigate a detour through a treacherous mountain range
    • Run a marathon
    
**Available Ability Aspects in Full Player’s Guide:** 

Generic Aspects ([Ability] Skill X aspects 0 through 50), [Ability] Skill [X] – Synergy)

Levels there are Ability Specific Aspects at: 4

### Treatment 

**Higher Abilities:**
Finesse, IQ, Creativity

**Description:**

The ability to help others heal from wounds, poisons, and other physical problems.

Some things you can do with Treatment

    • Set a broken bone
    • Bandage a wound
    • Prevent infection
    
**Available Ability Aspects in Full Player’s Guide:** 

Generic Aspects ([Ability] Skill X aspects 0 through 50), [Ability] Skill [X] – Synergy)

Levels there are Ability Specific Aspects at: 2, 10

### Vigilance 

**Higher Abilities:**
Toughness, IQ

**Description:**

Vigilance is the ability to keep aware in difficult situations. Among these are staying awake during long guard watches, to keeping alert as a forward scout, to keeping an eye on the news for long periods to watch for certain tell-tale events. In a lot of ways, Vigilance is the mental equivalent to endurance.

Some things you can do with Vigilance

    • Keep watch through a long night
    • Notice signs of an approaching enemy army earlier
    • Avoid being surprised in an ambush
    
**Available Ability Aspects in Full Player’s Guide:** 

Generic Aspects ([Ability] Skill X aspects 0 through 50), [Ability] Skill [X] – Synergy)

Levels there are Ability Specific Aspects at: 4

### Wellness 

**Higher Abilities:**
Tougness

**Description:**

Wellness is the ability to stand up to disease, poison, harsh weather conditions (such as being too cold or too hot), and being able to mange your diet to maximize your health.

Some things you can do with Wellness

    • Recover from illness
    • Work through a flu
    • Drink poison without negative effects
    
**Available Ability Aspects in Full Player’s Guide:** 

Generic Aspects ([Ability] Skill X aspects 0 through 50), [Ability] Skill [X] – Synergy)

Levels there are Ability Specific Aspects at: x, 3, 6 

### Wisdom 

**Higher Abilities:**
Creativity, Memory

**Description:**

This is the ability to not make stupid decisions, prevent yourself from making common mistakes, and to learn from experience.

Some things you can do with Wisdom

    • Avoid struggles
    • Catch subtle easily missed nuance
    • Give good advice
    
**Available Ability Aspects in Full Player’s Guide:** 

Generic Aspects ([Ability] Skill X aspects 0 through 50), [Ability] Skill [X] – Synergy)

Levels there are Ability Specific Aspects at: 5, 11, 18 

## 5th+ Tier Abilities: Specializations or Specialized Skills

### Beauty 

**Higher Abilities:**
Finesse, Wellness

**Description:**

Beauty is the ability to make oneself (or others) attractive, as well as developing the physical traits to fit the ideals of beauty.   It includes cosmetology, adapting attire, makeup, proper dieting and appearance-oriented exercises.

Some things you can do with Beauty

    • Win a beauty pageant. 
    • Get authorities who would be attracted to you to give you a bit of leniency
    • Model for a magazine
    • Perform a mild disguise to fit with the basic appearances of a culture
    
**Available Ability Aspects in Full Player’s Guide:** 

Generic Aspects ([Ability] Skill X aspects 0 through 50), [Ability] Skill [X] – Synergy)

Levels there are Ability Specific Aspects at: 2, 5, 10


### Cartography 

**Higher Abilities:**
Accuracy, Survival

**Description:**

Cartography is the ability to read maps, deduce location, create maps, and generally keep from getting lost. 

Some things you can do with Cartography

    • Not get lost when visiting a new city
    • Guide a ship across the ocean
    • Map out an unexplored area
    • Find out where you are by the position of stars or sun.
    
**Available Ability Aspects in Full Player’s Guide:** 

Generic Aspects ([Ability] Skill X aspects 0 through 50), [Ability] Skill [X] – Synergy)

Levels there are Ability Specific Aspects at: 5, 10, 12, 18

### Exercise 

**Higher Abilities:**
Travel, Martial Arts, Endurance

**Description:**

Exercise is your ability to improve and enhance your body.
Some things you can do with exercise

    • Run a Mile
    • Lift heavy weights
    • Compete in strength competitions
    • Understand about  proper protein intake while exercising
    
**Available Ability Aspects in Full Player’s Guide:** 

Generic Aspects ([Ability] Skill X aspects 0 through 50), [Ability] Skill [X] – Synergy)

Levels there are Ability Specific Aspects at: 1, 4, 8

### Forage 

**Higher Abilities:**
Observe, Survival

**Description:**

Forage is a subset of survival, helping you find what you need in hostile environments. Examples include finding water and food in a desert, finding needed medicinal plants in a rainforest, and locating pockets of air when trapped in a sinking ship.

Some things you can do with Forage

    • Determine which fruits are safe or poisonous
    • Find the best spot to dig for water
    • Extract Ice from Lunar Soil
    • Find air tanks in an abandoned and radioactive city
    
**Available Ability Aspects in Full Player’s Guide:** 

Generic Aspects ([Ability] Skill X aspects 0 through 50), [Ability] Skill [X] – Synergy)

Levels there are Ability Specific Aspects at: 1, 12

### Grapple 

**Higher Abilities:**
Martial Arts, Endurance, Hold

**Description:**

Grapple is the ability to wrestle with an opponent, and trying to control their movements.

Some things you can do with Grapple

    • Take part in Judo competitions
    • Tackle a fleeing criminal
    • Wrestle a bear barehanded
    • Hold down a thrashing fireman’s hose
    
**Available Ability Aspects in Full Player’s Guide:** 

Generic Aspects ([Ability] Skill X aspects 0 through 50), [Ability] Skill [X] – Synergy)

Levels there are Ability Specific Aspects at: 2, 6


### Identification 

**Higher Abilities:**
Observe, Memory

**Description:**

Identification is the ability to recognize somethings value, worth, and purpose from various clues about the item.
Some things you can do with Identification

    • Figure out when and where an object was made
    • Tell what a good price for an object would be
    • Tell if an ID matches the person in front of you
    
**Available Ability Aspects in Full Player’s Guide:** 

Generic Aspects ([Ability] Skill X aspects 0 through 50), [Ability] Skill [X] – Synergy)

Levels there are Ability Specific Aspects at: 2

### Infiltrate 

**Higher Abilities:**
Sneak, Tactics

**Description:**

Infiltrate is the ability to sneak into an unfriendly location without raising suspicion or alarm.

Some things you can do with Infiltrate

    • Sneak past guards
    • Pick a lock
    • Hotwire a car
    • Imitate a security guard
    
**Available Ability Aspects in Full Player’s Guide:** 

Generic Aspects ([Ability] Skill X aspects 0 through 50), [Ability] Skill [X] – Synergy)

Levels there are Ability Specific Aspects at: 2, 10, 15, 16, 19

### Inspire

**Higher Abilities:**
Conversation, Creativity

**Description:**

Inspire is the ability to drive others to do better, achieve more, or pursue specific tasks with greater vigor. 

Some things you can do with Inspire

    • Encourage a crowd to cheer for someone
    • Impress the value of certain morals on someone
    • Give elegant speeches that move the listeners
    
**Available Ability Aspects in Full Player’s Guide:** 

Generic Aspects ([Ability] Skill X aspects 0 through 50), [Ability] Skill [X] – Synergy)

Levels there are Ability Specific Aspects at: 3

Special: Recruit – Skill 12

### Learn

**Higher Abilities:**
Argue, Deduction, Mathmatics

**Description:**

Learn is the ability to pick up new mental abilities and to adapt to new information.

Some things you can do with Learn

    • Answer questions teachers ask in class
    • Choose high quality reading material and develop skills from it
    • Cram for an exam
    
**Available Ability Aspects in Full Player’s Guide:** 

Generic Aspects ([Ability] Skill X aspects 0 through 50), [Ability] Skill [X] – Synergy)

Levels there are Ability Specific Aspects at: 1, 4, 8

### Locksmith

**Higher Abilities:**
Infiltrate, Sneak

**Description:**

Locksmith is the ability to create mechanisms designed to control access… or to circumvent them.

Some things you can do with Locksmith

    • Create locks
    • Open locks
    • Create booby traps
    
**Available Ability Aspects in Full Player’s Guide:** 

Generic Aspects ([Ability] Skill X aspects 0 through 50), [Ability] Skill [X] – Synergy)

Craft Skill 5 – Locksmithing

*Note: This specialization exists mainly to give infiltration and sneak oriented characters access to craft’s locksmithing primarily for lock-breaking purposes. If a character is actually intended to be a locksmith, using craft may make more sense.*

### Philosophy 

**Higher Abilities:**
Deduction, Morality

**Description:**

Philosophy is the ability to understand strands of concept and thought to come to new ideas. It covers the ability to make educated guesses, intelligent reasoning about things unknown, and to come up with new ideas to test out. In many ways, philosophy is the ability to come up with good questions. It also helps you weed out bad or intentionally misleading ideas, making you more resistant to manipulation.

Some things you can do with Philosophy

    • Notice lies and inconsistency
    • Avoid being fast-talked
    • Make split second moral decisions
    • Avoid decision paralysis
    
**Available Ability Aspects in Full Player’s Guide:** 

Generic Aspects ([Ability] Skill X aspects 0 through 50), [Ability] Skill [X] – Synergy)

Levels there are Ability Specific Aspects at: 2, 4, 10

### Politics 

**Higher Abilities:**
Argue, Conversation, Inspire

**Description:**

Politics is the ability to manipulate masses of people for specific goals.

Some things you can do with Politics

    • Run for political office
    • Manipulate government
    • Help yourself or others consolidate political power
    
**Available Ability Aspects in Full Player’s Guide:** 

Generic Aspects ([Ability] Skill X aspects 0 through 50), [Ability] Skill [X] – Synergy)

Levels there are Ability Specific Aspects at: 5, 20

*Note: It should be paid attention to that Politics is one of the few "6th Tier" abilities. Broad Skills are 4th Tier, Normal Specialized skills are 5th Tier, but politics is a specialized skill of a specialized skill, making it a kind of unofficial 6th Tier. These are difficult to level, but can gain bonuses equal to the combined bonus and level of the specialized skill above them, if that value is greater than its other higher level abilities.*

### Romance 

**Higher Abilities:**
Beauty, Conversation

**Description:**

Romance is the ability to understand the art of wooing, seduction, lust, pursuit, and ability to pique the interests of individual(s) for romantic encounters of various kinds. This ranges from using sex appeal to get what you want, to finding the right person to marry, to playing a successful matchmaker.

Some things you can do with Romance

    • Seduce an old wealthy noble
    • Catch the eye of the man/woman/enby of your dreams
    • Make people more drawn to a romance in a performance
    • Notice the compatibility of couples
    
**Available Ability Aspects in Full Player’s Guide:** 

Generic Aspects ([Ability] Skill X aspects 0 through 50), [Ability] Skill [X] – Synergy)

Levels there are Ability Specific Aspects at: 1, 2, 3 & 3, 8

*Note: It should be paid attention to that Romance is one of the few "6th Tier" abilities. Broad Skills are 4th Tier, Normal Specialized skills are 5th Tier, but Romance is a specialized skill of a specialized skill, making it a kind of unofficial 6th Tier. These are difficult to level, but gain bonuses equal to the combined bonus and level of the specialized skill above them, if that value is greater than its other higher level abilities.*

### Teach

**Higher Abilities:**
Argue,Resolution, Wisdom

**Description:**

Teach is the ability to help others learn or exercise in the fastest, most efficient, or most effective ways.

Some things you can do with Teach

    • Help students overcome a learning difficulty
    • Give instruction to a large group
    • Tutor someone to get ahead of their peers
    
**Available Ability Aspects in Full Player’s Guide:** 

Generic Aspects ([Ability] Skill X aspects 0 through 50), [Ability] Skill [X] – Synergy)

Levels there are Ability Specific Aspects at: x, 12 & 12 & 12

### Track 

**Higher Abilities:**
Survival

**Description**:

The ability to follow stuff for long distances. This includes tracking game, following escaped fugitives, and being a stalker.

Some things you can do with Track

    • Find lost keys
    • Find an animal in the woods
    • Locate an escaped prisoner
    
**Available Ability Aspects in Full Player’s Guide:** 

Generic Aspects ([Ability] Skill X aspects 0 through 50), [Ability] Skill [X] – Synergy)

Levels there are Ability Specific Aspects at: 6, 8

### Train

**Higher Abilities:**
Conversation, Survival

**Description:**

The ability to teach creatures to react to specific commands, from making willing troops follow specific commands without question to teaching dogs to do tricks.
Some things you can do with Train

    • Improve troops’ response time to avoid incoming fire
    • Get your star boxer to instantly respond to visual cues
    • Teach a dog to walk on its hind legs and shake hands
    
**Available Ability Aspects in Full Player’s Guide:** 

Generic Aspects ([Ability] Skill X aspects 0 through 50), [Ability] Skill [X] – Synergy)

Levels there are Ability Specific Aspects at: 1, 16

### Weapon Mastery

**Higher Abilities:**
Arms [Weapon Category]

**Description:**

Weapon Mastery, as a specialization to Arms, is the ability to use a very specific kind of weapon exceptionally well, knowing all it's details, special tricks, and more complex maneuvers. 

Some things you can do with Weapon Mastery

    • Perform a complex sword dance
    • Use a weapon difficult for most people fluidly
    • Kill things really effectively
    
**Available Ability Aspects in Full Player’s Guide:** 

Generic Aspects ([Ability] Skill X aspects 0 through 50), [Ability] Skill [X] – Synergy)

Levels there are Ability Specific Aspects at: 1, 3, 5, 6 & 6, 10 

# <a name="aspects"></a>Aspects

Aspects are parts of your character that set him or her apart from the typical. Aspects give adjustments to your character, alter the way people use their abilities, give actions which can be used during intense time (most abilities cannot be used proactively during intense times, only able to be used when the GM asks you to roll as a reaction).

Below, is an example aspect entry. Whenever you see anything in square brackets [like this] in an aspect, that means that you can replace what's in there with something of your choice. However, that choice sticks through all the similar brackets in the aspect. Further, any brackets like this: [X] let you choose a number of your choice. (In rare instances, there may also be others like [Y], or [Z]. Each of these you may also replace similarly with a number. However, when you choose this, it's part of the aspect when you choose it. For example, if you got the aspect "Make [x] cookies for [person]", you would write it down as something like "Make 3 cookies for Mom." or "Make 12 cookies for Ghandi", but once you chose it, it wouldn't change, and the aspect is stuck like that. Similar, [*] means you can put the word of your choice to replace the asterisk, but requires the GM to approve it.

When the brackets are in the title of an aspect, such as "[ability] skill thing", it means that there are many possible aspects you can make with it, that are generally unrelated. For example, taking it and making a "cooking skill thing" would be one aspect, and "music skill thing", another you could make from it, would be considered a totally different aspect.

However, when you have an action that says something like "Prepare x minutes" and "get a +x bonus", the number is decided each time its used.  So in this example, however long you prepared would decide how large a bonus you got each time you used the aspect.


> ### Example Aspect Title
> **Category:** Categories of the aspect, some categories have special rules or information listed at the end of the chapter.
>
> **Point Cost:** This is how many character points it takes to buy the aspect.
>
> **Prerequisites:** These are things your character has to have or have done before the aspect can be taken.
> **Effect:** If listed, this is what the aspect does for you.
>
> **Action:** If listed, this tells the action(s) given to you by the aspect to use during intense time. (They can also be used outside of intense time, if you're wondering.)
>
>> ***Action Name***
>>
>> **Preparation:** What you have to do to get the action ready. Usually involves fatiguing an ability.
>>
>> **Check:** The roll you have to match to successfully do the action.
>>
>> **Duration:** How long the action's effects last. (Instant usually just does something or changes something and leaves it at that. Permanent lasts until something reverses it, otherwise, times are as they say.)
>>
>> **Target:** This is what's affected by the action.
>>
>> **Effect:** What the action does.
>>
>> **Failure:** What happens if you failed the check.
>>
>> **Excellent Success:** This happens if you roll the highest possible result.
>>
>> **Auto-Failure:** Something that makes the check fail no matter what.
>>
>> **Fatal Failure:** This happens if your roll was the lowest possible and you failed the check.
>>
>> **Excellent Failure:** Happens when your check passes, but you rolled the lowest possible result, or when you rolled the highest possible and you still don't pass the check. (GM may also use if you tie a contest)


## Rules about Aspects
    • Each Specific aspect can be bought only once (although if the aspect has variables in its name, versions with different variables generally may be bought unless the aspect says otherwise).
    • If an aspect has brackets in its name, that means its a variable aspect. For example, if you have one whose name has [Ability] Skill 0 – Skill Inquiry. That means you can replace the [Ability] with the Ability of your choice. So, in this case, the variable aspect can become Resolution Skill 0 – Skill Inquiry.
        ◦ Note, in this case Resolution Skill 0 – Skill Inquiry is the specific aspect name. The rule about specific aspects only being bought once only applies to Resolution Skill 0 – Skill Inquiry. If you were to get it for another ability, such as turning another copy of [Ability] Skill 0 – Skill Inquiry into Sprint Skill 0 – Skill Inquiry, it would count as another specific Skill. And so our character here has both Resolution Skill 0 – Skill Inquiry as well as Sprint Skill 0 – Skill Inquiry
        ◦ An Aspect can have multiple brackets. Each combination counts as a different specific Skill.  

## Aspect Categories
**Ability Skill:**

Ability Skills will always have an associated level. Each ability skill costs it's level, is associated with an ability (either by it's design, or lets you choose the associated ability upon taking it), and requires the previous level of ability skill to get. Whenever you do anything with the ability skill, it counts as doing that thing with its associated ability.

**Background & Destiny:**


Background & Destiny aspects tell something about your character. Where they come from, who they are, and where they're going. Whether they have a great destiny to pursue, have family to come home to, or were raised orphan on the streets. Background aspects give access to destiny points. Either ways to get them, or ways to spend them. Background aspects can only be taken with GM permission. Further, many (although not all) of these have to be taken at character creation. 

**Creative: **


Creative Aspects sacrifice game balance for the purpose of player creativity. Generally, a GM focused on strict balance of power and numbers should ban creative aspects wholesale. On the flip side, a GM who is more focused on storyline and player options should encourage creative aspects.

**Culture:**


In order to have an ability with the culture category, a character needs to belong to a culture. Further, the aspect is only guaranteed to work in the character's home culture. Whether or not it's useful in other places is up to the GM. 

**Exclusive - [Category]:**


An exclusive aspect means that you can only have one of the aspects of that category. (Not as common, but there are also "Exclusive (x) - Category" aspects, which limit you to "x" number of aspects in that category.)

**Family:**


Characters generally have deeply connected families. Although it's socially oriented, it's not tied to one's culture in any way. Anything with family trait that deals with social stuff remains, no matter what culture the character is in. As a note, family often also includes very close friends as well. Long time family friends, lovers, etc.

**Free:**


The aspect doesn't cost anything to take. Any player make take it at a time when the GM is allowing character building to take place, at no cost, as long as they meet any prerequisites. If it's free and instant, the only real limiting factor to a player who has the prerequisites from using it, is the realization that they can do so.

**Instant:**


Instant aspects are aspects that can be gained at any time, even in the middle of intense action. They are considered aspects that anyone that meets the prerequisites has the ability to do if they really wanted to. If it's free and instant, the only real limiting factor to a player who has the prerequisites from using it is the realization that they can do so.

**Loyalty:**


Loyalty aspects have a requirement and a penance. If the requirement is ever failed to be met, the character loses access to that loyalty (and anything that has prerequisites of that loyalty) until that penance is done. 
Loyalties also have one or more sub-categories, some of which follow:
    • Ability-oriented: This is membership built around an ability and pursuing associated goals. These generally have a prerequisite of the ability they are oriented around, and position among the membership is at least partially dictated by one's skill in the ability.
    • Government: This is one that follows a specific ruling institution. In addition to its requirements, it also requires that the governments laws and rulings (which may change as time goes on) be followed in addition to normal requirements.
    • Social: This is a group that exists for the group's sake. Social groups require preferential treatment to members, but also get you preferential treatment. Usually (but not always) preference is given to particularly active members or members with seniority.

Further, some loyalties may list allies and enemies. These come with social modifiers towards people with the other loyalties.
Also, loyalties may list themselves as 'exclusive'. If a loyalty is listed as exclusive, you cannot have any loyalties in the same sub-category unless they are listed as allies or they list it as an ally.

**Mental:**


This aspect is closely tied to mind. If for some reason mind abilities are incapacitated as a whole, Mental aspects generally can't be used.

**Meta:**


An aspect like this reaches 'out of the game' in order to function, basing on a character's or GM's personal attributes. Most of these are more lighthearted and do not always fit well into a more serious campaign. Any Meta aspect is up to GM discretion to allow or not. Further, it is encouraged for a GM to, with some of these aspects, allow them to some players and not allow them to other players. Most of these are intended to try and make a group game more enjoyable (although some are just weird for amusement sake.) Further, a GM may require that a player take a meta aspect, but if GM does so, the character doesn't have to pay extra character points for it (although they may choose to if they want to.)
Also, as a general rule, all Meta abilities end in the word 'Soul'.

**Personality:**


If Mind is ever reduced to zero, all aspects in the ability category may have their points redistributed among personality category aspects, and the character may take the Amnesia aspect. Further, Personality aspects must be roleplayed out. If the GM doesn't think a player is roleplaying their personality aspects, they may be taken away.

**Physical:**


This aspect is closely tied to body. If for some reason body abilities are incapacitated as a whole, Physical aspects generally can't be used.

**Social:**


Social aspects are limited to the culture they're taken in. If the character goes to a different culture, they may still apply (with complications) if the two cultures have contact. If the two cultures don't have contact, the aspects don't apply.

**Title:**


Titles usually are relatively cheap to get, but expensive in their prerequisites. They often give access to special abilities, gain you extra prestige in social circles, usually affect how people act towards you in some form. Further, although a player may name their character, “Master Bob” or “Lord of Shadows Fred”, without a proper title, most the NPCs are likely to just call them “Bob” and “Fred”. If a character has a title, NPCs are more likely to call them by a flamboyant name (that is, if it fits the theme of their title).

**Tool:**


Tools aspects focus around using your stuff. Unless otherwise noted, they all require having a specific tool in order to use them.

## Aspect List

### [Ability] Skill 0 - Skill Inquiry

**Category:** Ability Skill

**Point Cost:** 0

**Prerequisite:** None

**Effect:** If the GM or an event besides your player would require you to roll [Ability], you may roll the dice and add your level in [Ability] to the roll. If you do not have any levels in [Ability], you may choose to fatigue a higher ability by 1 and use that higher ability's value instead.

**Special:** This aspect can be taken at any time. If something tests to see if you have an aspect in an ability, Response does not count. All abilities that a character has (or doesn't have) are considered to automatically have Basic Inquiry, and it doesn't need to be recorded.

### [Ability] Skill 1 - Beginner

**Category:** Ability Skill, Title

**Point Cost:** 1

**Prerequisite:** 1 level in [Ability].

**Effect:** You are considered trained in [Ability] and also know its basic technical language. In conversation, you can be recognized as someone who understands the topic and is familiar with it.

### [Ability] Skill 2 - Surge

**Category:** Ability Skill

**Point Cost:** 2

**Prerequisite:** 2 levels in [Ability], at least one [Ability] Skill 1 aspect.

**Effect:** You may completely fatigue [Ability] to get a bonus to the ability on its roll roll equal to its level. You may only use this once per action.

### [Ability] Skill 3 - Knowledge

**Category:** Ability Skill

**Point Cost:** 3

**Prerequisite:** 3 levels in [Ability], at least one [Ability] Skill 2 aspect.

**Effect:** You have extensive knowledge about the subject, and a number of related subjects (such as prominent individuals related to [Ability], different schools of practicing [Ability], history of [Ability], etc. 



Some abilities may also be applied to broader areas as well in which the ability is often associated. Some examples: 

    • Archeology can be used to have knowledge of current events

    • Argue and Philosophy can both help with politics

    • Artistry on the value of various paintings

    • Beauty lends itself to knowledge of the rich & famous, and of local gossip

    • Craft and Design lend to knowledge of the latest discoveries, strengths and weaknesses of multiple materials, structural weakpoints, etc.

    • Conversation can understand local figures of note and random trivia.

    • Drive could lend to knowledge of many different models of vehicles, their strengths and weaknesses

    • Strength and Constitution based abilities lend to sports knowledge, knowledge of strong heros, some ancient strength-based culture, and a number of health tips. In barbarian cultures, it can also come with knowledge of politics, current events, and much more.

    • Train may help with military techniques or with understanding dog breeds (depending on how the player typically uses it)

    • Etc.



In general, the player should think of what someone who excels in the chosen ability would likely be subjected to and watch for, and such things would be areas of knowledge for that ability.

### [Ability] Skill 4 - Intensity

**Category:** Ability Skill

**Point Cost:** 4

**Prerequisite:** 4 levels in [Ability], at least one [Ability] Skill 3 aspect.

**Effect:** You can make checks and actions that use [Ability] during intense time when you otherwise might not be able to. You can also call the start of intense time if you start with an [Ability] action.

### [Ability] Skill 5 - Novice

**Category:** Ability Skill, Title

**Point Cost:** 5

**Prerequisite:** 5 levels in [Ability], at least one [Ability] Skill 4 aspect.

**Effect:** You may add an additional 1d6 to each of your rolls for this ability.

### [Ability] Skill 6 - Maverick

**Category:** Ability Skill

**Point Cost:** 6

**Prerequisite:** 6 levels in [Ability], at least one [Ability] Skill 5 aspect.

**Effect:** At any point you may replace any 2d6 of your a dice roll in [Ability] with 1d12. This increases your chances of getting an extreme result, for better or worse.



### [Ability] Skill 7 - Hireable

**Category:** Ability Skill

**Point Cost:** 7

**Prerequisite:** 7 levels in [Ability], at least one [Ability] Skill 6 aspect.

**Effect:** You may spend a day working and make half a typical day's wage using [ability] working for someone (assuming you are in a culture that has regular work and wages).

Further, your highest ability with hireable during character creation gains you 50 times your level in day's wages to start the game with.

### [Ability] Skill 8 - Reaction

**Category:** Ability Skill

**Point Cost:** 8

Prerequisites: 8 levels in [Ability], at least one [Ability] Skill 7 aspect.

**Effect:** If an action was just declared, you may fatigue [ability] by 2 to do an action that uses [ability] to act before their action takes place (this only can be done if your action takes an equal or lesser amount of time than the other person's.)



Further, you may not do multiple actions in response to a single action, and you can't react to your own actions. 



You may, however, react to multiple different actions in a turn, or react in response to someone reacting.



Additional reactions in a turn (according to the above rules) causes an additional 1 fatigue for each time you've already reacted that turn.



Note: The fatigue cost to do the ability as a reaction only is paying for getting to go first. It doesn't cover any of the ability's fatigue costs.

### [Ability] Skill 9 - Assistant

**Category:** Ability Skill

**Point Cost:** 9

Prerequisites: 9 levels in [Ability], at least one [Ability] Skill 8 aspect.

**Effect:** You know enough about [ability], that you can be helpful even to most  masters without getting in the way. You gain the following action:
>
> #### [Ability] Assist
>
>**Preparation:** Fatigue [ability] by 1, 1 action
>
>**Check:** 1/4 [ability] level of target (round down).
>
>**Duration:** Instant
>
>**Target:** Character that you can see and communicate with that accepts your assistance.
>
> **Effect:** Target gains a bonus to their next use of [ability] equal to half your level, round down.
>
>**Failure:** Target gets a negative bonus equal to however much you failed the check by.

### [Ability] Skill 10 - Journeyman

**Category:** Ability Skill, Title

**Point Cost:** 10

**Prerequisite:** 10 levels in [Ability], at least one [Ability] Skill 9 aspect.

**Effect:** You may add an additional 1d6 to each of your rolls for this ability.

### [Ability] Skill 11 - Impress

**Category:** Ability Skill

**Point Cost:** 11

**Prerequisite:** 11 levels in [Ability], at least one [Ability] Skill 10 aspect.

**Effect:** You're good enough to where you can catch the attention of the average person. You gain the following action: 



### [Ability] Impress
>
>**Preparation:** Fatigue [ability] by 1, 1 action
>
>**Check:** Contest target's Sanity
>
>**Duration:** 1 turn
>
>**Target:** Any character who can see you or that you can communicate with.
>
>**Effect:** If target has 10 fewer levels in [ability] than you, you gain a +1 assertive social modifier towards them or distract them. (Distracting them causes them to automatically fail their next observe check if it doesn't target you.)

### [Ability] Skill 12 - Capable

**Category:** Ability Skill

**Point Cost:** 12

**Prerequisite:** 12 levels in [Ability], at least one [Ability] Skill 11 aspect.

**Effect:** At any point you may replace any 2d6 of your a dice roll in [Ability] with 3D4. This decreases your chances of getting an extreme result, for better or worse.

### [Ability] Skill 13 - Self Employable

**Category:** Ability Skill

**Point Cost:** 13

**Prerequisite:** 13 levels in [Ability], at least one [Ability] Skill 12 aspect.

**Effect:** You may spend a day working and make money based on your ability. 

To determine how much you make: 

Divide a typical day's wages by 10. Then multiply the result by an [ability] check for the day.

### [Ability] Skill 14 - Focus

**Category:** Ability Skill

**Point Cost:** 14

**Prerequisite:** 14 levels in the chosen ability, at least one [Ability] Skill 13 aspect.

**Effect:** With the chosen ability, at any time you may choose to double the preparation time (if cost is instant, increase to 1 action) to increase the roll result by +5. This may only be done once per roll.

### [Ability] Skill 15 - Expert

**Category:** Ability Skill, Title

**Point Cost:** 15

**Prerequisite:** 15 levels in the chosen ability, at least one [Ability] Skill 14 aspect.

**Effect:** With the chosen ability, at any time you may choose to take a -10 to the ability's checks in order to reduce the preparation's fatigue cost (if it fatigues itself) by 1. 

### [Ability] Skill 16 - Rush

**Category:** Ability Skill

**Point Cost:** 16

**Prerequisite:** 16 levels in the chosen ability, at least one [Ability] Skill 15 aspect.

**Effect:** With [ability], you may take a -10 to the ability's checks in order to cut preparation time in half. (You may only do this once per roll).

### [Ability] Skill 17 - Stability

**Category:** Ability Skill

**Point Cost:** 17

**Prerequisite:** 17 levels in the chosen ability, at least one [Ability] Skill 16 aspect.

**Effect:** With the chosen ability, at any time you may choose to double the preparation's fatigue cost (if cost is 0, increase to 1) to increase the roll result by +5. This may be done after the roll has been made, but only once per roll.

### [Ability] Skill 18 - Reliability

**Category:** Ability Skill

**Point Cost:** 18

**Prerequisite:** 18 levels in [Ability], at least one [Ability] Skill 17 aspect.

**Effect:** At any point you may replace any 1d6 of your a dice roll in [Ability] with a bonus of +3. This lowers the average, but it also removes all possibility of chance on your roll, making you able to perfectly predict how well you'll do.



### [Ability] Skill 19 - Group Assist

**Category:** Ability Skill

**Point Cost:** 19

**Prerequisite:** 19 levels in [Ability], at least one [Ability] Skill 18 aspect.

**Effect:** You know enough about [ability], to quickly and efficiently guide others in what to do. You gain the following action:
>
> #### [Ability] Assist Group
>
>**Preparation:** Fatigue [ability] by 2, 1 action
>
>**Check:** Highest [ability] level of characters targeted.
>
>**Duration:** Instant
>
>**Target:** A group of characters, all of whom you can see and communicate with, who accept your assistance.
>
> **Effect:** All targets gains a bonus to their next use of [ability] equal to a quarter of your level, round down.
>
>**Failure:** Targets gets a negative bonus equal to however much you failed the check by.

### [Ability] Skill 20 - Master

**Category:** Ability Skill, Title

**Point Cost:** 20

**Prerequisite:** 20 levels in the chosen ability, at least one [Ability] Skill 19 aspect.

**Effect:** You may add an additional 1d6 to each of your rolls for this ability. Further, the character can rightly have a title that includes “Master”, and NPCs are likely to refer to them by it. 

### [Ability] Skill 21 - Incorporated Knowledge

**Category:** Ability Skill

**Point Cost:** 21

**Prerequisite:** 21 levels in the chosen ability, at least one [Ability] Skill 20 aspect.

**Effect:** So much of your knowledge is incorporated into your actions, that just by being around you, people can pick up information on the topic. You receive a bonus to Teach when teaching [ability] equal to half your level in [ability], rounded down.

### [Ability] Skill 22 - Student Magnetism

**Category:** Ability Skill

**Point Cost:** 22

**Prerequisite:** 22 levels in the chosen ability, at least one [Ability] Skill 21 aspect.

**Effect:** Once per month in game, check [ability], if the result is 50 or higher, the character will be approached by by a seeking student with [ability] at level 2D6, that the character may accept as a student, which will count as a ally with 50% loyalty.

### [Ability] Skill 23 - Diversification

**Category:** Ability Skill

**Point Cost:** 23

**Prerequisite:** 23 levels in the chosen ability, at least one [Ability] Skill 22 aspect.

**Effect:** When you take this aspect, you get 23 character points that must be spent on an ability that shares one or more higher abilities with [ability].

### [Ability] Skill 24 - Skilled Focus

**Category:** Ability Skill

**Point Cost:** 24

**Prerequisite:** 24 levels in the chosen ability, at least one [Ability] Skill 23 aspect.

**Effect:** With the chosen ability, at any time you may choose to double the preparation time (if cost is instant, increase to 1 action) to increase the roll result by +10. This may only be done once per roll.

### [Ability] Skill 25 - Ace

**Category:** Ability Skill, Title

**Point Cost:** 25

**Prerequisite:** 25 levels in the chosen ability, at least one [Ability] Skill 24 aspect.

**Effect:** With the chosen ability, at any time you may choose to take a -5 to the ability's checks in order to reduce the preparation's fatigue cost (if it fatigues itself) by 1. (This ability replaces Expert if you have it). The character is generally recognized for having attained this level of skill.

### [Ability] Skill 26 - Skilled Rush

**Category:** Ability Skill

**Point Cost:** 26

**Prerequisite:** 26 levels in the chosen ability, at least one [Ability] Skill 25 aspect.

**Effect:** With [ability], you may take a -5 to the ability's checks in order to cut preparation time in half. (You may only do this once per roll).

### [Ability] Skill 27 - Skilled Stability

**Category:** Ability Skill

**Point Cost:** 27

**Prerequisite:** 27 levels in the chosen ability, at least one [Ability] Skill 26 aspect.

**Effect:** With the chosen ability, at any time you may choose to double the preparation's fatigue cost (if cost is 0, increase to 1) to increase the roll result by +10. This may be done after the roll has been made, but only once per roll.

### [Ability] Skill 28 - Ace's High

**Category:** Ability Skill

**Point Cost:** 28

**Prerequisite:** 28 levels in the chosen ability, at least one [Ability] Skill 27 aspect.

**Effect:** When you roll for [ability], you may fatigue [ability] by 1, and reroll, keeping the higher roll. Only use this once per action, and declared before the roll is made.

### [Ability] Skill 29 - Enhanced Reliability

**Category:** Ability Skill

**Point Cost:** 29

**Prerequisite:** 29 levels in [Ability], at least one [Ability] Skill 28 aspect.

**Effect:** At any point you may replace any 1d6 of your a dice roll in [Ability] with a bonus of +4. This lowers the average, but it also removes all possibility of chance on your roll, making you able to perfectly predict how well you'll do.

### [Ability] Skill 30 - Ace Talent

**Category:** Ability Skill

**Point Cost:** 30

**Prerequisite:** 30 levels in [Ability], at least one [Ability] Skill 29 aspect.

**Effect:** You may add an additional 1d6 to each of your rolls for this ability.

### [Ability] Skill 31 - Exceptional ability

**Category:** Ability Skill

**Point Cost:** 31

**Prerequisite:** 31 levels in [Ability], at least one [Ability] Skill 30 aspect.

**Effect:** At any point you may replace any 2d6 of your a dice roll in [Ability] with 2d8, increasing your possible roll results.

### [Ability] Skill 32 - Complete Knowledge

**Category:** Ability Skill

**Point Cost:** 32

**Prerequisite:** 32 levels in [Ability], at least one [Ability] Skill 31 aspect.

**Effect:** You are completely knowledgeable in the subject. You know all the intricate ends-and-outs. If information on the subject is publicly available anywhere, you know it already, and you don't need to roll to pass a knowledge check on the subject.

### [Ability] Skill 33 - Contribution

**Category:** Ability Skill

**Point Cost:** 33

**Prerequisite:** 33 levels in [Ability], at least one [Ability] Skill 32 aspect.

**Effect:** At this point, not only have you fully mastered the skill, but you've made some significant contribution to the field, be it in the form of new records broken or new knowledge discovered, you've achieved something many aspire to. You have literally surpassed the rest of the world in some aspect. Anyone with at least 5 levels in [Ability] knows your name. Further, if applicable for any reason, you may treat your tech level as 0.2 higher.

### [Ability] Skill 34 - Grand Focus

**Category:** Ability Skill

**Point Cost:** 34

**Prerequisite:** 34 levels in the chosen ability, at least one [Ability] Skill 33 aspect.

**Effect:** With the chosen ability, at any time you may choose to double the preparation time (if cost is instant, increase to 1 action) to increase the roll result by +15. This may only be done once per roll.

### [Ability] Skill 35 - Grand Master

**Category:** Ability Skill, Title

**Point Cost:** 35

**Prerequisite:** 35 levels in the chosen ability, at least one [Ability] Skill 34 aspect.

**Effect:** With the chosen ability, at any time you may reduce the preparation's fatigue cost (if it fatigues itself) by 1. (This ability replaces the Ace Ability). The character is likely very well known for their level of ability.

### [Ability] Skill 36 - Grand Rush

**Category:** Ability Skill

**Point Cost:** 36

**Prerequisite:** 36 levels in the chosen ability, at least one [Ability] Skill 35 aspect.

**Effect:** With [ability], you may cut preparation time in half. (You may only do this once per roll).

### [Ability] Skill 37 - Grand Stability

**Category:** Ability Skill

**Point Cost:** 37

**Prerequisite:** 37 levels in the chosen ability, at least one [Ability] Skill 36 aspect.

**Effect:** With the chosen ability, at any time you may choose to double the preparation's fatigue cost (if cost is 0, increase to 1) to increase the roll result by +15. This may be done after the roll has been made, but only once per roll.

### [Ability] Skill 38 - Further Diversification

**Category:** Ability Skill

**Point Cost:** 38

**Prerequisite:** 38 levels in the chosen ability, at least one [Ability] Skill 37 aspect.

**Effect:** When you take this aspect, you get 38 character points that must be spent on an ability that shares one or more higher abilities with [ability].

### [Ability] Skill 39 - Advanced Reliability

**Category:** Ability Skill

**Point Cost:** 39

**Prerequisite:** 39 levels in [Ability], at least one [Ability] Skill 38 aspect.

**Effect:** At any point you may replace any 2d6 of your a dice roll in [Ability] with a bonus of +5. This lowers the average, but it also removes all possibility of chance on your roll, making you able to perfectly predict how well you'll do.

### [Ability] Skill 40 - Grand Talent

**Category:** Ability Skill

**Point Cost:** 40

**Prerequisite:** 40 levels in [Ability], at least one [Ability] Skill 39 aspect.

**Effect:** You may add an additional 1d6 to each of your rolls for this ability.

### [Ability] Skill 41 - Astounding ability

**Category:** Ability Skill

**Point Cost:** 41

**Prerequisite:** 41 levels in [Ability], at least one [Ability] Skill 40 aspect.

**Effect:** At any point you may replace any 2d6 of your a dice roll in [Ability] with 2d10, increasing your possible roll results.

### [Ability] Skill 42 - Apprentice Magnetism

**Category:** Ability Skill

**Point Cost:** 42

**Prerequisite:** 42 levels in the chosen ability, at least one [Ability] Skill 41 aspect.

**Effect:** Once per month in game, check [ability], if the result is 75 or higher, the character will be approached by by a seeking apprentice with [ability] at level 7D4, that the character may accept as an apprentice, which will count as a ally with 70% + 3d10 percent loyalty.

### [Ability] Skill 43 - Just Because

**Category:** Ability Skill

**Point Cost:** 43

**Prerequisite:** 43 levels in the chosen ability, at least one [Ability] Skill 42 aspect.

**Effect:** Once per gaming session, you may automatically roll maximum on your roll for [ability], or an [ability] skill aspect.

### [Ability] Skill 44 - Perfect Focus

**Category:** Ability Skill

**Point Cost:** 44

**Prerequisite:** 44 levels in the chosen ability, at least one [Ability] Skill 43 aspect.

**Effect:** With the chosen ability, at any time you may choose to double the preparation time (if cost is instant, increase to 1 action) to increase the roll result by +20. This may only be done once per roll.

### [Ability] Skill 45 - Extended Complete Knowledge

**Category:** Ability Skill

**Point Cost:** 45

**Prerequisite:** 45 levels in [Ability], at least one [Ability] Skill 44 aspect.

**Effect:** You are completely knowledgeable in the subject. You know all the intricate ends-and-outs. In addition to knowing any publicly knowable information, it's not that hard to make an educated guess on non-publicly available information when it comes to your subject. Even if information is a secret, if it deals directly with a subject related to your ability, you may still make a knowledge check about it.

### [Ability] Skill 46 - Perfect Rush

**Category:** Ability Skill

**Point Cost:** 46

**Prerequisite:** 46 levels in the chosen ability, at least one [Ability] Skill 45 aspect.

**Effect:** With [ability], you may cut preparation time in half. (You may only do this once per roll). 

### [Ability] Skill 47 - Guild Leader

**Category:** Ability Skill, Title

**Point Cost:** 47

**Prerequisite:** 47 levels in the chosen ability, at least one [Ability] Skill 46 aspect.

**Effect:** At this point, if you have gotten at least 10 Apprentices, all of these apprentices will be taking on their own students and some their own apprentices. At this point, you are considered the leader of the organization with an average of loyalty 70% and a typical membership of 6D20 members. If you, yourself, are already part of an organization, you will be considered the lead of your own chapter, and have relative autonomy.

### [Ability] Skill 48 - Field Diversification

**Category:** Ability Skill

**Point Cost:** 48

**Prerequisite:** 48 levels in the chosen ability, at least one [Ability] Skill 47 aspect.

**Effect:** You get 48 character points when you take this aspect that must be spent on an ability that shares one or more higher abilities with [ability].



### [Ability] Skill 49 - Perfect Reliability

**Category:** Ability Skill

**Point Cost:** 49

**Prerequisite:** 49 levels in [Ability], at least one [Ability] Skill 48 aspect.

**Effect:** At any point you may replace any 2d6 of your a dice roll in [Ability] with a bonus of +6. This lowers the average, but it also removes all possibility of chance on your roll, making you able to perfectly predict how well you'll do.

### [Ability] Skill 50 - Genius Savant

**Category:** Ability Skill, Title

**Point Cost:** 50

**Prerequisite:** 50 levels in [Ability], at least one [Ability] Skill 49 aspect.

**Effect:** Upon reaching this skill level, you are recognized as one of the greatest in your field. Even people not in your field will recognize your name almost instantly. Upon reaching this point, your name has taken on a legendary status. At this point, your name itself will be an unofficial title for skill. (e.g. for science skill, "He's an Einstein" or for beauty "She's a real Helen of Troy", etc.) Whenever you speak on the topic, people will listen, and any listener with less than 25 in [ability] will take your word for anything you say in the area unless another Genius Savant in the same ability contradicts you. No social roll needs to be made. You also get an additional 1d6 anytime you roll for [ability].



### [Ability] Skill [X] - Synergy

**Category:** Ability Skill

**Point Cost:** [X] times 2 for Skills, [X] times 4 for 3rd Tier abilities, [X] times 8 for 2nd Tier abilities, and [X] times 16 for 1st Tier abilities.

**Prerequisite:** [x] levels in [Ability], at least one [Ability] Skill [x-1] aspect.

**Effect:** This not only counts as an [Ability] Skill [X] for [Ability], but for the purpose of prerequisites, it also counts  as an [Ability] Skill [X] aspect for the lower abilities of [Ability].



Example: 

Taking [Body] Skill [3] – Synergy

Costs 24 points and counts as a Body Skill 3 Aspect for Body, Speed, Finesse, Toughness, Might, and every Skill that has Body as a higher ability that you have 3 or more levels in, such as Accuracy, Armor, Arms [Fast], Deflection, Sneak, Travel, Drive, etc. The use of this Aspect is a cheap way to cover a lot of aspects for generalist character builds.



Accuracy Skill 1 - Focus

**Category:** Ability Skill, Instant

**Point Cost:** 1

**Prerequisite:**  1 level in accuracy

**Action:**

> ####> ####> ####Focus
>
>**Preparation:** Choose a bonus desired, 1 turn prep for every bonus you're attempting to add to a roll and fatigue accuracy by the same amount.
>
>**Check:** Add 12 for every bonus you're attempting to add to a roll.
>
>**Duration:** Instant
>
>**Target:** A physical action you're about to take that has finesse as a higher ability.

**Effect:** When taking the action, you get the attempted bonus.
>
>**Failure:** You receive a negative equal to the bonus you attempted.

Note: This ability may not be applied to Accuracy.

### Accuracy Skill 8 - Professional Excellence

**Category:** Ability Skill, Instant

**Point Cost:** 8

**Prerequisite:**   8 levels in Accuracy, and one Accuracy skill 7 aspect

**Action:**

> #### Aiming
>
>**Preparation:** 1 action, fatigue accuracy by 1
>
>**Duration:** Until after your next non-aiming action.
>
>**Target:** An action that uses a finesse-based ability you plan to take.

**Effect:** Reduce the minimum roll for an Excellent effect by 1.

### Ally

**Point Cost:** Free

**Prerequisite:** Have characters you’re teammates with.

**Effect:** Instead of your actions having to be separately calculated, your team may group their actions together.

**Category:** Free

Roleplaying Tips: 

### Amnesia

**Point Cost:** -5

**Prerequisite:** May only be taken at a time when mind damage is at100%.

The character may rearrange the character points of all personality aspects into other personality aspects. Also, the points gained from Amnesia may only be spent on personality aspects. Also, when amnesia is taken, any roll for sanity or memory for information before amnesia was gained takes a -10 penalty.

Amnesia may be paid off, getting rid of Amnesia. When Amnesia is removed, personality aspects may be rearranged again, but only between current personality aspects and ones that the character has had before.

**Category:** Personality

*Roleplaying Tips: People with Amnesia generally come in two categories. Those who try to figure out their past, and those who are worried/annoyed by what their past may be as they try to move forward. Either way, they both have a tendency to be always unsure about the world around them. Wondering who knows them, who doesn't, and what unknowns are lurking everywhere.*

### Archaeology Skill 2 - Survey

"One of the secrets of archaeology is when an archaeologist says a site served religious purposes is that most ancient religions were built on basic facts on practical application of ideals and religion permeated all of their daily life. Saying a site served a religious purpose and leaving it at that is the archaeologist's equivalent of saying, 'I have no clue.'" 

**Category:** Ability Skill

**Point Cost:** 2

**Prerequisite:**  2 levels in archaeology, and one archaeology skill 1 aspect.

**Action:**

> #### Quick Survey
>
>**Preparation:** 1 minute of studying a site; fatigue archaeology by 1
>
>**Check:** Age of site in years divided by 25 (round down)
>
>**Duration:** Permanent
>
>**Target:** A site of ruins or site of similar archaeological interest that you haven't used quick survey on.
>
>**Effect:** Learn basic function and purpose of site
>
>**Failure:** If check fails by more than 10, incorrect information is gained.
>
>**Special:** Prep time may be intentionally increased in increments. For every 8 hours of added time, increase the check result of a roll by 5.

### Armor Skill 4 - Shield Glance

**Category:** Ability Skill

**Point Cost:** 4

**Prerequisite:**  4 levels in Armor, and one armor skill 3 aspect

**Action:**

> #### Shield Glance
>
>**Preparation:** Instant, Fatigue Armor by 1, have a shield capable of blocking the attack you're targeting
>
>**Check:** Contest check used for an attack which targets you 
>
>**Duration:** Instant
>
>**Target:** An attack which targets you
>
> **Effect:** The attack misses you, landing to your side instead.

### Armor Skill [x] - [Armor Type] Familiarity

**Category:** Ability Skill

**Point Cost:** [x]

**Prerequisite:**  [x] levels in Armor, and one armor skill [x-1] aspect, and no Armor Skill [x] - [Armor Type] Familiarity aspects with the same value of [x].

**Effect:** Choose an armor type, you may treat your armor as if it weighed 1kg less. 

**Special:** This aspect may be taken multiple times, but x must have a different value each time. 

### Armor Skill 15 - Armor Fluidity

**Category:** Ability Skill

**Point Cost:** 15

**Prerequisite:**  15 levels in Armor, and one armor skill 14 aspect, and at least four Armor Skill [x] - [Armor Type] familiarity aspects.

**Effect:** With Armor types that you have 4 Armor Skill [x] in, you may ignore a negative it gives to one of your Third Tier abilities (declare which ability when you take this aspect). 

**Special:** This aspect may be taken multiple times. Further, it only applies once to your whole set of armor (each region does not get the effect separately!)

### Arms [Weapon Category] Skill 1 - Weapon Attack

In combat, your best friend is whoever is at your side. Your second best friend, however, is your weapon.

**Category:** Ability Skill

**Point Cost:** 1

**Prerequisite:**  1 level in Arms [Weapon Category]

**Action:**

> #### Weapon Attack
>
>**Preparation:** 1 action, fatigue Arms [weapon category] by 1 + 1 for each KG of weight of weapon beyond your strength, be armed with a possession with [weapon category]
>
>**Check:** Contest against target's Dexterity
>
>**Duration:** Instant
>
>**Target:** A character that is next to you that can be affected by your weapon (or within the weapon's range if the weapon has a range ability).
>
>**Effect:** Deal the weapon's effect to the target
>
>**Excellent Effect:** Deal the weapon's excellent effect to the target if it has one. 
>
>**Failure:** Weapon does not deal effect,
>
>**Excellent Failure:** Do the weapons' Excellent failure effect. If there isn't one, deal the weapon's effect to the target.
> 
> **Fatal Failure:** If the weapon has a fatal effect, apply it. Otherwise, roll a d4 and do the following. 
> 
> 1. Hit yourself... deal weapon's effect to yourself.
> 
> 2. Hit ally... randomly deal weapon's effect to an ally.
> 
> 3. Weapon deals its effect to itself.
> 
> 4. Drop weapon... your weapon goes flying, 1d10 meters away from you in a random direction.

Note: The base weapon categories in the players guide are [Fast], [Heavy], [Melee], and [Ranged]. However, they are not the only weapon categories possible, and a weapon may belong to multiple categories.



![Accuracy](./img/Accuracy.jpg)

### Arms [Weapon Category] Skill 5 – Vital Aim

You are learning to adapt your knowledge of the sword to more than just the training room. You are intuitively learning the body's weak points and how to get to them.

**Category:** Ability Skill

**Point Cost:** 5

**Prerequisite:**  5 levels in Arms [Weapon Category], one Arms [Weapon Category] Skill 4 Aspect

**Effect:** 

You may increase the roll range your vital strike would normally happen in. (For example, if your weapon normally on gets vital strike on a 12, it instead gets it on 11-12. If it already gets it on 11-12, you get it 10-12. Etc.)

### Arms [Weapon Category] Skill 8 – Vital Focus

As your combat prowess improves, so does your ability to get reliable results.

**Category:** Ability Skill

**Point Cost:** 8

**Prerequisite:**  8 levels in Arms [Weapon Category], one Arms [Weapon Category] Skill 7 Aspect

**Effect:** You may cause vital strike at twice the range your normally would. (For example, if your weapon normally on gets vital strike on a 12, it instead gets it on 11-12. If it already gets it on 11-12, you get it 9-12. Etc.)

### Arms [Weapon Category] Skill 10 - Forcethrough

You know that in many forms of combat, your enemy's armor is more dangerous than the enemy themselves. You have studied the armor, and its weak points.

**Category:** Ability Skill

**Point Cost:** 10

**Prerequisite:**  10 level in Arms [Weapon Category], one Arms [Weapon Category] Skill 9 Aspect

**Action:**

> #### Forcethrough
>
>**Preparation:** Fatigue Arms [weapon category], be armed with a possession with [weapon category]
>
>**Check:** Automatic
>
>**Duration:** Instant
>
>**Target:** An attack made by you.
> 
>**Effect:** Sacrifice 1 follow-through with the weapon to "restore" the weapon's Vital Strike chance after its been reduced by an armor's Vital Protection

### Arms [Weapon Category] Skill 12 – Vital Striker

Your combat style is not random or clumsy, but a more elegant style that gives you the control you need to complete the job efficiently and quickly.

**Category:** Ability Skill

**Point Cost:** 12

**Prerequisite:**  12 levels in Arms [Weapon Category], one Arms [Weapon Category] Skill 11 Aspect

**Action:**

> #### Vital Strike
>
>**Preparation:** Fatigue Arms [weapon category], have just completed an attack that hit the target with a [weapon category] weapon.
>
>**Check:** Automatic
>
>**Duration:** Instant
>
>**Target:** An attack made by you.
>
> **Effect:** Even if you did not roll maximum, you still get to do the vital strike.

### Arms (Melee) Skill 3 - One-Hand Fighter

You are skilled at using a single weapon and the additional balance it provides. The use of free hands allows you to use the extra hand for balance, to stabilize your weapon more, or to add extra momentum when you attack, making your swing faster.

**Category:** Ability Skill

**Point Cost:** 3

**Prerequisite:**  3 levels in Arms [Melee], one Arms [Melee] Skill 2 aspect

**Effect:** As long as whatever weapon you use is used is a one-handed weapon and any other hands remain empty, you may treat your Weapon Fighting ability as two levels higher, including aspect prerequsites. This includes passing requirements for Arms [Melee] aspects. If you choose to wield more than a single one-hand weapon, this aspects effect does not apply (and while doing so, you lose access to any aspects you no longer meet the prerequisite for.)

### Arms (Fast) Skill 8 - Akimbo Fighter

Sometimes, fighting with two weapons is better than one.

**Category:** Ability Skill

**Point Cost:** 8

**Prerequisite:**  8 levels in Arms [Fast], one Arms [Fast] Skill 7 aspect

**Effect:** If you wield multiple weapons and every weapon you wield is affected by an Akimbo Fighter aspect, you receive an extra action every turn of intense action. This action may only be used on Arms actions for weapons you haven't used that turn.

### Artistry Skill 10 - Sellable

**Category:** Ability Skill

**Point Cost:** 10

**Prerequisite:**  10 levels in artistry, and one artistry skill 9 aspect.

**Effect:**

You gain the ability to create a piece of artwork. Creating the artwork can take any amount of time you choose. Assume the typical income for this is the same as earning quarter of the average income for the hours you worked. The artwork gives a positive or negative social modifier to those who view it of your specification equal to half your artistry check in making it while they view it.

### Artistry Skill 20 - Famous Artist

**Category:** Ability Skill

**Point Cost:** 20

**Prerequisite:**  20 levels in artistry, and one artistry skill 19 aspect, the "Artistry Skill 10 - Sellable" aspect.

**Effect:**

Your artwork sells instead for the numbers of hours you worked, times the typical income for an hour's work, times half your artistry check.

*Roleplaying Tips: Famous artists are often (but not always) egotistical.  To become a famous artist is a long and difficult, and often lonely, road. That long struggle makes the artists who succeed certain they are the cream of the crop and they know that to sell, they have to be perceived as top tier skill.*

### Beauty Skill 2 - Impressions

One of the key points of using physical beauty is to make a good first impression.

**Category:** Ability Skill

**Point Cost:** 2

**Prerequisite:**  2 levels in Beauty, and one Beauty skill 1 aspect.

**Action:**

> #### First Impression
>
>**Preparation:** 1 action, Fatigue Beauty by 2
>
>**Check:** Contest Target Player's Wisdom 
>
>**Duration:** Permanent
>
>**Target:** Any Character you have not used First Impression on before who could potentially be attracted to you.
>
> **Effect:** Get a +1 assertive social modifier when dealing with the targeted character.
>
>**Failure:** Get a -1 assertive social modifier when dealing with the targeted character.

### Beauty Skill 5 - Flaunt

Part of using one's natural looks is to cause a visual comparison between you and someone else. The key, however, is to make it look unintentional, for it's all lost if you look like a jerk while doing so.

**Category:** Ability Skill

**Point Cost:** 5

**Prerequisite:**  5 levels in Beauty, and one Beauty skill 4 aspect.

**Action:**

> #### Flaunt
>
>**Preparation:** 1 action, fatigue beauty by 1
>
>**Check:** Contest Target Character's Beauty
>
>**Duration:** 1/day
>
>**Target:** Another character.
>
> **Effect:** Get a +1 social assertive modifier with any viewing who could potentially be attracted to both you and your target. The target receives a -1 social assertive modifier.
>
>**Failure:** Get a -1 social assertive modifier when dealing with target and any viewing.

### Beauty Skill 10 - Fine Grooming

A number of people may look naturally good, but with effort, that good can be accentuated, while masking the bad to make a more alluring image.

**Category:** Ability Skill

**Point Cost:** 10

**Prerequisite:**  10 levels in Beauty, and one Beauty skill 9 aspect.

**Effect:**

For every thirty minutes you spend grooming, you may get a +1 bonus to your beauty checks that lasts the rest of the day.

Roleplaying Tips: People who do fine grooming know what they are doing is purely for appearance. As such, people who learn it are usually highly interested in pursuing social goals, and usually appearances are extremelly important to them.

### Being Skill 0 – Being Centering

**Category:** Instant

**Point Cost:** 0

>**Preparation:** Fatigue Being 1 (cannot be altered by other aspects) 
>
>**Duration:** Instant
>
>**Target:** Self
> 
> **Effect:** Reroll any one roll, give a bonus  to a roll equal to your level in Being, or reduce damage taken by your level in Being.



### Bleeding Heart

**Category:** Personality

**Point Cost:** -8

**Effect:** People attempting to persuade you do not apply their negative assertive social modifiers.

Roleplaying Tips: Bleeding Hearts are extra attentive to any sad story. They want to help everyone, and believe everyone, generally. They are often compassionate, but also often appear clueless to others. 

### Body Skill 0 – Body Centering

**Category:** Instant

**Point Cost:** 0
>
>**Preparation:** Fatigue Body 1 (cannot be altered by other aspects) 
>
>**Duration:** Instant
>
>**Target:** Self
>
> **Effect:** For a body roll, reroll the roll, give a bonus  to a roll equal to your level in Body, or reduce Body damage taken in by your level in Body.

### Bonus Language (*)

**Category:** Mental

**Point Cost:** 5/8/12/25

* Choose a language, and fluency

**Prerequisite:** Extended time in an area that uses the language or part of a culture that would have access to knowledge of the other language. Further, you must be part of a species that has language and you must have 3 levels in conversation.

**Effect:**

***Fluency:***

For cost of 5 – Halting – You speak or read-write the language badly (choose which when gaining this aspect), but enough to communicate.

For cost of 8 – Fluent – You speak and read/write the language with a thick accent (and occasionally noticeably improper grammar)

For cost of 12 – Native – You can speak and read/write the language correctly, without a detectable accent. (This is the level that characters get by default in their native language).

For cost 25 – Expert – You can speak any dialect and read/write any dialect of the language perfectly. (This includes mimicking accents, able to talk and understand complicated 'tech lingo', and other things).

**Special:** A character with multiple languages has the ability to translate between them, but will use the less fluent of the two for determining fluency of the translation.

### Cartography Skill 5 - Find Position

**Category:** Ability Skill

**Point Cost:** 5

**Prerequisite:**  5 levels in Cartography, and one Cartography skill 4 aspect.

**Action:**

> #### Find Position
>
>**Preparation:** Study surroundings for 1 minute, have access to maps and/or similar items, fatigue cartography by 6
>
>**Check:** 50, -5 for each discernible geographic feature (Note: The GM does not have to reveal this value.)
>
>**Duration:** Instant
>
>**Target:** Self
>
>**Effect:** Know your exact location relative to other places you know the location of.
>
>**Failure:** You think you know your location relative to other places you know the location of, but don't.

### Cartography Skill 10 – Stellar Cartography

**Category:** Ability Skill

**Point Cost:** 10

**Prerequisite:**  10 levels in Cartography, and one Cartography skill 9 aspect, Cartography Skill 5 - Find Position Aspect

**Effect:** If you can see the sky, discernable stellar objects such as the sun, moon, and constellations can count towards the geographic features needed for the Find Position action.

### Cartography Skill 12 - Geographic Memory

**Category:** Ability Skill

**Point Cost:** 12

**Prerequisite:**  12 levels in Cartography, and one Cartography skill 11 aspect, Cartography Skill 5 - Find Position Aspect

**Effect:** You do not need cartography equipment to do find position actions.

### Cartography Skill 18 - Material Survey

**Category:** Ability Skill

**Point Cost:** 18

**Prerequisite:**  18 levels in Cartography, and one Cartography skill 17 aspect, 10 levels in Identification, 5 levels in survival

**Effect:** You may search a region for a material that could occur in that region. (The material must be chosen before surveying begins) Searching the region takes one day per square kilometer. At the end of the Survey, make a Cartography check versus a difficulty of 30. If you succeed you know whether or not the material can be harvested in that area.



Whether or not the material is there is based on a GM roll using the material's rarity. If the material is not there, the GM may optionally inform you of up to 6 other materials that are. 



A region only needs to have the presence of the material successfully checked for once. If checked again for the same material, the result will not change unless something about the region itself has changed significantly (such as a meteor strike, massive avalanche/earthquake, mining can remove presence of the material, etc.) 



The amount of the material discovered should be set by the GM based on its rarity (usually 1d100 times its rarity in metric tons.)

### Child

**Category:** Physical

**Point Cost:** -20

**Prerequisite:** Be able to have a childhood.

**Effect:** You are half the size of a normal member of your species. You also get a -5 to all assertive social modifiers  (unless a plead for personal assistance, where you get a +5) However, all learn and exercise  rolls have their results doubled. Any strength, toughness or wisdom rolls (or rolls for their lower abilities) are halved. Further, any social restrictions to children in your society apply to you.

*Note: When your character gets old enough to no longer be considered a child, you are expected to pay 20 character points and remove this aspect.*

### Cold Spirited

**Category:** Personality

**Point Cost:** 15

**Prerequisite:** 3 levels in Sanity

**Effect:** A character who is cold spirited has a difficult time being swayed in their actions in any way. In situations where others are trying to influence their actions, their Sanity is considered 2 higher. 

*Roleplaying Tips: Cold Spirited people have a tendency to come off as uncaring by other people. This isn't necessarily the case (although there are definitely uncaring cold spirited people). Those who are cold spirited merely put logic before emotions.* 

### Confident Street Orphan

Sure, you were raised on the street. Another young one doomed to a failed life, that's what they all thought. They all looked down on you. But their pity, their stares, their sneers only fueled your desire to make something of yourself. You're going to be amazing, no matter what it takes, and prove your worth to the cold harsh world, and prove all those stares wrong.

**Category:** Background & Destiny

**Point Cost:** 20

**Prerequisites:** You cannot have the Family Member, Guardian aspect

**Effect:** You gain 1 destiny point every game session.

### Conversation Skill 6 - Fish for information

**Category:** Ability Skill

**Point Cost:** 6

**Prerequisite:**  6 levels in Conversation, and one Conversation skill 5 aspect.

**Action:**

> #### Fish for Information
>
>**Preparation:** 1 minute of talking, fatigue conversation by 1
>
>**Check:** Target's morality (apply social modifiers)
>
>**Duration:** Instant
>
>**Target:** 1 character you can communicate with
>
> **Effect:** Learn one of the following:
>
> * If they have a loyalty Aspect, and if so, what it is.
>
> * Know what one social modifier they have is and for how much.
>
> * The levels the character has in one of their abilities.
>
> * A piece of information if they know (the GM may assign bonuses for information that they wish to keep secret, applicable bonuses include their sanity or  IQ level and morality bonuses for associated topics).
>
>**Failure:** No further conversation uses of yours may be made against the target today



### Conversation Skill 25 - Linguist

You are a student of language, and have learnt not only languages, but the patterns behind languages, allowing you to quickly pick up new ones.

**Category:** Ability Skill

**Point Cost:** 25

**Prerequisite:**  25 levels in Conversation, and one Conversation skill 24 aspect, 2 bonus languages at the 25 point cost.

**Effect:** 

Each additional bonus language from this point on costs half its normal cost.

### Cooking Skill 5 - Healthy Meal

**Category:** Ability Skill

**Point Cost:** 5

**Prerequisite:**  5 levels in Cooking, and one Cooking skill 4 aspect.

**Action:**

> #### Healthy Meal
>
>**Preparation:** Choose x minutes to spend preparing the food, have cooking materials on hand
>
>**Check:** x times 2
>
>**Duration:** Permanent (Creates 1 use item of food)
>
>**Target:** A 2nd Tier ability (Mind or Body)
>
> **Effect:** When eaten (which takes a minimum of 15 minutes), consumed food restores x fatigue to target ability. Food must be consumed within 1 hour.
>
>**Failure:** Consumption of food causes x fatigue
>
> **Auto-Failure:** Happens if the person eating cannot digest all ingredients in food made (e.g. allergies, food intolerances, etc.)

### Cooking Skill 10 - Specialty Cooking

**Category:** Ability Skill

**Point Cost:** 10

**Prerequisite:**  10 levels in Cooking, and one Cooking skill 9 aspect.

**Action:**

> #### Specialty Meal
>
>**Preparation:** Choose x minutes to spend preparing the food, have cooking materials on hand
>
>**Check:** x times 4
>
>**Duration:** Permanent (Creates 5 items of food)
>
>**Target:** A 3rd or 4th Tier ability
>
> **Effect:** When eaten (which takes a minimum of 10 minutes), consumed food restores x fatigue to target ability. Food must be consumed within 1 hour.
>
>**Failure:** Consumption of food causes x fatigue
>
> **Auto-Failure:** Happens if the person eating cannot digest all ingredients in food made (e.g. allergies, food intolerances, etc.)

### Cooking Skill 15 - Travel Cooking

**Category:** Ability Skill

**Point Cost:** 15

**Prerequisite:**  15 levels in Cooking, and one Cooking skill 14 aspect.

**Action:** 

> #### Travel Cooking
>
>**Preparation:** Choose x minutes to spend preparing the food, have cooking materials on hand
>
>**Check:** x times 6
>
>**Duration:** Permanent (Creates 1 use item of food)
>
>**Target:** Any one ability
>
> **Effect:** When eaten (which takes 1 action), consumed food restores x fatigue to target ability. Food may be stored a number of months gametime equal to x. 
>
>**Failure:** Consumption of food causes x fatigue
>
> **Auto-Failure:** Happens if the person eating cannot digest all ingredients in food made (e.g. allergies, food intolerances, etc.)

### Craft Skill [X] - [Industry]

**Category:** Ability Skill

**Point Cost:** [X]

**Prerequisite:**  [X] levels in Craft, and one Craft skill [X-1] aspect, and no other Craft Skills of the same level, GM permission

**Effect:** With the proper set of tools for a chosen industry, you can create possessions of that industry. 



You may construct anything if you have plans readily available for how to create it and it's in your industry, as long as it does not require a tech level higher than yours (this has the exception of if you designed it yourself, such as using the design ability, you do not have to meet tech requirements.)



**Action:**
>
>**Preparation:** Plans for the possession  must be available, must have amount of crafting materials equal to weight of possession (used up in process), number of hours equal to possession's creation difficulty (each additional hour spend in preparation adds a +1 bonus to your roll). 
>
> *Note: Crafting materials can be substituted by paying one third of the possession's normal cost - which substitutes as paying for the materials.*
>
>**Check:** Possession's Creation Difficulty
>
>**Duration:** Permanent (Creates Possession)
>
>**Target:** Plans
>
> **Effect:** Possession is created. 
>
>**Failure:** Possession not created, ½ materials (round down) wasted. 1% chance (roll 100 on d100) of a successful failure.
>
> **Successful Failure:** Possession is not made exactly as planned. But thanks to a fortunate accident, the possession works differently, but significantly better than planned. GM may determine effects. (If GM does not add an effect, instead choose a single numerical value of the possession and either double it or halve it.)
>
**Note: Example industries include weapon-smithing, house building, carpentry, masonry, etc.**



### Craft Skill 5 - Locksmithing

**Category:** Ability Skill

**Point Cost:** 5

**Prerequisite:**  5 levels in Craft, and one Craft skill 4 aspect, and at least one existing Craft Skill in the Locksmithing Industry

**Effect:** With locksmithing tools, you can create locks. 



**Action:**
>
>**Preparation:** Must have amount of crafting materials equal to weight of possession (used up in process). 1 hour for each kilogram the lock weighs. 
>
>*Note: Crafting materials can be substituted by paying one third of the possession's normal cost - which substitutes as paying for the materials.*
>
>**Check:** Material’s damage resistance.
>
>**Duration:** Permanent (Creates Possession)
>
> **Effect:** Lock is created. Lock has a difficulty to pick equal to your check result.
>
>**Failure:** Possession not created, ½ materials (round down) wasted. 1% chance (roll 100 on d100) of a successful failure.

*Note: Example industries include weapon-smithing, house building, carpentry, masonry, etc.*

### Creativity Skill 0 – Creativity Centering

**Category:** Instant

**Point Cost:** 0

**Action:**
>
>**Preparation:** Fatigue Creativity 1 (cannot be altered by other aspects) 
>
>**Duration:** Instant
>
>**Target:** Self
>
> **Effect:** Reroll 1 roll of Creativity or one of its lower abilities, or reduce a vital strike chance by your level in Creativity.

### Cultural Familiarity [Nationality]

You have lived with, and around, a culture enough that even though you may not understand how it works as a whole, you have a basic understanding of some of the major things you need to know about it, either from direct or indirect experience. 

**Category:** Nationality

**Prerequisite:** GM permission, must have spent at least two years among the culture in question.

**Point Cost:** 5 times the difference in Tech Level of your nationality and the chosen one (minimum 5).

**Effect:** For the purposes of any prerequisites, you can count as a member of the chosen nationality as well as your own.

### Damaged Appearance (negative)

**Point Cost:** - 3/level

**Effect:** You are physically disfigured in a distracting or horrifying way. Upon first meeting anyone who is a member of your species, you get a -1 to all your assertive rolls to interact with them in a social nature . For every 3 levels of Damaged Appearance, you permanently have an additional -1 in dealing with members of your species socially. For every 5 levels, you have an additional -1 in dealing with members of all species socially.

**Category:** Physical – appearance

### Dance Skill 2 - Let Loose

Sometime, just busting out with some moves can ease the mind and let the stress melt away.



**Category:** Ability Skill

**Point Cost:** 2

**Prerequisite:** 2 levels in dance, one Dance Skill 1 aspect

**Action:** 
>
>**Preparation:** Fatigue Dance by 1, Fatigue Body by 1, 1 minute
>
>**Check:** 10 (or 5 if there is music playing)
>
>**Duration:** Instant
>
>**Target:** Self
>
> **Effect:** Restore 1 Sanity Fatigue, Restore 1 Mind Fatigue
>
>**Failure:** -1 on social rolls (both assertive and defensive) for the next minute

### Debate Skill 2 - Initiate Debate

More than just argument, you're skilled at public debate, and swaying the opinions of those around you.



**Category:** Ability Skill

**Point Cost:** 2

**Prerequisite:** 2 levels in debate, one Debate Skill 1 aspect.

**Action:** 

> #### Initiate Debate
>
>**Preparation:** 1 turn, Fatigue Debate  by 1
>
>**Check:** Contest Target's Morality (apply social modifiers)
>
>**Duration:** Until debate ends
>
>**Target:** Character(s) you can communicate with
>
> **Effect:** You begin a debate with the target. By controlling the starting point, you receive a +1 assertive social modifier for the debate.
>
>**Failure:** Get a -1 social assertive modifier when dealing with target.



> #### Enter Debate
>
>**Preparation:** 1 turn, Fatigue Debate 1
>
>**Check:** Contest Debate of a character in the argument
>
>**Duration:** Instant
>
>**Target:** A debate you can interact with.
>
> **Effect:** Become part of a debate currently going on.



> #### End Debate
>
>**Preparation:** 1 turn
>
>**Check:** Contest Debate of a character in the argument + their remaining debate fatigue.
>
>**Duration:** Instant
>
>**Target:** A debate you can interact with.
>
> **Effect:** The debate ends, and all characters in debate regain half of their debate fatigue (rounded down).

### Debate Skill 4 - Set Pretenses

You have skill at controlling the 'key issue' of a debate.



**Category:** Ability Skill

**Point Cost:** 4

**Prerequisite:**  4 levels in Debate, one Debate Skill 3 aspect

**Action: **

> #### Set Pretenses
>
>**Preparation:** 1 turn, must be in a debate, fatigue debate by 1
>
>**Check:** Auto-pass
>
>**Duration:** Until debate ends
>
>**Target:** Everyone in the debate
>
> **Effect:** You get a +1 modifier for the rest of the argument (these bonuses will be specifically referred to as 'pretense modifiers'.)

### Debate Skill 5 - Character reference

You have made an art out of making a debate personal.



**Category:** Ability Skill

**Point Cost:** 5

**Prerequisite:** 5 levels in Debate, one Debate Skill 4 aspect.

**Action: **

> #### Appeal Reputation
>
>**Preparation:** 1 turn, must be in a debate
>
>**Check:** Contest your Morality vs Morality of target (apply social modifiers)
>
>**Duration:** Until end of next turn
>
>**Target:** Opponent in debate
>
> **Effect:** Lose a -1 social assertive modifier when dealing with target for rest of argument, double your next roll for debate
>
>**Failure:** Permanently lose a +1 social assertive modifier when dealing with target, and with all debate viewers.



> #### Anger Opponent
>
>**Preparation:** 1 turn, must be in an argument
>
>**Check:** Contest Debate vs Target's Personality (apply social modifiers)
>
>**Duration:** Until argument ends
>
>**Target:** Anyone or Everyone in the argument
>
> **Effect:** Target gets a -1 assertive and defensive social modifiers for rest of argument as well as an additional -1 Sanity they've accumulated due to anger.



> #### Humiliate Opponent
>
>**Preparation:** 1 turn, must be in an argument
>
>**Check:** Contest Argue vs Target's Sanity (apply social modifiers)
>
>**Duration:** Until argument ends
>
>**Target:** Character in argument
>
> **Effect:** Observing characters get a +1 defensive social bonus towards the target for the rest of the day, target takes one fatigue to pride, you win the argument if the target has completely fatigued pride. Target gains a +1 defensive social modifier for rest of argument.

### Debate Skill 8 - Break Logic

Finding the flaws in the way others think is something you have a knack for.



**Category:** Ability Skill

**Point Cost:** 8

**Prerequisite:**  8 levels in Debate, one Debate Skill 7 aspect

**Action: **

> #### Break Logic
>
>**Preparation:** 1 turn, must be in a debate
>
>**Check:** Contest Debate vs Target's Debate (apply social modifiers)
>
>**Duration:** Until debate ends
>
>**Target:** Character in debate
>
> **Effect:** Remove all of target's pretense modifiers
>
>**Failure:** Remove all of your pretense modifiers.

### Debate Skill 10 - Call for Conclusion

You're good at ending a debate at the best possible moment to make you look good, or to keep up damage control.



**Category:** Ability Skill

**Point Cost:** 10

**Prerequisite:**  10 levels in Debate, one Debate Skill 9 aspect

**Effect:** Gain following actions:

**Action: **

> #### Concede
>
>**Preparation:** 1 turn, must be in a debate
>
>**Check:** Auto-pass
>
>**Duration:** Instant
>
>**Target:** Debate you are in
>
> **Effect:** Officially lose argument, halve any negative social modifiers you personally gained. Opponent gains a permanent +2 social modifier with anyone watching. Restore half of any debate fatigue lost during argument.



> #### Dramatic Conclusion
>
>**Preparation:** 1 turn, must be in a debate, must have at least 5 social modifiers gained during the course of the argument
>
>**Check:** Contest Debate vs Target's Debate (apply social modifiers)
>
>**Duration:** Permanent
>
>**Target:** Opponent in debate
>
> **Effect:** Officially win debate, gain permanent bonus to social assertive modifier with all those in or watching argument equal to half your temporary assertive social modifiers gained from argue this argument.

### Debate Skill 12 - Change Light

You're skilled at changing the flow of a debate by changing how people think about it.



**Category:** Ability Skill

**Point Cost:** 12

**Prerequisite:**  12 levels in Debate, one Debate Skill 11 aspect

**Effect:** Gain following actions:

**Action:** 

> #### Consolidate Argument
>
>**Preparation:** 1 turn, must be in an argument, fatigue debate by 1
>
>**Check:** Auto-pass 
>
>**Duration:** Until argument ends
>
>**Target:** Characters in argument
>
> **Effect:** Convert all pretense modifiers into either temporary assertive or temporary defensive social modifiers for the rest of the argument.



> ####Marginalize Topic
>
>**Preparation:** 1 turn, must be in an argument, must be in first turn of a debate, fatigue debate by 1
>
>**Check:** Auto-pass
>
>**Duration:** Instant
>
>**Target:** Character in argument
>
> **Effect:** End debate with no winner or loser



> #### New Evidence
>
>**Preparation:** Must be in a debate, must have GM approved evidence that hasn't been presented previously
>
>**Check:** Contest Debate vs Target's Debate (apply social modifiers)
>
>**Duration:** Until debate ends
>
>**Target:** Character(s) in debate
>
> **Effect:** Receive a +5 assertive social modifier, if done immediately after a debate would end, the debate continues  where it left off as if it did not end, any end-of-debate effects from it are removed, may not make a debate action next turn.



> #### Counterpoint
>
>**Preparation:** 1 turn, must be in an debate, fatigue debate by 1
>
>**Check:** Contest Deabte vs Target's Debate (apply social modifiers)
>
>**Duration:** Until debate ends
>
>**Target:** Character(s) in debate
>
>**Effect:** Remove or undo effect of opponent's most recent debate use.

### Debate Skill 15 - Civil Debate

You're able to make it so those who debate with emotion instead of logic fall flat on their face.



**Category:** Ability Skill

**Point Cost:** 15

**Prerequisite:**  15 levels in Debate, one Debate Skill 14 aspect

**Action:** 

> ### Civil Debate
>
>**Preparation:** 1 turn, must be in an debate, must have less than 5 social modifiers gained during the course of the debate
>
>**Check:** Auto-pass
>
>**Duration:** Until debate ends
>
>**Target:** Debate you are part of
>
> **Effect:** Anyone in the debate who attempts Appeal Reputation, Anger Opponent, or Humiliate Opponent has their roll halved (rounded down). Failure in their attempts also gives them an additional -5 defensive social modifier for the argument.

### Debate Skill 15 - Logical Knot

You're so skilled at making an argument that seems like it would make sense and seems simple, yet is so convoluted and confusing that you can easily make people's heads hurt.



**Category:** Ability Skill

**Point Cost:** 15

**Prerequisite:**  15 levels in Debate, one Debate Skill 14 aspect

**Action:** 

> #### Logical Knot
>
>**Preparation:** 1 turn, must be in an debate, fatigue debate by 1
>
>**Check:** Contest Target's Sanity
>
>**Duration:** Instant
>
>**Target:** Character in debate
>
> **Effect:** Target takes 5% mind damage (this does nothing if target is unconscious.)

### Deduction Skill 10 - Deductive Analysis

Deduction is the ability to notice patterns of things that have happened and discover what has happened as a result when time is taken to go over information.

**Category:** Ability Skill

**Point Cost:** 10

**Prerequisite:**  10 levels in Deduction, and one Deduction skill 9 aspect.

**Action:**

> #### Deductive Analysis
>
>**Preparation:** 5 minutes of examining target to be investigated, fatigue deduction by 1
>
>**Check:** Set by GM
>
>**Duration:** Instant
>
>**Target:** Storyline event or object with unknown information
>
> **Effect:** Character learns some of the unknown information that can be deduced from things present.
>
>**Failure:** Character learns incorrect information

*Note: The GM is not required to tell what the check difficulty is. Also, the information doesn't have to be obviously useful, but should at least allow more insight into the place, thing, etc. being researched.*

### Deflection Skill 1 - Deflect

Deflect is the ability to change the direction of something coming at you (provided that you have something capable of redirecting it).

**Category:** Ability Skill

**Point Cost:** 1

**Prerequisite:**  1 level in Deflection

**Action:**

> #### Deflect
>
>**Preparation:** Fatigue Deflection by 1
>
>**Check:** Check result of attacker (if attacked) or a number set by the GM (for events such as a falling rock).
>
>**Duration:** Instant
>
>**Target:** Effect that targets you, if you have a means of affecting whatever would cause that damage (such as a shield or piece of metal to deflect an arrow or bullet, a wand to deflect a magic attack, etc.)
>
> **Effect:** Prevent the damage, deflecting object takes 1/10th of the damage you would have taken.

### Deflection Skill 15 - Reflect

Reflect is the ability to not only avoid attacks, but to ricochet them back at the attacker.

**Category:** Ability Skill

**Point Cost:** 15

**Prerequisite:**  15 levels in Deflection, one Deflection Skill 14 aspect.

**Action:**

> #### Reflect
>
>**Preparation:** Fatigue Deflection by 1
>
>**Check:** Triple attacker's attack check
>
>**Duration:** Instant
>
>**Target:** Effect that targets you, if you have a means of affecting whatever would cause that damage (such as a shield or piece of metal to deflect an arrow, a wand to deflect a magic attack, etc.)
>
> **Effect:** The one who did the attack suffers its effect.

*Roleplaying Tips: Those who have reached this level of reaction do so on instinct rather than conscious thought, as conscious thought is usually too slow, so these people tend to be an odd combination of 'jumpy & clumsy' but 'graceful and controlled' at the same time. Such as they may see light glint out of the corner of their eye, and in instinctive reaction accidentally knock over a vase, which they then catch  before it hits the floor and set back in place in one fluid motion. Other examples include beginning to fall down stairs but gracefully landing their fall, and occasionally "dodging" when a friend waves hi, etc.*

### Design Skill 5 - Basic Design

**Category:** Ability Skill

**Point Cost:** 5

**Prerequisite:**  5 levels in Design, one Design Skill 4 aspect.

**Effect:** If the character has the prerequisites to craft a possession they could otherwise purchase, they can create plans for it. Creating plans take a minimum of one day of work, with an additional day for every fives points of creation difficulty the possession has. They must pass a Design check with a difficulty equal to the possession's creation difficulty.

### Design Skill 6 - Constructive Memory

**Category:** Ability Skill

**Point Cost:** 6

**Prerequisite:**  6 levels in Design, one Design Skill 5 aspect. Design Skill 5 - Basic Design aspect, 5 levels in memory

**Effect:** If you ever have the chance to see, in detail, the plans for a possession, you will always count as having the plans for it on hand. You also count as having the possession on hand for the purposes of the Advanced Design aspects for any possession you have the plans for.

### Design Skill 9 - Advanced Design, Effective

**Category:** Ability Skill

**Point Cost:** 9

**Prerequisite:**  9 levels in Design, one Design Skill 8 aspect. Design Skill 5 - Basic Design aspect

**Effect:** If a character has the prerequisites to craft a possession that they have on hand, they can create plans for a modified version of the possession. 

The possession's creation difficulty is increased by 10. When created, the possession will give a +1 to all rolls made using it, and it will be considered a "Premium" possession. The base purchase cost of the possession is increased by 20%(increasing production costs by 20% as well).

Creating plans takes a minimum of one day of work, with an additional day for every 10 points of creation difficulty the possession has. They must pass a Design check with a difficulty equal to the possession's creation difficulty.

### Design Skill 9 - Advanced Design, Cheap

**Category:** Ability Skill

**Point Cost:** 9

**Prerequisite:**  9 levels in Design, one Design Skill 8 aspect. Design Skill 5 - Basic Design aspect

**Effect:** If a character has the prerequisites to craft a possession that they have on hand, they can create plans for a modified version of the possession. 

The possession's creation difficulty is reduced by 10. When created, the possession will give a -1 to all rolls made using it, and it will be considered a "Shoddy" possession. The base purchase cost of the possession is reduced by 10%(reducing production costs by 10% as well).

Creating plans takes a minimum of one day of work, with an additional day for every 10 points of creation difficulty the possession has. They must pass a Design check with a difficulty equal to the possession's creation difficulty.

### Drive Skill 2 - Long Distance Drive

**Category:** Ability Skill

**Point Cost:** 2

**Prerequisite:**  2 levels in Drive, one Drive Skill 1 aspect.

**Effect:** As long as mind has no fatigue, if you're driving a vehicle long distances, you may go farther in a day than normal. After you've traveled a day's distance, you may increase the maximum distance by a percentage equal to your Drive ability for that vehicle. However, doing so causes your mind to become completely fatigued.

### Drive Skill 6 - Vehicle Dodge

While driving is generally thought of as an easy task there are times when quick reflexes and a sharp eye are needed.

**Category:** Ability Skill

**Point Cost:** 6

**Prerequisite:**  6 levels in Drive, one Drive Skill 5 aspect.

**Action:**

> #### Vehicle Dodge
>
>**Preparation:** Fatigue Drive by 1
>
>**Check:** Check of an attack targeting a vehicle you're driving, or a check set by GM if it's not an attack (such as dodging falling rocks).
>
>**Duration:** Instant
>
>**Target:** Damage that would happen to the vehicle.
>
> **Effect:** The vehicle does not take that damage.

![Durability](./img/Durability.jpg)

### Endurance Skill 5 - Shrug Off

**Category:** Ability Skill

**Point Cost:** 5

**Prerequisite:**  5 levels in Endurance, and one Endurance skill 4 aspect.

**Action:**

> #### Shrug off
>
>**Preparation:** Fatigue Endurance by 1
>
>**Check:** Damage Dealt to Body 
>
>**Duration:** Instant
>
>**Target:** Self: Body Damage Taken this turn
> 
> **Effect:** Damage taken is reduced by amount by which roll passed the check (As such, no damage reduced if check and roll are same, even though it's considered 'passed').  Fatigue endurance for every 5% damage prevented this way, rounded down.

### Exercise Skill 1 - Physical Experience

One key to exercise is making the opportunity to grow in daily actions.

**Category:** Ability Skill

**Point Cost:** 1

**Prerequisite:**  1 level in Exercise

**Action:**

> #### Physical Experience
>
>**Preparation:** Have fatigued all of body or one of its lower abilities, fatigue exercise by 1.
>
>**Check:** The number of levels you have in the fatigued ability
>
>**Duration:** Permanent
>
>**Target:** Yourself
>
>**Effect:** Gain 1 character point that must be spent on the fatigued ability, one of its lower abilities, or on one of its ability skill aspects (they may be saved up.)

### Exercise Skill 4 - Training

A good coach does well with a good trainee.

**Category:** Ability Skill

**Point Cost:** 4

**Prerequisite:**  4 levels in Exercise, one Exercise Skill 3 aspect

**Action:**

> #### Training
>
>**Preparation:** Be targeted by a use of teach, fatigue Exercise by 1.
>
>**Check:** Check the Teacher is trying to pass.
>
>**Duration:** Instant
>
>**Target:** A character trying to use the Teach ability on you to increase one of body's lower abilities.
>
> **Effect:** +20 to teacher's roll in regards to you or double character points you would gain from the teacher's action.

### Exercise Skill 8 - Workout

Sometimes experience or training just isn't good enough. Sometimes, it takes the dedication of going out and pushing yourself.

**Category:** Ability Skill

**Point Cost:** 8

**Prerequisite:**  8 levels in Exercise, one Exercise Skill 7 aspect

**Effect:** If you spend 8 consecutive hours just working out, you may gain 1 character point towards: any lower ability of body of your choice, an ability skill aspect of any of those abilities, or a physical category aspect.

### Experimental Subject

**Category:** Creative, Background & Destiny

**Point Cost:** 15

**Prerequisite:** GM Permission

**Effect:** You gain 2 destiny points each gaming session. 

You may take one ability you wouldn't normally meet the prerequisites for. You gained it as a result of being experimented on by people or beings beyond your comprehension (alien abductors, mad wizards, military experimental programs, etc.). The costs of this ability are double what they they would normally be. (Example: You don't need to meet all the requirements for exercise but it will cost 6 character points per level.)

*Note: GM may abuse this background. Use at your own risk!*

### Family Member, Dependent

**Category:** Family

**Point Cost:** -40

**Prerequisite:** GM Permission

**Effect:** You have a close family member that's important to you that is noticeably weaker, that you have to watch out for. They have a number of character points less than half of your own (GM may adjust this up or down by up to 15%). At GM's discretion (or story-line inspired reasons) the character may come to try and help you out (or get in the way). However, the character may also require your help on occasion too. Should your family member ever die, your character will take a 15% Mind damage that will not heal for one year in game. However, you get a +20 defensive social modifier with the goal of defending your family, and any use that involves protecting the family member gets a+2 to the roll. If the family member should ever die and you could have prevented it, the 15% mind damage is permanent and will not go away after a year, plus you will have a permanent -2 to sanity.

Any attempt to avenge a family member's death will remove the damage for the attempt, and give a bonus as if you were protecting the family member.

Further, your family member can arrive on the scene when you need them most (or manage to provide some kind of 'support from a distance' if they are unavailable) at the cost of 3 destiny points. Be warned, this can turn into a situation where you need to rescue them, although the GM should have them at least provide some kind of vital assistance in the process.

### Family Member, Guardian

**Category:** Family

**Point Cost:** 45

**Prerequisite:** GM Permission

**Effect:** You have a close family member that's important to you that is noticeably more capable than you, who watches out for you. They have a number of character points more than twice your own (GM may adjust this up or down by up to 15%). At GM's discretion (or story-line inspired reasons) the character may come to try and help you out (or get in the way). However, the character may also require your help on occasion too. Should your family member ever die, your character will take a 15% Mind damage that will not heal for one year. However, you get a +20 defensive social modifier with the goal of defending your family, and any use that involves protecting the family member gets a+2 to the roll. If the family member should ever die and you could have directly prevented it, you permanently lose two points of sanity.

Any attempt to avenge a family member's death will remove the loss for the attempt, and give a bonus as if you were protecting the family member.

Further, your family member can arrive on the scene when you need them most (or manage to provide some kind of 'support from a distance' if they are unavailable) at the cost of 10 destiny points.

### Family Member, Similar

**Category:** Background & Destiny, Family

**Point Cost:** 20

**Prerequisite:** GM Permission

**Effect:** You have a close family member that's important to you that is roughly your equal. They have a number of character points equal to your own (GM may adjust this up or down by up to 15%). At GM's discretion (or story-line inspired reasons) the character may come to help you out. 

However, the character may also require your help on occasion too. Should your family member ever die, your character will take a 15% Mind damage that will not heal for one year. However, you get a +20 defensive social modifier with the goal of defending your family, and any use that involves protecting the family member gets a+2 to the roll. Should the family member ever die and you could have directly prevented it, you take a permanent -2 to your sanity.

Any attempt to avenge a family member's death will remove the will loss for the attempt, and give a bonus as if you were protecting the family member.

Further, your family member can arrive on the scene when you need them most (or manage to provide some kind of 'support from a distance' if they are unavailable) at the cost of 5 destiny points.

### Finesse Skill 0 – Finesse Centering

**Category:** Instant

**Point Cost:** 0

**Action:**
>
>**Preparation:** Fatigue Finesse 1 (cannot be altered by other aspects) 
>
>**Duration:** Instant
>
>**Target:** Self
>
> **Effect:** Reroll 1 roll of finesse or one of its lower abilities, or reduce a vital strike chance by your level in finesse.

### First Experiences

**Category:** Creative, Free, Instant

**Point Cost:** 0

**Prerequisite:**  1 level in Mind

**Action:**

> #### First Experiences
>
>**Preparation:** Fatigue Mind by 1
>
>**Check:** Special (see Effect)
>
>**Duration:** Permanent
>
>**Target:** Self

**Effect:** You can keep a log of first experiences your character has had. Each entry should be a short title followed by a description. Each title may only be used once. Roll a 1d4, and subtract 1 for each word in the title. If you roll greater than a 1, you receive an additional character point to spend however you like.

*Rules note: You may have “First” in front of a title and it doesn't count against the number of words. Further, first experiences can be sub-categories of other experiences. For example: “First Kiss” (a -1 roll) would be a different entry than “First French Kiss” (a -2 roll). However, if your character's first kiss actually was a french kiss, the GM may rule your second title invalid.*



### Flexibility Skill 8 - Tumble

**Category:** Ability Skill

**Point Cost:** 8

**Prerequisite:**  8 levels in Flexibility, and one Flexibility skill 7 aspect.

**Action:**

> #### Tumble
>
>**Preparation:** Fatigue Flexibility by 1
>
>**Check:** Check vs Target's Accuracy
>
>**Duration:** Until end of target's next action or your next action, whichever comes first.
>
>**Target:** Any one character
>
> **Effect:** Target can't target you.

### Flexibility Skill 8 - Exotic Movement

You are talented at moving in such a way as to draw attention. It looks powerful and in control while maintaining a strong aesthetic and alluring value.

**Category:** Ability Skill

**Point Cost:** 8

**Prerequisite:**  8 levels in Flexibility, and one Flexibility skill 7 aspect and 2 levels in artistry.

> #### Exotic Movement
>
>**Preparation:** Instant Prep
>
>**Check:** 18
>
>**Duration:** Maintained
>
>**Target:** Observing characters.
>
> **Effect:** -10 to all non-social roles, and a +1 to social rolls for every 5 past the check. If you fail any non-social roles, get a -1 to all social rolls for the next hour instead, and during that time exotic movement may not be attempted.
>
>**Failure:** -1 to all social rolls for every 5 by which the check failed, and a 10% chance to take 1% body damage.

*Roleplaying Tips: People with exotic movement have the ability to make their bodies move in unique ways, usually through lots of practice. Usually, this practice is only carried out by one who actually enjoys the feel of the movements of their body. This leads to a tendency to be more sultry and sensual than the average person.*

### Flexibility Skill 15 - Contortionist

You are skilled at fitting yourself into places and into shapes that are awkward and unique. 

**Category:** Ability Skill

**Point Cost:** 15

**Prerequisite:**  15 levels in Flexibility, and one Flexibility skill 14 aspect.

> #### Contortionist
>
>**Preparation:** 1 minute
>
>**Check:** 10 for how many times bigger you are than the target space. (Example: if you are twice as big as the area you are trying to fit into the check is 20.)
>
>**Duration:** Maintained
>
>**Target:** A space that is desired to be fit into that is no smaller than 1/5th your size.
>
>**Effect:** You fit into a space that you otherwise wouldn't be able to. Movement speed is reduced to 1/10th normal.

*Roleplaying Tips: Contortionists often aren't made, but born. To be a good contortionist requires a lot of double-joints, and although some aren't in good enough shape to realize they're double-jointed, most who actually become contortionists know they're something unique. As such, a number of contortionists have no problems standing out, or acting strange. They know they're not like everyone else, so why would they want to act like it?*

### Forage Skill 1 - Food Hunt

You have become skilled at scouring the land for sustenance.

**Category:** Ability Skill

**Point Cost:** 1

**Prerequisite:** 1 level in forage

**Action: **
>
>**Preparation:** 30 minutes
>
>**Check:** 10 for an environment friendly to your survival requirements (such as a summer forest when looking for food), GM may increase check for harsher climes.
>
>**Duration:** Permanent
>
>**Target:** General area you are in.
>
>**Effect:** Find a source for one of your survival requirements (water, food, etc.)
>
>**Failure:** Roll 1d100. 
>
>On a 100, you get lucky and find your survival requirement anyway. 
>
>On a 2-10, you mistake unhealthy choices for healthy ones. Body and Mind each take 5% damage. 
>
>On a 1, you make a very unhealthy choice. GM may do as desired. (Examples include poison, 25% damage to Body or Mind, falling into a cave and being lost, etc.)

### Forage Skill 12 - Special Requirements Foraging

You are knowledgeable about both the wilderness and about the needs of others, allowing you to help others easily.



**Category:** Ability Skill

**Point Cost:** 12

**Prerequisite:** 12 levels in forage, one forage skill 11 aspect, 5 levels in treatment

**Action: **
>
>**Preparation:** 30 minutes
>
>**Check:** Minimum 20, GM may increase check for harsher or more difficult climes.
>
>**Duration:** Permanent
>
>**Target:** General area you are in, and another character who has survival requirement you don't have.
>
> **Effect:** Find a source for one of your target character's survival requirements (source of insulin, unique food required for an exotic pet, etc.)
>
>**Failure:** Roll 1d100. 
>
> On a 100, you get lucky and find the survival requirement anyway. 
>
> On a 2-10, you mistake unhealthy choices for healthy ones. Body and Mind each take 5% damage if target character accepts your assistance. 
>
> On a 1, you make a very unhealthy choice. GM may do as desired. (Examples include poison, 25% damage to Body or Mind, falling into a cave and being lost, etc.)

### Grapple Skill 2 - Lock Hold

**Category:** Ability Skill

**Point Cost:** 2

**Prerequisite:** 2 levels in Grapple, one Grapple Skill 1 aspect

**Action:**

> #### Lock Hold
>
>**Preparation:** 1 action, Fatigue Grapple by 1
>
>**Check:** Contest target's Strength + Dexterity
>
>**Duration:** Maintained
>
>**Target:** A Character you have in a hold
>
> **Effect:** Character is unable to do any body action or move.

### Grapple Skill 6 - Flying Grapple

**Category:** Ability Skill

**Point Cost:** 6

**Prerequisite:** 6 levels in Grapple, one Grapple Skill 5 aspect, Grapple Skill 2 - Lock Hold

**Action:** 

> #### Flying Grapple
>
>**Preparation:** Fatigue Grapple by 1
>
>**Check:** Contest target's Dexterity
>
>**Duration:** Instant
>
>**Target:** A character in a space you are about to land in
>
> **Effect:** If you would take damage from falling, instead that damage is done to your target. Upon dealing damage to a target, you may begin a hold on the target (without paying the preparation for it).

*Note: You cannot deal more damage to the target than they can take. Any remaining falling damage you can't deal to the target is still dealt to you.*

### Guilt Complex

**Category:** Personality

**Point Cost:** -5 per level

**Prerequisites:** Game has a GM, and GM permission

**Effect:** When the excitement is down and you have time to reflect, there's a 10% chance every day of downtime that you'll break down emotionally, and be unable to act efficiently. When this happens, it will last for 1d4 hours for each level of Guilt Complex, giving you a -4 to every roll during the duration. 

While in this state, another character may communicate with you, contest their conversation versus your sanity to change the number of hours you spend 'broken down' by 2 (either increasing it or decreasing it). 

If you ever spend more than 48 hours straight suffering from guilt in this manner, make a sanity roll against a difficulty of 5. If you fail, add a level of the suicidal aspect to your character, lowing your character point total appropriately.

Roleplaying Tips: Characters with a guilt complex are usually doubting their actions, never quite sure if they're doing the right thing, and wondering if they're just making the world worse instead of better. Generally, they're a major downer at any social event.

### Handed [*]

* Choose a specific hand or fine manipulator

**Category:** Physical

**Point Cost:** 4

**Effect:** One of your hands (or equivalent for non-humanoid races) is dominant over the others. You get a +1 on any single-handed activity that uses this hand, but a -4 on any single-handed activity that doesn't use that hand. This flaw may be taken up to 3 times, but only to be applied to the same hand. The effects stack.



### Handicapped [Type of Handicap] 

**Category:** Physical

**Point Cost:** - Variable

**Effect:** This applies to many possible drawbacks. You lose a benefit that your species has, and adjust your point cost appropriately. For example: If you're species normally has 2 arms, you may handicap to one arm and the point gain is the cost of that arm (as determined by guided mutations in the species guide). Or you may make yourself colorblind if you're a member of a normally color-seeing species, again, taking the difference in point gain. If the handicap affects your appearance (which would apply to losing an arm, but probably not to colorblindness) then you should also take the Damaged Appearance drawback to go with it. Handicapped may only remove positive point-cost species traits, not negative ones.

*Examples for Humans and human-like species in regards to area affected by handicap:*


| Handicap | Point Cost |
| --- | --- |
|Bald | -3|
|Blindness | -57 |
| Cripple (Remove Dash species ability) | -60 |
| Deaf | -20 | 
| Missing Hand | -50 |
| Mute | -10 |

### Handling Skill 1 - Taming

**Category:** Ability Skill

**Point Cost:** 1

**Prerequisite:** 1 levels in Handling

**Effect:** If you spend 1 month in game with regular contact with a creature, you may contest your handling versus that creature's mind + body. If you succeed, the creature counts as tamed, and will not attack you or those who closely associate with you.

### Handling Skill 5 - Commanding

**Category:** Ability Skill

**Point Cost:** 5

**Prerequisite:** 5 levels in Handling, one Handling Skill 4 aspect

**Effect:** If you spend 1 month in game with regular contact with a tamed creature, you may teach it to attempt a specific action upon being given a specific signal. If the creature would have any serious reason to not follow the command, the creature may make a sanity contest vs the might of the character giving the command in order to refuse the command.

### Heroic Acceleration

**Category:** Physical

**Point Cost:** 20

**Prerequisite:** 5 levels in Speed

**Effect:** A character with Heroic Speed  can respond more quickly to mentally demanding situations. Any Lower ability of Body that has the [Ability] Skill 8 – Reaction aspect may fatigue Speed as a source for its fatigue cost.

*Roleplaying Tips: A character with Heroic Acceleration is used to rushing from place to place. When forced to take things slow, others may often read them as distracted or detached from the situation, which isn't that far from the truth, as those with heroic acceleration are used to a faster-paced way of living than most.*

### Heroic Clarity

**Category:** Mental

**Point Cost:** 20

**Prerequisite:** 5 levels in Sanity

**Effect:** A character with Heroic Clarity can shrug off mental damage easily. Every time that a character with Heroic Clarity would take Mind damage, they may fatigue Sanity by 1 to ignore a percent of that damage equal to a Sanity check (this applies to all damage taken during that turn, not just from a single source). This can only be done once per turn.

*Roleplaying Tips: A character with Heroic Clarity has a tendency to never take things to heart. They are witty, confusing, and prone to strange twists of logic. Their unique and uncommon insights make them hard to mentally pin down. They are known for coming to their correct conclusions due to very obscure connections (Example statement by a character with Heroic Clarity: “Of course the Count is a werewolf, it's so obvious. Farmer Henry's chickens have been laying more eggs.”) This doesn't necessarily mean they're right more than others, just that they have obscure reasons when they are right.*



![Spiritual Energy](./img/SpiritualEnergy.jpg)

### Heroic Coordination

**Category:** Physical

**Point Cost:** 20

**Prerequisite:** 5 levels in Finesse

**Effect:** A character with Heroic Coordination can reliably control their body. The character may fatigue Finesse instead of one of finesse's lower abilities for that ability's next roll.

*Roleplaying Tips: A character with heroic coordination usually sees uses in terrain where others see obstacles. For example, a thief with heroic coordination may intentionally run into a narrow dead end ally, knowing he can jump back and forth between the two close walls to gain height to go over the end of the ally while his pursuers will be left behind. As such, they begin to get into the habit of holding an air of confidence in seemingly impossible situations.*

### Heroic Fortitude

**Category:** Physical 

**Point Cost:** 20

**Prerequisite:** 5 levels in toughness 

**Effect:** A character with Heroic Fortitude can shrug off damage easily. Every time that a character with Heroic Fortitude would take body damage, they may fatigue Toughness by 1 to ignore a percent of that damage equal to a toughness check (this applies to all damage taken during that turn, not just from a single source). This can only be done once per turn.

*Roleplaying Tips: Characters with Heroic Fortitude have a tendency to get hit... a lot. They've learned to take pain, and fuel it into their determination. Further, they've instinctively learned how to take a hit. As a result, hits that could be lethal usually are reduced to superficial. A character with Heroic Fortitude will often end up bleeding profusely bleeding all over from various superficial wounds, leading to others wondering how they're even standing. When in a dangerous situation, they're rarely talkative, focusing almost completely on the force of danger.*

### Heroic Ingenuity

**Category:** Mental 

**Point Cost:** 20

**Prerequisite:** 5 levels in Creativity

**Effect:** A character with Heroic Ingenuity  can reliably make accurate guesses about a situation. The character may fatigue Creativity instead of fatiguing one of Creativity's lower abilities for that ability's next roll.

*Roleplaying Tips: A character heroic Ingenuity has a tendency to 'feel' the world around them. Their ability partially stems from picking up many subconscious hints from the world around them. They ask questions chosen specifically to find unexpected reactions, touch random things as they walk, and seem to have an odd awareness to them, as if they're distracted by music only they can hear.*

### Heroic Intelligence

**Category:** Mental

**Point Cost:** 20

**Prerequisite:** 5 levels in IQ

**Effect:** A character with Heroic Intelligence can respond more quickly to mentally demanding situations. Any Lower ability of Mind that has the [Ability] Skill 8 – Reaction aspect may fatigue IQ as a source for its fatigue cost.

*Roleplaying Tips: A character with Heroic intelligence is usually a very observant character, one quick to point out solutions and rarely one to mince words. They are quick and decisive, and can often leave other people baffled when they head off to solve a problem before everyone else even feels done talking about it.*

### Heroic Recollection

**Category:** Mental

**Point Cost:** 20

**Prerequisite:** 5 levels in Memory

**Effect:** A character with Heroic Recollection is a wellspring of raw information. A character with Heroic Recollection can fatigue Memory by 1 to add 2 to a roll for Memory or one of its lower abilities.

*Roleplaying Tips: Characters with heroic recollection take in the events of the world around them like a sponge and release it just as easily. Often, information is stored sequentially, so a simple question can unleash a torrent of time-related information, both about what came before and what came after the event in question as they mentally relive the moment that gives them the answer.* 

### Heroic Strength

**Category:** Physical

**Point Cost:** 20

**Prerequisite:** 5 levels in Might

**Effect:** A character with Heroic Strength is a physical force to be reckoned with. A character with Heroic Strength can fatigue strength by 1 to add 2 to a roll for strength or one of its lower abilities.

*Roleplaying Tips: Characters with heroic strength are often used to plowing through situations with little difficulty. They often get used to the world simply bending to them when they come through. As such, many do not feel the need to talk in detail, letting simple body language do a lot of the talking, feeling only a few of their words needed to get their point across.*



![Body](./img/Body-Final.jpg)

### Hold Skill 3 - Enduring Hold

**Category:** Ability Skill

**Point Cost:** 3

**Prerequisite:**  3 levels in Hold, and one Hold skill 2 aspect.

**Action:** 
>
>**Preparation:** 1 action, must come in physical contact with target, fatigue Hold by 1
>
>**Check:** 1/5 target's weight in kilograms (round down) if not resisting, Contest Target's Hold if target is resisting.
>
>**Duration:** 1 turn
>
>**Target:** Anything with a Body Ability
>
>**Effect:** Keep target from moving until next action you may act. The next action you act, you you may skip the action to continue the hold. You must make the check again every turn.

### Hold Skill 7 - Constriction Hold

You are skilled at holds that prevent breathing or other survival requirements. 

**Category:** Ability Skill

**Point Cost:** 7

**Prerequisite:**  7 levels in Hold, and one Hold skill 6 aspect, Hold skill 3 - Enduring Hold

**Action:** 
>
>**Preparation:** 1 action, must have target in a hold, fatigue Hold by 1
>
>**Check:** 1/5 target's weight in kilograms (round down) if not resisting, Contest Target's Strength if target is resisting.
>
>**Duration:** 1 turn
>
>**Target:** Anything with a Body Ability
>
> **Effect:** Counts as maintaining an enduring hold. For one turn, you may prevent target access to one non-constant survival requirement (such as breathing).

*Roleplaying Tips: The training for chokes usually involves lots of being choked as well as choking. This usually results in either some kind of minor throat damage or strong throat muscles, resulting in an odd voice or loud voice respectively.* 

### Hold Skill 9 - Groundwork

**Category:** Ability Skill

**Point Cost:** 9

**Prerequisite:**  9 levels in Hold, one Hold skill 8 aspect, and Hold Skill 3 - Enduring Hold

**Effect:** Once per turn, you may make a movement action at ¼ your base movement speed while holding (this does not stop you from continuing your hold.)

*Roleplaying Tips: The training for groundwork usually involves lots of very close training with other people. Thus, many who learn groundwork quickly have to abandon their sense of personal space. As such, they learn to get close to people, and often have no compulsions about hanging themselves around the shoulders of a friend, hugging allies after a job well done, etc. They often come across as friendly, but some people with more distant attitudes can be put off by this.*

### Hold Skill 9 - Blood Hold

You can block off blood with a good hold

**Category:** Ability Skill

**Point Cost:** 9

**Prerequisite:**  9 levels in Hold, and one Hold skill 8 aspect, Hold skill 7 - Constriction Hold

**Action:** 
>
>**Preparation:** 1 action, must have target in a hold, fatigue Hold by 1
>
>**Check:** 1/5 target's weight in kilograms (round down) if not resisting, Contest Target's Toughness if target is resisting.
>
>**Duration:** 1 turn
>
>**Target:** Any creature with a Body Ability and internal organs that you have maintained a hold on for two turns
>
> **Effect:** Target goes unconscious for as long as you maintain the hold.

### Identification Skill 2 - Appraise

Even though you may not be able to tell the specifics, you're good at figuring out the basic details of a possession.

**Category:** Ability Skill

**Point Cost:** 2

**Prerequisite:**  2 levels in Identification, and one Identification skill 1 aspect

**Action:** 

### Appraise
>
>**Preparation:** 2 minutes
>
>**Check:** Work cost of the possession
>
>**Duration:** Instant
>
>**Target:** A possession
>
>**Effect:** Know the approximate value of the possession, and it's basic functions if it's tech level is no more than one tech level above yours.

### Infiltration Skill 2 – Pick Lock

Sometimes, the best way to understand something is to break it.

**Category:** Ability Skill

**Point Cost:** 2

**Prerequisite:**  2 levels in Infiltration, one Infiltration Skill 1 aspect

**Action:**

> #### Pick Lock
>
>**Preparation:** 10 actions, fatigue Infiltration by 1
>
>**Check:** Contest against lock check difficulty (set by lock crafter’s craft check when created equal to their result.)
>
>**Duration:** Instant
>
>**Target:** A lock within your reach
>
> **Effect:** Lock opens



### Infiltration Skill 10 - Undercover

**Category:** Ability Skill

**Point Cost:** 10

**Prerequisite:**  10 levels in Infiltration, one Infiltration skill 9 aspect

**Action: **
>
>**Preparation:** Attempt to infiltrate the facility/organization you wish to enter, passing 3 consecutive infiltration contests vs the wisdom of 3 individuals of that organization (each failure increases later difficulties with same organization by 5.)
>
>**Check:** Contest Observe of an individual of the organization/facility who acts as a gatekeeper or similar restrictive position. (GM may significantly increase difficulty for if you disguise as a high rank individual, or someone the gatekeeper should recognize.)
>
>**Duration:** 1 day
>
>**Target:** Place/Organization to be infiltrated
>
>**Effect:** For the day of infiltration, when sneaking in the place of infiltration, get a bonus to all sneak rolls equal to your level in Infiltration.
>
>**Failure:** You arouse suspicion, future checks to infiltrate same target have the difficulty of the check increased by 10.

### Infiltration Skill 15 - Official Forgery

You're skilled at getting officials to unwittingly make 'replacement copies' of documentation you need to be part of an organization.

**Category:** Ability Skill

**Point Cost:** 15

**Prerequisite:**  15 levels in Infiltration, one Infiltration skill 14 aspect, Infiltration Skill 10 - Undercover

**Effect:** If you manage 8 consecutive days undercover in a facility/organization, you don't need to make additional Infiltration checks to remain undercover, until such a time as your cover is blown.

### Infiltration Skill 16 - Inside Contact

You're skilled at getting into positions that help others of your choice infiltrate places you've already infiltrated.

**Category:** Ability Skill

**Point Cost:** 16

**Prerequisite:**  16 levels in Infiltration, one Infiltration skill 15 aspect, Infiltration Skill 10 - Undercover

**Effect:** If you manage two consecutive months undercover in game in a facility/organization, you can give any other individual of your choice a +20 to infiltration attempts.

### Infiltration Skill 19 - False Trail

You're skilled at leaving a trail of your activities... that point in the wrong direction.

**Category:** Ability Skill

**Point Cost:** 19

**Prerequisite:**  19 levels in Infiltration, one Infiltration skill 18 aspect, Infiltration Skill 15 - Official Forgery

**Effect:** If you manage one month of consecutive days in the game undercover in a facility/organization, you may cause that any one activity that might blow your cover to instead rouse suspicion on another individual. (In organizations/facilities with ranked positions, suspicion must be redirected to someone of near or lesser rank.)

### Inspire Skill 3 - Inspirational Speech

You're good at filling people with hope and the fire to follow their dreams.

**Category:** Ability Skill

**Point Cost:** 3

**Prerequisite:**  3 levels in Inspire, and one Inspire skill 2 aspect

**Action:** 

> #### Inspirational Speech
>
>**Preparation:** 1 minute communicating to targets, Fatigue Inspire by 1
>
>**Check:** Contest Target's Morality (roll only once, targets may save as a group or as individuals, according to GM) (apply applicable social modifiers)
>
>**Duration:** 1 day
>
>**Target:** A group of characters
>
> **Effect:** Targets get a +1 bonus to do the task you inspired them to do. Any of the target's morality bonuses that would go against the task are halved.

 

![Deduction](./img/Deduction.jpg)

### Investigate Skill 1 - Remember

**Category:** Ability Skill

**Point Cost:** 1

**Prerequisite:**  1 level in Investigate, game with a GM

**Action:**

> #### Remember
>
>**Preparation:** 1 action, fatigue investigation by 1
>
>**Check:** 5
>
>**Duration:** Instant
>
>**Target:** GM
>
> **Effect:** Be reminded of general concept introduced to the character previously.

### Investigate Skill 2 - Search

Discovering the hidden things in an area can often be a useful skill, and not just for finding lost keys.

**Category:** Ability Skill

**Point Cost:** 2

**Prerequisite:**  2 levels in Investigate, and one Investigate skill 1 aspect

**Action:**

> #### Search
>
>**Preparation:** 1 minute per square meter (with a +1 bonus for each additional minute spent per square meter.)
>
>**Check:** 12
>
>**Duration:** Instant
>
>**Target:**  Area of x square meters.
>
> **Effect:**  For an observe check of the target area, treat your senses as if they were of one level of detail higher than they normally are, and you have the chance to observe anything in the affected area.

### Investigate Skill 12 - Good Memory

**Category:** Ability Skill

**Point Cost:** 12

**Prerequisite:**  12 levels in Investigate, and one Investigate skill 11 aspect, game with a GM, Investigate Skill 1 - Remember

**Action:**

> #### Good Memory
>
>**Preparation:** 1 action, fatigue investigation by 1
>
>**Check:** 20
>
>**Duration:** Instant
>
>**Target:** GM
>
> **Effect:** You may ask general questions about things remembered, and GM should answer (it can be vauge) if your character has a reasonable chance of knowing the answer.

*Roleplaying Tips: People with a good memory often are not as socially adept as others. This is due to spending as much time thinking and remembering as experiencing. They also often have an annoying habit of correcting people at inconvenient times.* 

### Investigate Skill 17 - Eidetic Memory

**Category:** Ability Skill

**Point Cost:** 17

**Prerequisite:**  17 levels in Investigate, and one Investigate skill 16 aspect, game with a GM, Investigate Skill 12 - Good Memory

**Effect:** Anything that requires plans, written records, etc. that you have seen before but don't have, you may make an Investigation check with a difficulty of 40. If you succeed, you may make your next ability action as if you had the required documents.

**Action:**

> #### Eidetic Memory
>
>**Preparation:** 1 action, fatigue investigation by 1
>
>**Check:** 40
>
>**Duration:** Instant
>
>**Target:** GM
>
> **Effect:** You may ask highly specific  questions about things remembered, and GM should answer clearly if your character has been exposed to the information in any way.

*Roleplaying Tips: Good remembering has become more instinctual for the character with the Eidetic Memory. They begin to come outside of their internal world a bit more than they were before, and begin taking in even more information as a result. Although they're often even more quiet, they're also often more attentive.*

### Investigate Skill 25 - Enhanced Eidetic Memory

**Category:** Ability Skill

**Point Cost:** 25

**Prerequisite:**  25 levels in Investigate, and one Investigate skill 16 aspect, game with a GM, Investigate Skill 17 - Eidetic Memory

**Action:**

> #### Enhanced Eidetic Memory
>
>**Preparation:** fatigue investigation by 1
>
>**Check:** auto-pass
>
>**Duration:** Instant
>
>**Target:** GM
>
>**Effect:** You may ask highly specific  questions about things remembered, and GM should answer accurately if your character has been exposed to the information in any way.

*Roleplaying Tips: A character with Enhanced Eidetic Memory has a neigh-perfect memory. As such, they are confident of the world around them, as they remember everything, and are rarely surprised as a result. At this point, most only speak when spoken to, unless asked a question, where they will often answer in exhaustive detail.*

### IQ Skill 0 – IQ Centering

**Category:** Instant

**Point Cost:** 0

**Action:**
>
>**Preparation:** Fatigue IQ 1 (cannot be altered by other aspects) 
>
>**Duration:** Instant
>
>**Target:** Self
>
> **Effect:** For IQ or a lower ability, you may make your action this turn happen before an action just declared. (Note: They may do the same to this action.)
>
> OR
> 
> You may take an extra action this turn (Note: must be used for an action of IQ or a lower ability.)

### Jack of all trades

**Point Cost:** 40

**Prerequisite:** The “Quick on the uptake” and “Physical Adept” aspects

**Effect:** The character is a quick learner and skilled at many trades. As a result, they learn new trades very easily. With ten minutes of instruction (or an hour of surviving fiddling and experimenting on their own), a character may gain a level in any ability if they had no levels in it previously as long as they meet the prerequisites (meaning it will never go above a level of '1' by this method). Further, instead of spending character points, these levels are gained automatically, increasing the character's total number of character points as a result.

*Roleplaying Tips: A Jack of all trades is usually quite talkative and prone to starting conversations on random topics... often during other conversations.  An insulting word by an angry captor may remind him of a song which reminds him of the stanza which reminds him of a girlfriend.  A Jack of all Trades is usually really curious, and may start off random conversations with random questions, such as, “Do you ever wonder if you see colors inverted to what everyone else sees?” in the middle of intense time.*

### Learn Skill 1 - Mental Experience

One key to learning is to always watch for opportunities.

**Category:** Ability Skill

**Point Cost:** 1

**Prerequisite:**  1 level in Learn

**Action:**

> #### Mental Experience
>
>**Preparation:** Have fatigued all of Mind or one of its lower abilities, fatigue Learn by 1.
>
>**Check:** The number of levels you have in the fatigued ability
>
>**Duration:** Permanent
>
>**Target:** Yourself
>
> **Effect:** Gain 1 character point that must be spent on the fatigued ability, one of its lower abilities, or on one of its ability skill aspects (these points do not need to be spent immediately.)

### Learning Skill 4 - Studying

A good teacher does well with a good student.

**Category:** Ability Skill

**Point Cost:** 4

**Prerequisite:**  4 levels in Learn, one Learn Skill 3 aspect

**Action:**

> #### Studying
>
>**Preparation:** Be targeted by a use of teach, fatigue Learn by 1.
>
>**Check:** Check the Teacher is trying to pass.
>
>**Duration:** Instant
>
>**Target:** A character trying to use the Teach ability on you to increase one of mind's lower abilities.
>
> **Effect:** +20 to teacher's roll in regards to you or double character points you would gain from the teacher's action.

### Learn Skill 8 - Research

Sometimes, the best way to learn isn't from experience or teachers, but just bucking down and doing the research yourself.

**Category:** Ability Skill

**Point Cost:** 8

**Prerequisite:**  8 levels in Learn, one Learn Skill 7 aspect

**Effect:** If you spend 8 consecutive hours just in research, you may gain 1 character point towards: any lower ability of mind of your choice, an ability skill aspect of any of those abilities, or a mental category aspect. This does not have to be spent immediately.

### Loner

You work alone, and not only are you not very good with other people, you prefer not to have them around. Occasionally you may be forced to work with others, however, when you get the chance, you return to working on your own.

**Category:** Mental

**Point Cost:** -50

**Effect:** You cannot have any allies with a loyalty rating. You also may not become a follower of anything or anyone of your own free will. 

### Martial Arts Skill 1 - Unarmed Attack

When you're your weapon, it's hard to disarm you.

**Category:** Ability Skill

**Point Cost:** 1

**Prerequisite:**  1 level in Martial Arts

**Action:**

> #### Unarmed Attack
>
>**Preparation:** 1 action, fatigue Martial Arts by 1
>
>**Check:** Contest against target's Finesse
>
>**Duration:** Instant
>
>**Target:** A character within your reach
>
> **Effect:** Deal percent damage to the target's body equal to your levels in Unarmed Fighting or reduce target's body fatigue by an amount equal to your levels in unarmed fighting. 

(Damage is blunt damage)



![Hand-to-Hand](./img/Hand-to-Hand.jpg)

### Martial Arts Skill 7 - Unarmed Disarm

If your enemy brings a gun to a fistfight, the first step of winning is getting rid of the gun.

**Category:** Ability Skill

**Point Cost:** 7

**Prerequisite:**  7 levels in Martial Arts, one Martial Arts Skill 6 aspect

**Action:** 

> #### Unarmed Disarm
>
>**Preparation:** 1 action, Fatigue Unarmed Fighting by 1
>
>**Check:** Contest against target's Strength
>
>**Duration:** Instant
>
>**Target:** A character you have just succeeded in hitting with Unarmed Strike
>
> **Effect:** Remove a held possession from character

### Martial Arts Skill 10 - Strike Grapple

Strike with the precision of a cobra, and then hold on with the tenacity of a small angry dog.

**Category:** Ability Skill

**Point Cost:** 10

**Prerequisite:**  10 levels in Martial Arts, one Martial Arts Skill 9 aspect, Hold Skill 3 - Enduring Hold aspect

**Action:**

> #### Strike Grapple
>
>**Preparation:** 1 action, fatigue Martial Arts by 1
>
>**Check:** Contest against target's Finesse
>
>**Duration:** Instant
>
>**Target:** A character within your reach
>
>**Effect:** You may immediately attempt an Enduring Hold Action that does not need preparation and which receives an auto-success.

### Martial Arts Skill [X] - Precision Strike [Species]

You fully understand the anatomy of your target, and know all the physical weak points, allowing you to execute highly precise attacks..

**Category:** Ability Skill

**Point Cost:** [x] 

**Prerequisite:**  x levels in Martial Arts, one Martial Arts Skill [x-1], no other Martial Arts Skill [x] - Precision Strike aspects of the same skill level.

**Effect:** Receive a +2 to all Martial Arts rolls that target [species].

**Action:** 

> #### Precision Strike [Species]
>
>**Preparation:** 1 action, fatigue Martial Arts by 1
>
>**Check:** Contest against target's Finesse
>
>**Duration:** Instant
>
>**Target:** A [species] within your reach
>
> **Effect:** Deal percent damage to the target's body equal to your levels in Unarmed Fighting. Target temporarily loses use of one sense for two turns.
>
> Excellent Success: Target falls unconscious until they fatigue body by your Martial Arts level. 
>
> (Damage is blunt damage)

**Special:** You may take this aspect multiple times. Each time, it must be of a different skill level. They may, however, apply to the same species. X has a minimum level of 15

### Master of all trades

**Point Cost:** 40

**Prerequisite:** “Jack of all trades” aspect. CANNOT be taken during character generation, and you must have earned at least 40 character points since character generation.

**Effect:** 	You may buy generic skill aspects (the aspects labeled [ability] skill aspect) wholesale at five times their normal cost, getting the aspect for ALL abilities that meet the aspect's prerequisites. 

Example: If you buy Generic Aspects 1-5 (Beginner - Novice) using this aspect after buying Master of All Trades, and you have Skill aspects 1-4 for all level 5 abilities, any abilities that don't have a level 5 generic skill aspect will automatically have the Generic Ability Skill 5 - Novice Aspect.

*Roleplaying Tips: A Master of all trades has a lot in common with a Jack of all trades, but a master is usually quieter and more observant. By this point, they've come to the depressing realization that they know more and are better at most things than most everyone around them, leaving them few people to learn from. As a result, they'll usually keep quieter, so they aren't distracted from watching for masters in various skills that they could learn from.  When they find one, however, they happily go back to acting like a talkative and inquisitive typical Jack of all trades.*



### Mathematics Skill 6 - Financial Management

**Category:** Ability Skill

**Point Cost:** 6

**Prerequisite:**  6 levels in Mathematics, and one Mathematics skill 5 aspect.

**Action:**
>
>**Preparation:** 8 hours (spend looking at papers, trading, etc.)
>
>**Check:** 20 - the culture's tech level
>
>**Duration:** Instant
>
>**Target:** A collection of finances you have access to within a culture.
>
> **Effect:** Increase available finances by 1% (round down).
>
>**Failure:** Decrease available finances by 1% (round down).

### Mated for Life (Avg.)

A close bond can often be a wonderful thing.

**Category:** Social

**Point Cost:** 0

**Prerequisite:**  Choose a lifetime mate

**Effect:** You are focused on a single other character for your romance. You gain a +5 defensive social modifier against romance rolls that target you, except for your chosen mate, to which you gain a -10 defensive social modifier.

### Mated for Life (Strong)

A perfect match is not very common, but when it happens, nothing can break their bonds of love.

**Category:** Personality

**Point Cost:** 5

**Prerequisite:**  Choose a lifetime mate

**Effect:** You are focused on a single other character for your romance. No character besides your chosen mate may succeed in a romance attempt targeting you. You gain a -20 defensive social modifier on all romance rolls targeting you from your romance partner.



### Memory Skill 0 – Memory Centering

**Category:** Instant

**Point Cost:** 0

**Action:**
>
>**Preparation:** Fatigue Memory 1 (cannot be altered by other aspects) 
>
>**Duration:** Instant
>
>**Target:** Self
>
>**Effect:** Add your might level as a bonus to a Memory roll, or the roll of a lower ability of Memory.

### Might Skill 0 – Might Centering

**Category:** Instant

**Point Cost:** 0

**Action:**
>
>**Preparation:** Fatigue Might 1 (cannot be altered by other aspects) 
>
>**Duration:** Instant
>
>**Target:** Self
>
> **Effect:** Add your Might level as a bonus to a might roll, or the roll of a lower ability of Might.


### Mind Skill 0 – Mind Centering

**Category:** Instant

**Point Cost:** 0

**Action:**
>
>**Preparation:** Fatigue Being 1 (cannot be altered by other aspects) 
>
>**Duration:** Instant
>
>**Target:** Self
>
> **Effect:** For a mental roll, reroll the roll, give a bonus  to a roll equal to your level in Mind, or reduce Mind damage taken in by your level in Mind



### Morality Skill [x] - Views

You've come to a better understanding of what you believe.

**Category:** Ability Skill

**Point Cost:** [x]

**Prerequisite:**  [x] levels in Morality, and one Morality skill [x-1] aspect, no Morality Skill - Views aspect with the same level as this one.

**Effect:** You get a +3 defensive social modifier when a moral of your choice is gone against. (Example morals include: Protecting family, Gaining money, Staying alive, Not causing any kind of social disturbance, Following a code of honor, etc.)

**Special:** You may take this aspect more than once, but it may not have the same level for any use. However, they may share moral choices (resulting in higher support for various views.) Further, if at any time one you fail a defensive social roll that uses one of your views modifiers, but the result of the action supports one of your other modifiers, you may move a +1 views social defense modifier from the defeated moral to the supported moral permanently.

*Roleplaying tips: People with the Views ability aspect rarely hide their morals, and will often share their views readily, often being vocal about them.*



![Energy](./img/Energy1.jpg)

### Morality Skill 2 - Chaotic Ideology

You are in tune with the ideology of chaos. Despite others having the terrible misconception of chaos being evil (generally due to the fact that most get lazy with a degree of order, and don't like the extra work surviving requires in a chaotic environment), you realize that quite on the contrary, it's a vital force of life and goodness. You see the constant chaos of nature, personal interaction, creation and destruction, and more as vitally necessary to continue life and culture. You realize that without chaos, the heartbeat of all things – change – would cease and all things would eventually grind to a halt, effectively becoming as stable as a rock – and effectively as dead as one too. People with Chaotic Ideology constantly try to spark imagination in those around them and fight stagnation where it comes along (usually ideologically rather than actually fighting).

Those with a Chaotic Ideology aren't all necessarily anarchists (although many are, citing all the damage and problems governments cause), and some may see government as useful as long as it serves the people to increase their capabilities and options instead of stifling and controlling them. They also recognize that patriotism is usually is a sign people have become subtly and inadvertently brainwashed, valuing their government for its own sake instead of its purpose. In these cases, a person with Chaotic Ideology has a moral obligation to bring dissent to the forefront. They are firm protectors of freedom in nearly all its forms, and among the greatest opponents of tyranny.

**Category:** Ability Skill

**Point Cost:** 2

**Prerequisite:**  2 levels in Morality, and one Morality skill 1 aspect, you may not have Morality Skill 2 - Ordered ideology.

**Effect:** You are resistant to any form of social control. If anyone would win an assertive social roll to influence you, you may roll a d6. On a 5 or 6, it fails regardless (although the you may choose to fill the request anyway, but are just as likely to do so in a way that meets the words, but not the meaning, or vica-versa.)

*Roleplaying tips: People with a Chaotic Ideology are more likely to be that person who says, “Well, if you like her, why don't you talk with her?” or “I'm bored... lets go do something fun and crazy!”, “Give me Freedom, or Give me Death!”, or “I'll try most anything once.”*

### Morality Skill 2 - Ordered Ideology

You are in tune with the ideology of order. You are aware that people are uncomfortable with changes in their world, and that things that cause them to change can often affect the powers that be in unexpected ways. As society currently works, its stands to reason that any change could upset the delicate balance that society has, and send things tumbling into chaos. 

Those with Ordered Ideology aren't all necessarily corporate tools - or other kinds of tools (although many are), they view protecting the order of the world equivalent to promoting the safety of the world. 

**Category:** Ability Skill

**Point Cost:** 2

**Prerequisite:**  2 levels in Morality, and one Morality skill 1 aspect, you may not have Morality Skill 2 - Chaotic ideology.

**Effect:** If a superior would attempt an inspirational speech action on you, it  automatically succeeds. You receive a +2 social modifier in dealing with organizations you're a part of.

*Roleplaying tips: People with an ordered Ideology are more likely to be that person who says, “Don't fix what isn't broken” or “Just work hard, and good things will come.”, “Why should I have to change?”, or “If today I stand here as a revolutionary, it is as a revolutionary against the Revolution. ”*

### Morality Skill 2 - Organizational Values [Organization]

You are in tune with the ideology of a particular organization. You realize your own limitations, and know you can't have all the answers, and so it makes sense to trust in the judgment of something greater than yourself.

Those with Organizational Values will usually find an organization that fits their own personal values, and over time move their personal views to more match that of the organization.

**Prerequisite:**  2 levels in Morality, and one Morality skill 1 - Views aspect that shares a view with your chosen organization, further, if the organization has membership requirements, you must meet them.

**Category:** Ability Skill, Loyalty

**Point Cost:** 2

**Effect:** When working with a group of five or more that is predominantly made of members of your organization,  you receive a +1 bonus to all rolls.

*Roleplaying tips: People with a organizational values are more likely to be that person who says, “Our leaders know what they're doing.” or “You should have more pride in your country.”, “The system works, don't question it.”, or “Obedience to lawful authority is the foundation of manly character.”*

Some example Organizations and their requirements: 

    • Country - Born into citizenship or completed immigration

    • Religion - Adherence to the religion's belief and participation of an initiation rite (baptism, circumcision, pilgrimage, trials of rite, etc.), possibly specific follower aspects as presented in a fantasy setting.

    • Police Force - Special Training and possible ability requirements.

    • Moral Orders (ex. Lion's Club, Monestaries, etc.) - Take an oath to the order, membership dues (work, money, etc.)

    • Secret Orders - Be chosen and approached for membership.

### Morality Skill 3 - Compassionate

Compassionate people usually do their best to help those around them. They're warmhearted, caring, and generally well-liked as a result. The compassionate person is usually giving without second thought, but in turn, people have a tendency to reciprocate and past kindnesses have a tendency to come back to them at various times, or become more willing to help them in the future. This results in the exact amount of favors that a Compassionate person having at any given time being fluid and a bit random, but a kind of karmic increasing return rule seems to sometimes apply.

**Category:** Ability Skill

**Point Cost:** 3

**Prerequisite:**  3 levels in Morality, and one Morality skill 2 aspect, you may not have Morality Skill 3 - Survivalist  ideology.

**Effect:** Once a day, a compassionate person may roll a d6. If the result is 4, 5, or 6, they should double their assertive social modifiers for that day. If the result is 1 or 2, they should half their available income for that day.

*Roleplaying Tips: Compassionate people generally give to the poor, help out those in need, and put themselves on the line for others. Someone with Compassionate will often say things like, “How can you look at them and not help?”, “My mission is to help the downtrodden and to defend the defenseless.”, “No one should suffer... and if I have to give my life to stop others' suffering, so be it.”, “Do to others what you'd want them to do to you.”, “Love your enemies.”, or “I have a dream that...the sons of former slaves and the sons of former slave owners will be able to sit together at the table of brotherhood.”*

### Morality Skill 3 - Survivalist

All that matters in life is living through as much of it as possible. If you're dead, nothing matters, and while alive, it only matters that you're not dead. Although inherently self-centered, survivalists often work quite well with groups, after all, a group has a better chance of surviving than an individual alone. But if that group is heading for its own destruction, a survivalist will easily abandon what looks like a lost cause. Survivalists often join with the side that appears strongest and follow it as long as it makes them able to survive the world around them. As time goes on, they gravitate towards positions that will make them more secure, more well guarded, give them more power over their surroundings, etc. 

However, as a survivalist moves up to more and more secure social ranks, their focus often changes from moving to that that position to protecting that position. When this happens, the survivalist, no longer quiet and amiable, will do their best to make as little strong enemies as possible even at great cost, but do their best to eliminate any opposition they feel they can reasonably defeat. A friendly and amiable survivalist can often become a tyrant, given the position of power.

**Prerequisite:**  3 levels in Morality, and one Morality skill 2 aspect, you may not have Morality Skill 3 - Compassionate  ideology.

**Effect:** A survivalist gets a +3 on any one roll once per day if the result would make a difference between life and death.

*Roleplaying Tips: Survivalists are usually pretty focused on the needs for survival rather than comforts, what's needed to get by, and they have a tendency to 'not sweat the small stuff' but focus on more important issues. Things a survivalist might say include things like the following: “I'm happy where I am, why should I stick my neck out for a promotion?”, “If you've got your health, you've got everything.”, “It is not truth that matters, but victory.”,  or “You guys can go ahead and get yourselves killed, I'll stay here where its safe.”*

### Morality Skill 5 - Moral Analysis

You've become good at judging the consequences of your actions in regards to what you believe.

**Category:** Ability Skill

**Point Cost:** 3

**Prerequisite:**  3 levels in Morality, and one Morality skill 2 aspect

**Action:** 

### Moral Analysis
>
>**Preparation:** Fatigue Morality by 1
>
>**Check:** Auto-success
>
>**Duration:** Instant
>
>**Target:** A defensive social roll that you have not used morality on yet. (Must be either your own roll, or that of a character you can communicate with.)
>
>**Effect:** Roll receives a bonus equal to your level in morality.

### Morality Skill 9 - Moral Persuasion

You've become good at understanding the morals of others, and figuring out how to interact with them on their own terms.

**Category:** Ability Skill

**Point Cost:** 9

**Prerequisite:**  9 levels in Morality, and one Morality skill 8 aspect

**Action:**

> #### Moral Persuasion
>
>**Preparation:** 3 minutes conversation with a character who you have not targeted with Moral Persuasion today.
>
>**Check:** Contest target Character's Morality + Creativity (apply social modifiers)
>
>**Duration:** Permanent
>
>**Target:** Listening Characters
>
>**Effect:** Do one of the following:
>
>* Target gains a  -1 defensive social modifier for every 10 by which you pass their Morality.
>
>* Remove a -1 defensive social modifier of someone else's on the target for every 10 by which you pass their Morality.
>
>* Temporarily remove either a +1 or -1 defensive social modifier from morality on the target character for every 5 by which you pass their morality, this lasts for only an hour.
>
>**Failure:**  Target gains a +1 defensive social modifier concerning you for every 1 by which you failed to pass their morality.
>
>Note: You only roll once, that roll is used for all Listening Characters.

### Music Skill 5 - Melody

"It's got a good tune, and I can hum to it."

**Category:** Ability Skill

**Point Cost:** 5

**Prerequisite:**  5 levels in Music, and one Music skill 4 aspect

**Action:**

> #### Melody
>
>**Preparation:** 3 minutes, fatigue music by 1
>
>**Check:** 10
>
>**Duration:** Maintained
>
>**Target:** Anyone that can hear.
>
> **Effect:** Mind Fatigue healed by 1 by however much the roll surpasses the check (and then Mind damage if all fatigue is healed for each additional 1). 

*Note: Notice the duration is “Maintained” on the healing, this means that the damage and fatigue returns should the musician stop the music.*

### Music Skill 5 - Beat

"It's got a good beat, and I can dance to it."

**Category:** Ability Skill

**Point Cost:** 5

**Prerequisite:**  5 levels in Music, and one Music skill 4 aspect

**Action:**

> #### Beat
>
>**Preparation:** 1 turn, Fatigue Music by 1
>
>**Check:** Twice amount of Fatigue of targeted ability. (Make only one check for all affected, check their fatigue against it.)
>
>**Duration:** 2 turns.
>
>**Target:** An ability of everyone within 1 meter per level of Music (with the exception of the player using it)
>
> **Effect:** Anyone affected may act as if they had all “Push it to the Limit” aspects, and don't have to take damage until after the duration ends. Further, if they rest for an equal amount of turns after the duration ends, they may choose to not take the damage from pushing to the limit.

### Music Skill 8 - Syncopation

"Woah, the music is like... woah... I need to sit down..."

**Category:** Ability Skill

**Point Cost:** 8

**Prerequisite:**  8 levels in Music, and one Music skill 7 aspect

**Action:**
>
>**Preparation:** 3 minutes, fatigue music by 2
>
>**Check:** 10
>
>**Duration:** Maintained
>
>**Target:** Anyone that can hear.
>
> **Effect:** Mind is fatigued by 1 by however much the roll surpasses the check (and then damage for 1 is caused for each additional 1 fatigue that would be caused if Mind is completely fatigued). 

*Note: Notice the duration is “Maintained” on the fatigue and damage, this means that the damage and fatigue goes away should the musician stop the music.*

### Music Skill 12 - Catchy tune

"It's a great song, but I can't get it out of my head!"

**Category:** Ability Skill

**Point Cost:** 12

**Prerequisite:**  12 levels in Music, and one Music skill 11 aspect

**Effect:** You may declare any use of melody or syncopation you do to be a catchy tune. If so declared, the results of a Melody or Syncopation action doesn't go away after you stop maintaining it (treat healing as normal healing, and syncopated fatigue as normal fatigue.)

### Music Skill 12 - Target Audience [Audience Trait]

"I don't get what's so great about that music, why do so many people like them?"

**Category:** Ability Skill

**Point Cost:** 12

**Prerequisite:**  12 levels in Music, and one Music skill 11 aspect

**Effect:** The music you play is tailored to a specific audience. You may choose one identifying trait, and have the music affect only people with that trait (examples include 'Goths', 'Punk fans', 'People patriotic to the British Crown', etc.) If you would be unaffected by the music, your check result is reduced by 10.

### Obesity [x]

Chubby, chunky, fluffy, rotund, round,  gravitationally gifted... 

**Category:** Physical

**Point Cost:** -10 times x

**Prerequisite:** No levels in Skinny

**Effect:** Your character's movement speeds are slower by 10% for every level of this aspect (round down to the nearest meter per second, minimum of 0.) Each level increases your weight above the normal for your species by 30%.

### Observe Skill 4 - Doubletake

You have a second sense on when you should look closer at stuff you'd otherwise overlook.

**Category:** Ability Skill

**Point Cost:** 4

**Prerequisite:**  4 levels in Observe, and one Observe skill 3 aspect

**Action:**

> #### Doubletake
>
>**Preparation:** 1 action, Fatigue Observe by 1
>
>**Check:** set by GM
>
>**Duration:** Instant
>
>**Target:** An event that just happened that you haven't used doubletake on.
>
> **Effect:** If you made an observe check, you may remake it, using the higher of the two rolls.

*GM Note: In order to keep the game realistic, it is generally a good idea to roll this ability's check for the player, in order to prevent them from acting based on the die roll instead of what they observed.*

### Opponent, Rival [Name]

Who started it? It doesn't really matter. Your rival gets on your nerves, and you get on your rival's nerves. You both face off against each other all the time, and it's anyone's guess who will come on top each time. 

**Category:** Background & Destiny

**Point Cost:** -10

**Prerequisite:** GM permission

**Effect:** You gain 2 character points per gaming session.

There is another specific character you fight on a regular basis.  This character has the same number of character points as you (give or take up to 15% at GM discretion), and will keep within that range of your character points.

Any time there is a competition in which you are both involved, you must compete against each other. Any time there is any kind of conflict, there's a 10% chance (1 on a 1d10) that they will show up to face off against you (and insist that they specifically face off against you). If they would be in the conflict for storyline reasons, they will target you specifically. You get a -10 assertive social modifier towards your rival.

If your rival ever dies, you lose this aspect.

### Opponent, Arch-nemesis [Name]

They are that omnipresent force that you can feel down the back of your neck, they're that chill that runs down your spine. You know you're going to have to face them and end this once and for all someday, but how, you can only fathom.

**Category:** Background & Destiny

**Point Cost:** -20

**Prerequisite:** GM permission

**Effect:** You gain 5 destiny points per gaming session.

There is another specific character you fight (or fight the minions of) on a regular basis. This character has at least three times the number of character points as you (though it can be significantly more. Although you can eventually catch up to your Arch-nemesis.)

Any time there is a competition in which you are both involved, and you have the opportunity to act against your arch-nemesis, you must take it. Any time there is any kind of conflict, there's a 10% chance (1 on a 1d10) that they or their minions will show up to face off against you. If they would be in the conflict for storyline reasons, they will target you specifically. You get a -10 assertive social modifier towards your target.

If your arch nemesis ever dies, you lose this aspect.

Against your arch-nemesis, any time you would target them and contest them, you get a +2 on every roll. Further, when you face off directly against them, any destiny point cost is half off (round down, minimum one.)

### Patriotism

It doesn't matter what country you live in or the reason you live in it. What matters is you live in that country, and that makes it the best country because it's your home. You will likely protect it within an inch of your life, and support it no matter the path it takes, because if the country falls, all that's left is uncertainty.  

**Category:** Loyalty - Government

**Point Cost:** - 5

**Effect:** You must make a Sanaty roll with a difficulty of 15 anytime a government official from your culture lies and you would otherwise know it. If you fail, you must act as if it is true, probably eventually coming to believe the lie. You get a -1 to any assertive social roll involving someone of a different culture, and a -1 defensive social modifier to any member of your government. When sides are taken in any conflict, you must side with the side your country supports.

*Roleplaying Tips: Patriotic people are usually a bit one-sided when someone disagrees with the values of their culture, and will generally associate what they find 'good' with their country or culture. Examples of things Patriotic people may say include, “Homemade Pie is what my country is all about.”, “You should never shirk from your duty to your country!”, “The doom of a nation can be averted only by a storm of flowing passion, but only those who are passionate themselves can arouse passion in others.”, “By the will of the Emperor, I will rend you limb from limb!” or “It's right because it's the Law!”*

### Personality Skill 1 - Friendship

"Keep your friends close..."

**Category:** Ability Skill

**Point Cost:** 1

**Prerequisite:** 1 level in personality

**Effect:** If you have a number of days of interaction with another character greater than their combined amount of friendship-originating social modifiers; you may make a personality check, contesting 5 + all social modifiers they have that might apply to you. If you pass the check, you gain a +1 assertive social modifier towards the chosen character, and they gain a +1 assertive social modifier towards you.

### Personality Skill 1 - Break Bonds

"...and keep your enemies closer."

**Category:** Ability Skill

**Point Cost:** 1

**Prerequisite:** 1 level in personality

**Action:**

> #### Break Bonds
>
>**Preparation:** Fatigue Personality by 1, Reaction
>
>**Check:** Combined social Modifiers the Character has towards you. 
>
>**Duration:** Permanent
>
>**Target:** Target who just failed at an attempt to gain (or use) social modifiers involving you.
>
> **Effect:** Target loses all assertive social modifiers specific towards you, and you both mutually have a +4 defensive social modifiers towards each other.

### Personality Skill 5 - Attack Pride

For some, their honor or pride is their greatest strength but also their greatest weakness. 

**Category:** Ability Skill

**Point Cost:** 5

**Prerequisite:** 5 levels in personality, one personality skill 4 aspect.

**Action:**

> #### Attack Pride
>
>**Preparation:** Fatigue Personality by 1, 3 turns
>
>**Check:** Contest target's Personality (apply social modifiers)
>
>**Duration:** Instant
>
>**Target:** Another Character you can communicate with
>
> **Effect:** You may do one of the following to the target:
>
> * Cause 5% damage to their Mind
>
>* Fatigue one of their mental abilities by one for each level in personality they have more than you.
>
>* Target gains a -1 defensive social modifier towards you that lasts the day.
>
>**Failure:** Target gains a +2 defensive social modifier with things involving you.

### Personality Skill 5 - Fashion Ambivalence

There are beautiful people in the world, but you know beauty doesn't matter for anything. It's what's inside that counts. Beautiful people may be eye catching, but it arouses your suspicion just as much, if not more, than your awe of them. 

**Category:** Ability Skill

**Point Cost:** 5

**Prerequisite:** No Beauty aspects, 5 levels in personality, one personality skill 4 aspect.

**Effect:** 

You may not use First Impression or Flaunt (although you may still make beauty checks), but you also can't be targeted by beauty or its uses. Also someone else's Damaged Appearance does not affect their interactions with you.

*Roleplaying tips: Someone who has fashion Ambivalence is just as likely to be friends with someone horrendously ugly as they are someone who looks great. Generally, they don't work much on their own looks either, doing grooming necessary for health (such as bathing) but rarely 'sprucing up'. This results in a more 'down to earth' appearance for most of them. They may still be beautiful, but if they are, it's natural and not the result of trying to look it.*

### Philosophy Skill 2 - Philosophical Judgment

**Category:** Ability Skill

**Point Cost:** 2

**Prerequisite:**  1 level in Philosophy, and one Philosophy skill 1 aspect

**Effect:** You may roll Philosophy instead anytime you would roll for Morality.

### Philosophy Skill 4 - Philosophical Analysis

**Category:** Ability Skill

**Point Cost:** 4

**Prerequisite:**  4 levels in Philosophy, and one Philosophy skill 3 aspect

**Action:**

> #### Philosophical Analysis
>
>**Preparation:** 1 turn, must be in an argument, fatigue Philosophy by 1
>
>**Check:** Contest Target's Process
>
>**Duration:** The argument
>
>**Target:** Another character in an argument
>
>**Effect:** Target loses last effect added to the argument, or you double last effect added by an opponent of the target (your choice).

### Philosophy Skill 10 - Paradigm Shift

**Category:** Ability Skill

**Point Cost:** 10

**Prerequisite:**  10 levels in Philosophy, and one Philosophy skill 9 aspect

**Action:**

> #### Paradigm Shift
>
>**Preparation:** 10 minutes
>
>**Check:** Contest Target's Creativity (apply appropriate social modifiers)
>
>**Duration:** Permanent
>
>**Target:** A character you can communicate with.
>
>**Effect:** Target adds a +1 or -1 defensive social modifier of your choice.

### Phobias

**Category:** Personality

**Point Cost:** Variable

**Effect:** The character is irrationally afraid of something. When they encounter the object of their fear, they will enter a different mindset called a panic attack. Given time after the initial exposure, some of the more lightly affected people will come to their senses after they get over their initial rush of irrational fear, however, some can't overcome it. Phobias are rated based on how often they are an issue on average, and how long a panic attacks lasts. 

|Duration | < once per  day Frequency | About once per day | > once per day |
|---|---|---|---|
|1d6 turns | Cost: -5 | Cost: -10 | Cost: -25
|1d6 minutes|Cost: -20|Cost: -30|Cost: -40|
|Games w/ GM: Until object of fear is removed  & Games w/o GM:  1 hour|Cost: -35|Cost: -45|Cost: -60|

When in a panic attack, a character has two options. Freeze (forfeiting all actions) or running. If neither will prevent an encounter with the object of fear (such as someone with a fear of spiders being stuck in a corner and approached by a spider), they may roll a Sanity check. If they pass a difficulty of 10, they may gain the option to attack the object of their fear as well. 

A panic attack may be ended early by the attentive care of another character. The other character can't do anything but attend the character in the panic attack for the duration, but doing so (and passing a personality check with a difficulty of 10) reduces the duration by half. 

In a game with no GM:  the player rolls a dice at the beginning of each period of intense action. If the result of the roll is a 1, the object of fear is present or referenced in some way. (If less than once per day, the dice is a d20, if once per day, a d12, if more than once per day, a d4.) What the object of fear is may be noted, but is otherwise irrelevant to gameplay.

In a game with a GM: Talk over options for what the phobia is with the GM. How common (or uncommon) it is should be decided based on the frequency, and with the exception of characters learning the phobia (and abusing it), or weird exceptions (such as an arachnophobic entering a city of spider-worshippers), the object of fear shouldn't appear much more or less often than expected.

*Roleplaying Tips: People with phobias are generally unaffected by their phobia in day-to-day life. As most people with phobias don't like to think about it, sometimes a character can be known for years without knowing about their phobias.  However, when the phobia strikes, the character displays a lot of irrationality, because at its core, that's what a phobia is, an irrational fear. There is no overcoming such a fear without time, effort, and preparation. Although many phobias are about stuff that make sense to be afraid of (such as lightning, stinging insects, or country music), a lot of phobias can be about something that's not inherently dangerous, such as the color pink, rodeo clowns, or touching light switches.*

### Physical Adept

**Point Cost:** 20

**Prerequisite:** 5 levels in at least 4 different Body abilities.

**Effect:** Once per day, a character may treat an ability as if it has [Ability] Skill 2- Surge or [Ability] Skill 4 - Intensity, even if it doesn't and doesn't have the prerequisites.

*Roleplaying Tips: Someone who is a physical adept is good at acting quickly, with grace and skill, even if they don't know what they're doing, to at least make it look like they do. Impromptu motions often look well practiced and fluid.*

Physical Tolerance

**Point Cost:** 15

**Prerequisite:** 5 levels in Toughness

**Action:**

> #### Physical Tolerance
>
>**Preparation:** Fatigue Toughness by 1
>
>**Check:** Auto-success
>
>**Duration:** Instant
>
>**Target:** Self
>
> **Effect:** Ignore the onset of problems from failing to meet a survival requirement for one additional turn beyond this one. This can not be used more than once per problem.

### Politics Skill 5 - Campaign

**Category:** Ability Skill

**Point Cost:** 5

**Prerequisite:**  5 levels in Politics, and one Politics skill 4 aspect

**Effect:** If you spend 2 months in game working on a political campaign, may make a politics check with a difficulty of 15 + 5 for each major opposing campaign. You may give a target politician or political issue gain a +2 assertive social modifier towards general populace for one month in game (Player Characters don't count as general populace. Further, special groups within the population also do not count as general populace.)

If you are in a society where politicians/issues are voted on, if your politician/issue has a greater assertive modifier than any competing politician/issue when the vote happens, then it will win the vote. However, if you are in a society where the populace doesn't get a say in the politicians/issues, then upon reaching this point, a rebellion will likely start when the politician/issue has a greater assertive modifier than the current regime.

### Politics Skill 20 - Legal Immunity [culture]

**Category:** Ability Skill

**Point Cost:** 20

**Prerequisite:**  20 levels in Politics, and one Politics skill 19 aspect

**Effect:** You are immune to the laws of a given culture (within reason). This may be due to a treaty that will ship you to a different country to be tried for any crimes, that you rule the given culture, or other reasons, but regardless, you are unaffected by the culture's laws. (The actions of individuals, however, are not stopped, only the laws are prevented from affecting you. Also, if you're immune due to being a foreign diplomat, this doesn't stop the affected government from shipping you back home to be tried there.)

### Procedure Skill 3 - Repetition

**Category:** Ability Skill

**Point Cost:** 3

**Prerequisite:**  3 levels in Procedure, and one Procedure skill 2 aspect

**Effect:** If you have done the same task 5 actions in a row and passed checks, as long as you continue to do only that action, you will auto-pass every time as long as no variables for that action change.

### Purge [Ability]

**Category:** Physical or Mental (or possibly another 2nd Tier related category if your campaign has additional 2nd Tier abilities.)

**Point Cost:** 10

**Action:**

> #### Purge
>
>**Preparation:** Instant, Fatigue [Ability] by 1 (this amount may not be reduced in any way.)
>
>**Check:** Auto-success
>
>**Duration:** Instant
>
>**Target:** Any one of [Ability]'s lower abilities
>
>**Effect:** 
>
> Heal 1 fatigue of target ability



![Spirit](./img/Spirit.jpg)

### Push past the Limit [2nd Tier Ability]

**Category:** Physical or Mental (or possibly another 2nd Tier related category if your campaign has additional 2nd Tier abilities.)

**Point Cost:** 10

**Effect:** You may take 5% damage to the 2nd Tier ability instead of fatiguing the ability (or one of its lower abilities), allowing you to use abilities even if you are fully fatigued. If you reach 100% damage this way, instead of going unconscious, you 'snap'. If body reached 100% damage, your body just gives out and you can't move (and breathing too hard to speak), and if you hit 100% mind, you may just collapse, or the GM may have you suffer temporary insanity.

### Quick on the uptake

**Point Cost:** 20

**Prerequisite:** 5 levels in at least 4 different Mind abilities.

**Effect:** Once per hour, a character may temporarily increase the level of any broad skill or specialized skill that they are trained in by 1. This increase lasts for an hour. 

*Roleplaying Tips: Someone quick on the uptake will often seem confused and curious about something within their own thoughts, and prone to moments of realization to these unknown thoughts.  Then they may proceed to act on it, if not, they'll usually end up forgetting what they were thinking about altogether as something else catches their mind's fancy.*

### Recruit - Skill 12

(Ability Skill for Inspire, Personality, or Conversation. Choose which one this ability is for upon taking it.)

**Category:** Ability Skill

**Point Cost:** 12

**Prerequisite:**  12 levels in either Inspire, Personality, or Conversation, and one level 11 aspect in the same ability.

**Action:** 

> #### Recruit
>
>**Preparation:** Fatigue Inspire, Conversation or Personality by 3, a number of minutes equal to 1/10th of the target's character points.
>
>**Check:** Contest target's morality (all social modifiers applied)
>
>**Duration:** Permanent
>
>**Target:** A character whose largest social modifier is you (and tends them towards actions to help you.)
>
> **Effect:** Target becomes your assistant with 
> 
> **Auto-Failure:** You automatically fail if gaining this assistant would give you more assistants than your level in being.

### Regeneration Skill 1 - Regenerative Healing

**Category:** Ability Skill

**Point Cost:** 1

**Prerequisite:** 1 level in Regeneration

**Effect:**  If you rest for 1 hour, you may make a regeneration check. The check has a difficulty equal to the amount of body damage you've taken. 

If you pass, you heal an amount of body damage equal to the difference between the check difficulty and the result.

### Resolution Skill 1 - Resolve

**Category:** Ability Skill

**Point Cost:** 1

**Prerequisite:** 1 level in Resolution

**Action:** 

> #### Resolve
>
>**Preparation:** Fatigue Resolution by 1, 
>
>**Check:** Amount of Mind Damage you have taken
>
>**Duration:** Instant
>
>**Target:** Self
>
>**Effect:** Prevent all Mind Damage that would be taken in the next turn that's not self-inflicted.
>
>**Failure:** Take another 3 points of Mind Damage

### Rest

**Point Cost:** Free

**Action:**
>
>**Preparation:** At least 8 hours without activity
>
>**Check:** Auto-success
>
>**Duration:** Instant
>
>**Target:** Self
>
> **Effect:** 
> 
> Do one of the following:
> 
> * Heal Body, Mind, and any other 2nd Tier ability's damage by 10% each.
> 
> * Heal all Fatigue to all abilities (including Being).

### Romance Skill 1 - Gender Preference

**Category:** Ability Skill, Personality

**Point Cost:** 1

**Prerequisite:** 1 level in Romance

**Effect:** You find a certain gender more attractive than others. 

When taken, you receive a -4 defensive social modifier towards anyone having your preferred gender. However, when targeted by Romance of someone who does not match your preference, you receive a +20 defensive social modifier.

### Romance Skill 2 - Ability Preference [Ability]

**Category:** Ability Skill, Personality

**Point Cost:** 2

**Prerequisite:** 2 levels in Romance, one Romance Skill 1 aspect

**Effect:** You find a certain skills highly attractive. 

When taken, you receive a negative defensive social modifier towards anyone having more levels in the chosen ability than you. However, when targeted by Romance of someone who does not exceed your levels in your chosen ability preference, you receive a +15 defensive social modifier.

### Romance Skill 3 - Aspect Preference [Aspect]

**Category:** Ability Skill, Personality

**Point Cost:** 3

**Prerequisite:** 3 levels in Romance, one Romance Skill 2 aspect

**Effect:** You find a certain traits highly attractive. 

When taken, you receive a -5 social modifier towards anyone having the chosen aspect. However, when targeted by Romance of someone who doesn't have the chosen aspect, you receive a +15 defensive social modifier.

### Romance Skill 3 - Woo

**Category:** Ability Skill

**Point Cost:** 3

**Prerequisite:** 3 levels in Romance, one Romance Skill 2 aspect

**Action:**

> #### Woo
>
>**Preparation:** 3 minutes of communication with target character.
>
>**Check:** Total combined social modifiers of target character that would involve you.
>
>**Duration:** 1 day (additional uses increase duration by additional days)
>
>**Target:** A character which could hold romantic interest for user. 
>
> **Effect:** Gain +1 assertive Social Modifier to all interactions with target during duration. The whole total lasting the whole duration
>
>**Failure:** Suffer -1 Social Modifier to all interactions with target during duration.

### Romance Skill 8 - Gold-dig

**Category:** Ability Skill

**Point Cost:** 8

**Prerequisite:** 8 levels in Romance, one Romance Skill 7 aspect

**Action:**

> #### Gold-dig
>
>**Preparation:** 3 minutes of communication with target character, once per day.
>
>**Check:** Total combined social modifiers of target character that would involve you.
>
>**Duration:** 1 day (additional uses increase duration by additional days)
>
>**Target:** A character which could hold romantic interest for user. 
>
> **Effect:** Gain access to 1% of target's wealth (cumulative with previous uses). Using any of target's wealth resets wealth you have access to what you had before you used Gold-dig on the character. 
>
>**Failure:** Suffer -1 Social Modifier to all interactions with target during duration and reset your finical access.

### Rush Skill 8 - Burst

**Category:** Ability Skill

**Point Cost:** 8

**Prerequisite:** 8 levels in Rush, one Rush Skill 7 aspect

**Action:**

> #### Burst
>
>**Preparation:** Fatigue Rush by 1
>
>**Check:** 1 + Body damage you've taken
>
>**Duration:** 1 turn
>
>**Target:** Self
>
> **Effect:** By each 5 that you pass the check, add +1 to your might or speed for this turn. You may not use burst again for an hour.
>
>**Failure:** Pull a muscle (or equivalent) and fatigue body by 10.
>
> Excellent Failure: Add +1 to your might and speed for this turn, but after this turn, your body fatigues by 10 and you can't use burst again for an hour.

### Sanity Skill 0 – Sanity Centering

**Category:** Instant

**Point Cost:** 0

**Action:**
>
>**Preparation:** Fatigue Sanity 1 (cannot be altered by other aspects) 
>
>**Duration:** Instant
>
>**Target:** Self
>
> **Effect:** Reduce Mind damage taken by your Sanity.



### Skinny [x]

**Category:** Physical

**Point Cost:** -8 times [x]

**Prerequisite:** No levels in Obesity

**Effect:** You are lighter than typical. Your weight is reduced by 10% for every level of this aspect. (Max level is 5) Adjust how much weight you can handle appropriately (max carry weight is affected by your weight). Further, when you take damage, take X more damage.

### Sneak Skill 3 - Stealth

**Category:** Ability Skill

**Point Cost:** 3

**Prerequisite:** 3 levels in Sneak, one Sneak Skill 2 aspect

**Action:** 

> #### Stealth
>
>**Preparation:** 1 action, fatigue sneak by 1
>
>**Check:** Contest against targets' Observe, Continued each turn
>
>**Duration:** Maintained
>
>**Target:** Any character who may end up seeing you who does not already know where you are.
>
> **Effect:** Targets do not know you are there, and may not react to you being there as long as you maintain Sneak.

### Sneak Skill 6 - Steal

**Category:** Ability Skill

**Point Cost:** 6

**Prerequisite:** 6 levels in Sneak, one Sneak Skill 5 aspect

**Action:**

> #### Steal
>
>**Preparation:** 1 minute
>
>**Check:** Contest vs Observe of person who has Possession. (And vs Observe of anyone who may be watching the possession.)
>
>**Duration:** Instant
>
>**Target:** Possession of another character that you can see (or easily see evidence of, such as the bulge of a pocket holding a wallet.)
>
> **Effect:** You successfully get the possession. 
>
>**Failure:** You are detected by the person. 

### Soul: Beginner's

**Category:** Meta

**Point Cost:** 0

**Prerequisite:** Game with a GM, and GM approval, and should be a beginning player

**Effect:** Any time the player asks about game mechanics they haven't asked before, they get a +1 to their next roll. Also, if their character dies, they may instantly take control of the character's long lost sibling/clone/identical twin/soul copy/resurrected form/etc. This 'new' character may rush onto the scene at any time if the GM so allows. This 'new' character is identical in all ways to their previous one. (Although, if the player so desires, they may act different , have different memories, different name, and/or a slightly different appearance.)



![Spiraltaur](./img/Spiraltaur.jpg)

### Soul: Blazing

**Category:** Meta

**Point Cost:** 5

**Prerequisite:** Game with a GM, and GM approval, must be taken at character creation

**Effect:** When starting a game or campaign a character starts out with 50% more character points. However, they may not gain any character points after that (yes, this aspects 5 points does come out of that.). If this character should die, the player's next character in this campaign starts out with a total of one less character point than this character had. Further, that character must take Blazing Soul as well (but the new character does not get an additional 50% points.)



### Soul: Caged

**Category:** Meta

**Point Cost:** -5

**Prerequisite:** Game with a GM, and GM approval (in fact, the GM may require some players to take it.)

**Effect:** Receive +2 to the level of Being for gaming session. Any time you disagree with the GM about the game rules or a GM ruling, and argument ensues, this bonus gets a -1 penalty for every 20 seconds of argument (this does mean the bonus can become a negative, and it can kill the character). Further, when the GM applies the negative, arguing about the amount of the negative or time of argument counts as time of argument, and GM may even increase the penalty arbitrarily during an argument as the GM sees fit.



(A good GM should only require players to take this ability as a last resort. Since a GM is effectively the referee of the game, the GM's calls on things should be final. The purpose of this aspect is when a player argues so much all the time that it makes the game not fun for other players, this serves as impetus to keep said arguments short... and then reward a player who would normally reduce the fun of the game with a nice +2 and a base bonus of 5 character points for when they actually behave well instead.)

### Soul: Energizing

**Category:** Meta

**Point Cost:** 1

**Prerequisite:** Game with a GM, and GM approval

**Effect:** Any time a gaming session starts, and you have supplied any kind of food or drink for everyone at the gaming session, once during that gaming session you may heal any (or all) players of all of their fatigue. 

If the food is homemade by the player, they can also restore 5% damage in Body and Mind as well.



### Soul: Gamer’s

**Category:** Meta

**Point Cost:** 10

Prerequisites: Game has a GM, and GM permission

**Effect:** Your character plays TOY (or their world's equivalent). As a result, your character can talk about people's abilities, levels, and aspects like you do. Since TOY mimics real life in its applications, you can quickly relay analytical information on your surroundings that otherwise are harder to define. As a result, once per gaming session, you may give a +1 to any roll after you roll. If assisting another player who has gamer's soul for that roll, you may increase it to a +2.



### Soul: Guardian

**Category:** Meta

**Point Cost:** 20

Prerequisites: Game has a GM, and GM permission

**Effect:** The guardian spirit aspect represents that 'still small voice', a conscious, that internal voice that tells you what's a good idea, the leading of the subconscious, or some kind of guardian force that guides your actions. Although it should be noted that these often give good ideas, occasionally this isn't the case. 

At any point, you may demonstrate your personal knowledge or skills in an area, and the GM will rate it, giving your character an appropriate bonus. (from a -3 to a +3). Examples include giving a speech as a basis for your character's speech check, drawing a diagram of how to pick a lock to give your character a bonus to do so, demonstrating a particular style of parry with a practice sword or dowel rod to do the same move in game, etc.

### Soul: Pacified

**Category:** Meta

**Point Cost:** -10

**Prerequisite:** Game with a GM, and GM approval

**Effect:** If the actions of the player's character (directly or by design) result in the death of a non-hostile character (NPC or PC), any damage the player's character currently has is impossible to heal, regenerate, or replace in any way (it will always remain). If at the time they would use this ability, they have no damage, they may take their choice between 1% damage to either body or mind (if applicable.)

Note: GM may require a player to take this aspect.

### Speed Skill 0 – Speed Centering

**Category:** Instant

**Point Cost:** 0

**Action:**
>
>**Preparation:** Fatigue Speed 1 (cannot be altered by other aspects) 
>
>**Duration:** Instant
>
>**Target:** Self
>
>**Effect:** For Speed or a lower ability, you may make your action this turn happen before an action just declared. (Note: They may do the same to this action.)
>
>OR
>
> You may take an extra action this turn (Note: must be used for an action of Speed or a lower ability.)



### Sprint Skill 1 - Fastwalk

**Category:** Ability Skill

**Point Cost:** 1

**Prerequisite:** 1 levels in Sprint

**Action:** 

> #### Fastwalk
>
>**Preparation:** Fatigue Sprint by 1
>
>**Check:** Auto-pass
>
>**Duration:** Maintained
>
>**Target:** Yourself
>
> **Effect:** You may multiply your base speed by 1.5 

(For reference, a normal walk for a normal human is about 5 kph)

### Sprint Skill 2 - Jog

**Category:** Ability Skill

**Point Cost:** 2

**Prerequisite:** 2 levels in Sprint, one Sprint Skill 1 aspect

**Action:** 

> #### Jog
>
>**Preparation:** Fatigue Sprint by 2
>
>**Check:** Auto-pass
>
>**Duration:** Maintained
>
>**Target:** Self
>
>**Effect:** You may multiply your base speed by 2, fatigue Sprint by another 1 every minute you maintain the Jog

### Sprint Skill 4 - Run

**Category:** Ability Skill

**Point Cost:** 4

**Prerequisite:** 4 levels in Sprint, one Sprint Skill 3 aspect

**Action:** 

> #### Run
>
>**Preparation:** Fatigue Sprint by 3
>
>**Check:** Auto-pass
>
>**Duration:** Maintained
>
>**Target:** Self

>**Effect:** You may multiply your base speed by 3, fatigue Sprint by another 1 every turn you maintain the Run

### Sprint Skill 8 - Sprint

**Category:** Ability Skill

**Point Cost:** 8

**Prerequisite:** 8 levels in Sprint, one Sprint Skill 7 aspect

**Action:** 

> #### Sprint
>
>**Preparation:** Fatigue Sprint by 4
>
>**Check:** Auto-pass
>
>**Duration:** Maintained
>
>**Target:** Self
>
> **Effect:** You may multiply your base speed by 4, fatigue Sprint by another 2 every turn you maintain the Sprint. 
>
> If you completely fatigue Sprint during a sprint, you must make a Finesse Check with a difficulty equal to the fatigue you have taken in Sprint to avoid falling prone.

### Sprint Skill 18 - Top Sprint

**Category:** Ability Skill

**Point Cost:** 18

**Prerequisite:** 18 levels in Sprint, one Sprint Skill 17 aspect

**Action: 

> #### Top Sprint
>
>**Preparation:** Fatigue Sprint by 8
>
>**Check:** Auto-pass
>
>**Duration:** Maintained
>
>**Target:** Self
>
>**Effect:** You may multiply your base speed by 8, fatigue Sprint by another 4 every turn you maintain the Top Sprint. 
>
> If you completely fatigue Sprint during a Top sprint, you must make a Finesse Check with a difficulty equal to the fatigue you have taken in Sprint to avoid falling prone. If you fail the check, you also take body damage equal to your level in Sprint.

### Sprint Skill 26 - Max Sprint

**Category:** Ability Skill

**Point Cost:** 26

**Prerequisite:** 26 levels in Sprint, one Sprint Skill 25 aspect

**Action:** 

> #### Max Sprint
>
>**Preparation:** Fatigue Sprint by 10
>
>**Check:** Auto-pass
>
>**Duration:** Maintained
>
>**Target:** Self
>
> **Effect:** You may multiply your base speed by 10, fatigue Sprint by another 5 every turn you maintain the Top Sprint. 
>
> If you completely fatigue Sprint during a Max sprint, you must make a Finesse Check with a difficulty equal to the fatigue you have taken in Sprint to avoid falling prone. If you fail the check, you also take body damage equal to your level in Sprint. Then make a toughness check with the same difficulty. If you fail the toughness check, you immediately fall unconscious.

### Steady Income [x]

**Category:** Culture

**Point Cost:** 10 times [x]

**Prerequisite:** Part of a culture that has money.

**Effect:** You gain half the culture's average daily wage every day. (Round down). If taken multiple times, the amount increases respectively (Steady income [1] gives ½ the listed income, Steady Income [2] gives the full income, Steady Income [3] gives 1 ½ the income, Steady income [4] gives twice the full income, etc.) It should be noted that this is generally not earned money. It is usually income gained through stock holdings, social security, regular allowance from parents, etc. For each level, increase your starting income by 50 times the daily wage.

(Note: for a child's allowance, a character may take only half or a quarter of a level in steady income to reflect the small amount of money they get, costing 5 points and 2 points respectively)

### Something to Prove

You take pride in your ability. However, there are other people out there who think they're better than you. Well, that's where they're wrong. And if by some chance they're not wrong, someday, they will be!

**Category:** Background & Destiny

**Point Cost:** 10

**Prerequisite:** GM Permission

**Effect:** If you would fail at a roll using your highest leveled ability (or one tied for your highest level), you may spend one destiny point to add +5 to your result.

### Suicidal [level]

**Category:** Personality

**Point Cost:** -30 per level

**Prerequisite:** Game has a GM, and GM permission

**Effect:** Every day you must make a sanity roll against a difficulty of 4 per level of suicidal you have. If you fail by not passing the check or by rolling minimum on your dice, you must attempt to end your life in a way that should reasonably succeed if no one interferes. 

Although this drawback usually gets in the way of gameplay, it may be fitting for a character such as an inadvertently fallen angel, a dishonored samurai, an emotionally needy character abandoned, or other depressed character types with other players always having to watch them to keep them alive.

*Roleplaying Tips: Suicidal people have often have one somewhat disturbing trait in their speaking. They won't stop talking about how bad the world is, or about how wonderful an afterlife will be, or about their lost honor, etc.* 

### Supplemental Health

**Category:** Tool, Free

**Point Cost:** None

**Prerequisites:** Game has a GM, and GM permission.

**Effect:** 

The character takes regular supplements (Medicines, Drugs, Herbel remedies, Vitamins, specialty foods, etc.) meant to alter performance in some way.

Each individual supplement will belong to a stack. Each stack may have only one of each A & B category. 

A player may only have a number of stacks equal to 3 + their sanity. 

Various supplements are listed in the Possessions section.

The player may ignore the cost for supplements in their stack as long as the total dosage cost of their supplements is less than or equal their *levels* in Steady Income Aspect plus *half their levels* in an ability they have the [Ability] Skill 7 - Hireable aspect in.

### Survival Skill 5 - Nature Sense

**Category:** Ability Skill

**Point Cost:** 5

**Prerequisite:** 5 levels in Survival, one Survival Skill 4 aspect

**Action:** 

> #### Nature Sense
>
>**Preparation:** Investigate the setting you are in for 1 minute. 
>
>**Check:** 10 + GM discretion (generally harder the more unfamiliar the environment) 
>
>**Duration:** Instant
>
>**Target:** Terrain that you can currently see
>
> **Effect:** Gain information on general environmental hazards to watch out for, with increasing accuracy for however much you pass the check by.

### Survival Skill 6 - Material Harvest

**Category:** Ability Skill

**Point Cost:** 6

**Prerequisite:** 6 levels in Survival, one Survival Skill 5 aspect, 1 level in cartography

**Effect:** If you know a material is in an area, you may attempt to harvest it. This takes 10 minutes of work. When the work is completed, make a survival check with a difficulty of its Harvest Difficulty. You receive a number of kilograms equal to its Kilograms per check. (This amount cannot surpass the amount of the material that's actually there.)

*Note: The weight is *only* listing the amount of material itself that was harvested. It is not purified, and so the actual weight may be much more, especially if its harvested in the form of an ore.*

### Swimmer

**Category:** Physical

**Point Cost:** 5

**Prerequisite:** Be part of a species that travels on ground that can't normally swim. 

**Effect:** You have a swim speed 1/5th of your ground speed. This fatigues body once per minute of use.

### Swimmer - Expert

**Category:** Physical

**Point Cost:** 10

**Prerequisite:** Swimmer Aspect

**Effect:** Your swim speed is 1/3 of your ground speed, and it fatigues your body once per hour of use instead.

### Tactics Skill 3 - Sport Tactics [Sport]

**Category:** Ability Skill

**Point Cost:** 3

**Prerequisite:** 3 levels in Tactics, one Tactics Skill 2 aspect

**Effect:** You are skilled at working within the rules of a specific game. By taking this aspect, all your tactics rolls within the chosen game are increased by 5, but outside of the game decreased by 5.

**Special:** You may take this aspect more than once. Each time you take it, choose a different sport. After you take it a second time, the decrease of 5 no longer applies to tactics uses.

### Tactics Skill 4 - Tactical Flanking

**Category:** Ability Skill

**Point Cost:** 4

**Prerequisite:** 4 levels in Tactics, one Tactics Skill 3 aspect, At least one level in Arms or Martial Arts abilities.

**Action:** 

> #### Tactical Flanking
>
>**Preparation:** Be in a position to attack target with an ally able to attack target from a different side (anywhere between an opposite position to a perpendicular position)
>
>**Check:** None
>
>**Duration:** As long as positions remain
>
>**Target:** 1 opponent
>
> **Effect:** Opponent has to spend an extra action to target an opponent not targeted by its last use.

### Tactics Skill 5 - Tactical Adjustment

**Category:** Ability Skill

**Point Cost:** 5

**Prerequisite:** 5 levels in Tactics, one Tactics Skill 4 aspect

**Action:** 

> #### Tactical Adjustment
>
>**Preparation:** 1 action, fatigue tactics by 1
>
>**Check:** Current amount of modifiers affecting target (ignoring whether those modifiers are pluses or minuses)
>
>**Duration:** A number of turns equal to amount by which you pass the check.
>
>**Target:**1 character you can communicate with.
>
>**Effect:** Target gets +1 modifier to all rolls of one ability of your choice. 

### Tactics Skill 6 - Tactical Planning

**Category:** Ability Skill

**Point Cost:** 6

**Prerequisite:** 6 levels in Tactics, one Tactics Skill 5 aspect, 1 level in inspire.

**Action:** 

> #### Tactical Planning
>
>**Preparation:** 5 minutes
>
>**Check:** 20, -1 for each additional minute spent preparing.
>
>**Duration:** 1 day
>
>**Target:** Group of allies (may include self) about to pursue a goal who are not already under the effects of tactical planning.
>
> **Effect:** While working towards the goal, those affected, if they are not affected by a plan already, they may receive a +1 to all rolls for every 10 by which the check is passed.
>
>**Failure:** Allies receive a -1 to all rolls for every 5 by which check is failed.

### Tactics Skill 10 - Unit Flanking

**Category:** Ability Skill

**Point Cost:** 10

**Prerequisite:** 10 levels in Tactics, one Tactics Skill 9 aspect, 1 level in Cartography

**Effect:** Tactical Flanking's target may be an entire unit of creatures affected by an opponent's use of tactics. 

### Tactics Skill 10 - Master Plan

Some people have a fire that burns within them, a great plan that their life revolves around.

**Category:** Ability Skill

**Point Cost:** 10

**Prerequisite:** 10 levels in Tactics, one Tactics Skill 9 aspect, 1 level in Philosophy, GM permission

**Effect:**

When this aspect is taken, the character's master plan is must be stated. Any Plan check involving the goals or methods of their master plan has double the result, but any plan not involving their master plan while the master plan is in effect is reduced to half (round down). A master plan must be a long term goal, reasonably being one that would take more than a year to succeed in. When you finish a master plan, only then may you take another master plan (at no extra cost). 

### Tactics Skill 15 - Unit Adjustment

**Category:** Ability Skill

**Point Cost:** 15

**Prerequisite:** 15 levels in Tactics, one Tactics Skill 14 aspect, 2 levels in Inspire

**Effect:** You may use Tactical Adjustment on a whole target group of allies who are all affected by Tactical planning, if you can get a message spread throughout the unit, instead of just a single character.



![Weapon Fighting](./img/Weapon-Fighting.jpg)

### Tactics Skill 15 - Counterplan

Sometimes, the best plan is making sure your enemy's plan fails.

**Category:** Ability Skill

**Point Cost:** 15

**Prerequisite:** 15 levels in Tactics, one Tactics Skill 14 aspect, Tactics Skill 6 - Tactical Planning

**Action:**

### Counterplan
>
>**Preparation:** 10 minutes prep with a group which which you have had tactical planning that is not currently protected by a use of counterplan.
>
>**Check:** 25
>
>**Duration:** 1 day
>
>**Target:** Any known opponents to a goal you or your allies are about to pursue.
>
> **Effect:** While working against the chosen goal, those affected, if they are not affected by a counterplan already, their opponents will receive a -1 to all rolls for every 10 by which the check is passed when any ability of their targets any in a group you have done tactical planning with.
>
>**Failure:** Allies receive a -1 to all rolls for every 5 by which check is failed.

### Tactics Skill 20 - Counter-Counterplan

Sometimes, being flexible is the best preparation.

**Category:** Ability Skill

**Point Cost:** 20

**Prerequisite:** 20 levels in Tactics, one Tactics Skill 19 aspect, Tactics Skill 6 - Tactical Planning

**Action:** 

> Counter-Counterplan
>
>**Preparation:** 10 minutes, may not have any other counter-counterplans in effect.
>
>**Check:** Special
>
>**Duration:** 1 day
>
>**Target:** Group of allies (may include self) about to pursue a  goal with known opponents.

**Effect:** While working towards the goal, those affected, will be unaffected by any enemy's Counterplan if the enemy's counterplan check result was less than the check made for counter-counterplan.
>
>**Failure:** Allies receive a -1 to all rolls for every 5 by which check is failed.

### Teach Skill [x] - Basic Instruction in [Ability]

**Category:** Ability Skill

**Point Cost:** [x]

**Prerequisite:** [x] levels in Teach, one Teach Skill [x-1]  aspect, at least [x] levels in [Ability] 

Note: ([x] must be at least level 5)

**Effect:** If you spend 25 hours instructing a target (these hours do not have to be all at once), you may make a teach + [ability] check to teach a target student. 



If you pass a difficulty of 3 times the level  in [ability] of the target, they may gain one character point to be used in that ability or one of its lower abilities (they must still meet prerequisites to spend the point on the ability).

**Special:** You may take this aspect multiple times, however, you may never have more than one of a given [x] value. For example, if you have Teach Skill 5 - Basic Instruction in Travel, you can't take a Teach Skill 5 - Basic Instruction in Tactics aspect, but you could take a Teach Skill 6 - Basic Instruction in Tactics instead. 

### Teach Skill 12 - General Education

**Category:** Ability Skill

**Point Cost:** 12

**Prerequisite:** 12 levels in Teach, one Teach Skill 11  aspect

**Effect:** If you spend 25 hours instructing a target (these hours do not have to be all at once), you may make a teach check to teach target student in any one ability of your choice.



If you pass a difficulty of 3 times the level  in the ability of the target, they may gain one character point to be used in that ability or one of its lower abilities (they must still meet prerequisites to spend the point on the ability).

### Teach Skill 12 - Coach

**Category:** Ability Skill

**Point Cost:** 12

**Prerequisite:** 12 levels in Teach, one Teach Skill 11 aspect

**Effect:** You may teach multiple characters simultaneously in the same target ability in one of Body's lower abilities. For every additional target, check difficulty is increased by +2 (check result compares to each person being taught separately, however.)

### Teach Skill 12 - Teacher

**Category:** Ability Skill

**Point Cost:** 12

**Prerequisite:** 12 levels in Teach, one Teach Skill 11 aspect

**Effect:** You may teach multiple characters simultaneously in the same target ability in one of Mind's lower abilities. For every additional target, check difficulty is increased by +2 (check result compares to each person being taught separately, however.)

### Throw Skill 1 - Toss

**Category:** Ability Skill

**Point Cost:** 1

**Prerequisite:** 1 levels in Throw

**Action:** 

> #### Toss
>
>**Preparation:** 1 turn, fatigue Throw by 1
>
>**Check:** Twice distance you want to throw + 1 for every kg of weight of the item you are throwing.
>
>**Duration:** Instant
>
>**Target:** An area you conceivably toss something to.
>
> **Effect:** Chosen item you have lands in space you desire.
>
>**Failure:** For every 1 by which you miss, the target lands 1 meter off target in a random direction.

Note: If it is a weapon thrown, the weapon does it's effects on attack. You need appropriate arms weapon attack aspects to receive normal bonuses to roll or to hit a living target.

### Throw Skill 15 - Strike Throw

**Category:** Ability Skill

**Point Cost:** 1

**Prerequisite:** 15 levels in Throw, one Throw Skill 14 aspect, Martial Arts Skill 10 aspect - Strike Grapple

**Action:**

> #### Strike Throw
>
>**Preparation:** 1 action, fatigue Martial Arts and Throw by 1
>
>**Check:** Contest against target's Finesse
>
>**Duration:** Instant
>
>**Target:** A character within your reach
>
> **Effect:** Move target a number of meters equal to a third of your Might (round down). Then contest your Finesse vs their Finesse. If they lose, they fall prone (requiring 1 action to get back up.)

### Too Much to Live for

You know, you really can't let things stop you. No matter what. This emergency now? It's just a roadblock to the things to come later. Nothing will stop you. Nothing. Because you have a goal to achieve, and you can't let this get in the way of it.

**Category:** Background & Destiny

**Point Cost:** 10

**Prerequisite:** GM Permission

**Effect:** Any time the result of failing a roll would kill your character, you may spend one destiny point and reroll that dice.

### Tool Focus [*]

*Tool Name

**Category:** Tool

**Point Cost:** 5

**Prerequisite:** 5 Levels in an ability used by a tool or weapon possessed that has the focus' name. (Example: Tool Focus [*] of the matching tool, Weapon Mastery [Spear] to use Tool Focus [Grandfather's Tassled Spear], etc.)

**Effect:** Characters may be skilled in a specialized skill, but they may have one specific tool they prefer over any other (such as a musician that favors a specific electric guitar, or a warrior that prefers their heirloom tasseled spear over any other kind of weapon). When using a possession that has the preferred tool's name, they get a +3 bonus. This Aspect may be taken multiple times, each time applying to a new tool. 



### Toughness Skill 0 – Toughness Centering

**Category:** Instant

**Point Cost:** 0

**Action:**
>
>**Preparation:** Fatigue Toughness 1 (cannot be altered by other aspects) 
>
>**Duration:** Instant
>
>**Target:** Self
>
> **Effect:** Reduce body damage taken by your toughness.



### Track Skill 6 - Find Trail

**Category:** Ability Skill

**Point Cost:** 6

**Prerequisite:** 6 levels in Track, one Track Skill 5 aspect

**Action:**

> #### Find Trail
>
>**Preparation:** Investigate some known information about the target (past tracks, biological information, etc.) for 5 minutes.
>
>**Check:** 
>
> Difficulty = ((target  group's lowest sneak score) + (Trail Start Difficulty) + (how many hours ago group was last there)) / number of characters in target group
>
>
>**Duration:** Instant
>
>**Target:** Any 1 creature or group
>
>**Effect:** You are now on the trail of the target, and may follow the path the target took. The GM may require occasional tracking checks as terrain changes or obstacles arise.



Use the following table as a guideline for trail start difficulties:

| | |
|:---|:---|
| 1 |Follow footsteps of a well-known target in deep fresh snow on a clear day. |
|5 |Follow the trail of a small child passing through mud.|
|10|Follow the tracks of a deer in the woods.|
|15|Follow a wary ape through a rainforest.|
|20|Track a mouse through a mall.|
|30|Follow the trail of an escaped convict in a crowded concrete city without talking to anyone.|
|40|Follow exact trail of a single migrating butterfly of an unknown species.|
|50|Track a high-flying bird through a desert in the middle of a sandstorm.|

### Track Skill 8 - Track Analysis

**Category:** Ability Skill

**Point Cost:** 8

**Prerequisite:** 8 levels in Track, one Track Skill 7 aspect, Track Skill 6 - Find Trail

**Effect:** By analyzing the tracks, if you pass a Find Trail check by more than 5, you can also know basic information about what the target did, such as increase/decrease load, the steps taken in a fight, etc. Passing by 10 or more will let you know more details (such as who won or lost the fight, speed and skill of fighters involved, rough weight of load change, etc.)

### Train Skill 1 - Instant Response

**Category:** Ability Skill

**Point Cost:** 1

**Prerequisite:** 1 level in Train

**Effect:** If you have one character with  90% loyalty to you or greater or a tamed creature, you may train them to react to a stimulus instinctively with a specific action. Each attempt will be a contest between your train and their sanity. (They may choose to auto-fail this.) Each time you do this to a character, they lose 2% loyalty.



Whenever they are presented with a stimulus, they will automatically make that action. Should a character not desire to make that action, they need to make a sanity check versus the train check you made when you ingrained the reaction into them. If they fail or choose not fight the command, the character/creature will perform the action. If it is not their turn, the character will automatically fatigue being by 1, move ahead in turn order to the current position if possible, and perform the action.

### Train Skill 16 - Group Training

**Category:** Ability Skill

**Point Cost:** 16

**Prerequisite:** 16 levels in Train, one Train Skill 15 aspect, Train Skill 1 - Instant Response

**Effect:** You can train a group to of characters/creatures to all respond on the same command as per the Instant Response rules. You may also train the group to all respond to the same stimulus, and act in a specific order (instead of all trying to act at once) with their own unique actions.

### Travel Skill 4 - Shortcut

**Category:** Ability Skill

**Point Cost:** 4

**Prerequisite:** 4 levels in Travel, one Travel Skill 3 aspect

**Effect:** On a long journey, you may make a Travel check that has a difficulty of 10 on simple terrain (may be more difficult in more confusing terrain.) If you pass the check, reduce travel time by 10%. For every additional 10 by which you pass the check, you may subtract 10% off of the new travel time (For example, if you pass a check on a 100 day journey by 11, you reduce it to 90 days for the check, and 10% off of the 90 for a final result of 81 days of journey.)

Failing the check adds on 10% travel time to the journey, with an additional 10% added for each 5 by which the check failed.

### Treatment Skill 2 - First Aide

Sometimes, getting to someone quickly is more important than doing it well.

**Category:** Ability Skill

**Point Cost:** 2

**Prerequisite:**  2 levels in Treatment, and one Treatment skill 1 aspect.

**Effect:** If a character would die and you are near them, you may fatigue all of treatment's available fatigue (minimum 2) and prevent their death with a treatment check of 1/8th the damage they've taken.

### Treatment Skill 10 - Bedside Care

If you spend a day caring for someone who is damaged, you can attend to their needs properly so their body can focus on healing.

**Category:** Ability Skill

**Point Cost:** 10

**Prerequisite:**  10 levels in Treatment, and one Treatment skill 9 aspect.

**Effect:** When providing treatment, make a treatment check of 10 + 1/4th of damage they've taken. If you pass the check, double the natural healing abilities of the patient. The maximum number of patients you can attend to is equal to half your treatment level. If you pass the check by more than 10, triple the patient's healing rate.



![Reaction](./img/Reaction.jpg)

### Vigilance Skill 4 - Alertness

**Category:** Ability Skill

**Point Cost:** 4

**Prerequisite:** 4 levels in Vigilance, one Vigilance Skill 3 aspect

**Action:** 

> #### Alertness
>
>**Preparation:** Instant, Fatigue Vigilance by 1
>
>**Check:** Double the total Fatigue of target ability.
>
>**Duration:** 1 turn
>
>**Target:** Any Ability that has maxed its fatigue.
>
> **Effect:** You may fatigue the ability for 1 past it's maximum. However, after spending it, instead of adding one fatigue, you may 'extreme fatigue' the ability. This allows one additional use, but all fatigue on the ability takes twice as much resources and/or time to heal. (You can only extreme fatigue an ability once.)

### Weapon Mastery [Category] Skill 1 - Weapon Focus

**Category:** Ability Skill

**Point Cost:** 1

**Prerequisite:** 1 level in Weapon Mastery [category]

**Effect:** If you are using a weapon of the chosen category, (the aspect's category is generally a weapon sub-category), then you may use Weapon Mastery [category] instead of Arms for the purpose of any Arms rolls. In addition, you make take arms aspects and apply them to your weapon mastery instead (however, their category must be taken as this subcategory instead of a normal weapon category.)

### Weapon Mastery [Category] Skill 3 - Block

**Category:** Ability Skill

**Point Cost:** 3

**Prerequisite:** 3 levels in Weapon Mastery [category], one Weapon Mastery [category] Skill 2 aspect

**Action:** 

> #### Block
>
>**Preparation:** Instant Prep, source of damage must be able to be sensed in some way.
>
>**Check:** Same check as target.
>
>**Duration:** Instant
>
>**Target:** Damage contest targeting you
>
> **Effect:** Your weapon, if applicable, takes 1/5th the damage and you take none.

### Weapon Mastery [Category] Skill 5 - Disarm

**Category:** Ability Skill

**Point Cost:** 5

**Prerequisite:** 5 levels in Weapon Mastery [Category], one Weapon Mastery [Category] Skill 4 aspect, Weapon Mastery [Category] Skill 3 - Block

**Action:**

> #### Disarm
>
>**Preparation:** Fatigue Weapon Mastery by 1
>
>**Check:** Contest Opposing Character's Arms ability with their weapon.
>
>**Duration:** Instant
>
>**Target:** Target Character's weapon after you completed a successful block action against the target.
>
> **Effect:** They are disarmed, their weapon falls to the ground by them.

### Weapon Mastery [Category] Skill 6 - Sunder

**Category:** Ability Skill

**Point Cost:** 6

**Prerequisite:** 6 levels in Weapon Mastery [Category], one Weapon Mastery  [Category] Skill 5 aspect

**Action:**

> #### Sunder
>
>**Preparation:** 1 action, Fatigue Weapon Mastery [Category] by 1
>
>**Check:** Contest target player's Arms ability with their current weapon.
>
>**Duration:** Instant
>
>**Target:** Weapon of a character you would have just damaged from an attack.
>
> **Effect:** Deal the damage to their weapon instead.

### Weapon Mastery [Melee Category] Skill 6 - Parry & Riposte

**Category:** Ability Skill

**Point Cost:** 6

**Prerequisite:** 6 levels in Weapon Mastery [Melee Category], Weapon Mastery [Melee Category] Skill 5 - Block

**Action:**

> #### Parry & Riposte
>
>**Preparation:** Fatigue Weapon Mastery [Melee Category] by 1
>
>**Check:** Contest Opposing Character's Arms ability with their current weapon.
>
>**Duration:** Instant
>
>**Target:** Target Character's melee weapon which you just succeeded in a block attempt against and have not taken another action against.
>
> **Effect:** Deal Opposing character damage they would have dealt you.

### Weapon Mastery [Category] Skill 10 - Crippling Attack

**Category:** Ability Skill

**Point Cost:** 10

**Prerequisite:** 10 levels in Weapon Mastery [Category], one Weapon Mastery [Category] Skill 9 aspect

**Action:**

> #### Crippling Attack
>
>**Preparation:** 1 action, Fatigue Weapon Offense by 1
>
>**Check:** Contest target player's arms of any weapon they're currently using + their Toughness
>
>**Duration:** Instant
>
>**Target:** A character in range of your weapon that is vulnerable to Vital Strike.
>
> **Effect:** Deal your weapons effect to the target. If this deals more than 10% damage, your target becomes unable to meet a survival requirement of your choice until healed, or gains a handicapped aspect of your choice until healed.

### Wellness Skill 3 - Ignore Ailment

**Category:** Ability Skill

**Point Cost:** 3

**Prerequisite:** 3 levels in Wellness, one Wellness Skill 2 aspect

**Action:** 

> #### Ignore Ailment
>
>**Preparation:** Fatigue Wellness by 3
>
>**Check:** Combined Numerical effects (fatigue, damage, etc.) from disease or poison.
>
>**Duration:** 10 minutes
>
>**Target:** Self
>
> **Effect:** You postpone any effects of the disease or poison for the duration. (Damage, fatigue, etc. still accumulate, they just don't get applied till after the duration.) If accumulated fatigue or damage would render you unconscious or dead, the effect immediately ends.

*Roleplaying Tip: Those who have this aspect often pride themselves on 'toughing through' physical problems. They'll often downplay anything they're going through as a result, and when forced to lie in bed, will often get up and start doing things when no one notices.*

### Wellness Skill 6 - Fight Disease

**Category:** Ability Skill

**Point Cost:** 6

**Prerequisite:** 6 levels in Wellness, one Wellness Skill 5 aspect

**Effect:** Whenever you get sick, the duration of the sickness is reduced by half.

### Wellness Skill [x] - Poison Immunity [Poison]

**Category:** Ability Skill

**Point Cost:** [x]

**Prerequisite:** [x] levels in Wellness, one Wellness Skill [x-1] aspect, no Wellness Skill [x] - Poison Immunity aspects of the same level.

**Effect:** You have trained or altered your body in some way to make it immune to a specific poison. Whenever you are poisoned with it, ignore its effects.

**Special:** You may take this aspect multiple times for different poisons. It must be a different level each time. X must be a minimum of 10.

### Wisdom Skill 5 - Resolution

**Category:** Ability Skill

**Point Cost:** 5

**Prerequisite:** 5 levels in Wisdom, one Wisdom Skill 4 aspect

**Action:** 

> #### Resolution
>
>**Preparation:** Fatigue Wisdom by 1
>
>**Check:** 10 times amount of fatigue that would be dealt to mind via target.
>
>**Duration:** Instant
>
>**Target:** A source of fatigue to your mind other than yourself

**Effect:** Reduce the fatigue you take by 1. For every additional 5 by which you surpass the check, reduce the fatigue by an additional one.

### Wisdom Skill 11 - Expected

**Category:** Ability Skill

**Point Cost:** 11

**Prerequisite:** 11 levels in Wisdom, one Wisdom Skill 10 aspect

**Action:** 

> #### Expected
>
>**Preparation:** Instant Prep, Fatigue Wisdom by 1
>
>**Check:** Contest an ability being used to surprise you.
>
>**Duration:** Instant
>
>**Target:** Self
>
> **Effect:** You are not surprised or caught off guard in a situation you otherwise would be surprised or caught off guard in. You may ignore a surprise effect.

### Wisdom Skill 18 - Reorient

**Category:** Ability Skill

**Point Cost:** 18

**Prerequisite:** 18 levels in Wisdom, one Wisdom Skill 17 aspect

**Action:** 

> #### Reorient
>
>**Preparation:** Instant Prep, Fatigue Wisdom by 1
>
>**Check:** Damage Dealt to Mind
>
>**Duration:** Instant
>
>**Target:** Damage dealt to Mind by a source other than yourself.

**Effect:** Damage taken is reduced by ½ amount by which roll passed the check, round down. (As such, no damage reduced if check and roll are same, even though it's considered 'passed')

# <a name="posessions"></a>Possessions
![Equipment](./img/Equipment.jpg)

People like to have stuff, generally. Not everyone, but enough people where it warrants attention. Further, a lot in addition to having things, like to use those things. That's what this section is about. Stuff your character can have, and what that stuff does.
## The Basics
A typical possession listed in this section will generally be in a table listing a few standard traits. The following is a simple example: 

|Possession Name | Categories | Traits (aka "What in the world this thing actually does, what stats it has, etc.")| Tech Level | Creation Difficulty | Primary Material | Day's Wage Cost |
|---|---|-------|---|---|---|---|
|Big shiny thing| Fluffles, Sparkles, Magic |It does something amazing. |3.14 |1,000,000 |1 solid ray of sunshine | 0 DW

This is a simple standard. Some tables will add extra columns for information most items have. The following are what each trait does:

**Possession Name:** This is the name of the thing. No big deal here, we use names all the time. 

**Categories:** Often, categories of a possession will be referenced by the rules of the game. Whenever rules act different based on categories (and subcategories), the rules will reference them. Such as if the object is magical, if it counts as a ranged weapon, etc. (If you're on a table specifically of weapons, the 'weapon' part may just be assumed, and it may just list 'ranged'. Same with Armor, Homes, etc.)

**Traits:** These are the nitty-gritty rules-specific stuff of this particular item. Usually, most of what you should need should be here.  Pay extra attention to any traits in bold. These are common effects shared by many possessions in the table, and those effects will be explained of the possession's secion.

**Year / Tech Level:** This is the level of technological development of a society necessary to create the possession (years are given to make it easier for games set in our world). In short, you can't buy laser rifles in the stone age.  (TMI on tech level at the end of the possessions chapter.)

**Creation Difficulty:** This is the crafting difficulty (according to the craft ability), the difficulty that has to be passed in order to make the item if you have all of the primary material. Other things may also use the creation difficulty for various purposes. 

**Primary Material:** This is what the majority of the object is made of. We make things a little simpler here by ignoring a lot of the little bits. However, if there is something ridiculously expensive or important to the construction process, it might be listed after the primary material (such as a laser rifle including an expensive focusing crystal in addition to materials of which the body is made.) In crafting and other methods, it may be possible to change the primary material of a possession (changing its stats accordingly), but secondary 'special' materials can't be switched and have the possession still work.

**DW C/P:** Short for "Day's Wage Cost, Current/Period", this is the suggested cost of the possession, based on the value of an average day's wages. The first value is for it in modern times, the second is for the period in which it was created. ~*(And yes, I'm using the value of how many days wages something costs. That way we can ignore inflation, monetary trends, etc. And keep things in values that make sense. And seriously, to you people who want it in gold for medieval eras... do you know how rarely people saw gold pieces in medieval days? You and your games where 100 gold gets you a hat... you do realize just a handful of gold could practically set you up for life in that time? 100 gold for a hat... is that hat really worth retirement? Are you nuts!?! Besides, This also lets the cost be easily switched from system to system, because although not everyone uses gold, almost every society has an average day's wage.*)~

If you want to change the cost for different eras, simply multipy the cost by the possession's tech level, and then divide by the new tech level. (TMI on Day's Wage Cost at the end of the possessions chapter.)As a note, this value assumes that all workers in the process are being paid the average wage for the time period, for high quality versions of the items. However, more often than not, this was not the case as often kings, tyrants, and more really threw off the balance (most people making way less than the average), and stuff was often made cheaper for reasons of speed. For most ancient historical time periods, these values should be assumed to be the maximum that a player will end up paying (unless there's something really weird going on in the story). Also, keep in mind these are the prices when the item was first created, and is considered state-of-the-art.

|Package|Contents|Day's Wage Cost |
|---|---|---|
|Really Big shiny things |5 Big Shiny things, 1 liter of water, half a hippo (still living, location of other half of hippo unknown) |money still can't buy this|

Another thing you'll see throughout the possession section is Packages. This make buying stuff for a new character quick and simple. Packages will include a series of items you might buy individually, but has the total cost listed (sometimes with a small discount of 5%). These are usually items that make part of a set, like a backpack of hiking gear, a policeman's uniform and standard gear, etc. 

## Weapons

So, the person new to roleplaying games may be asking, "Why are you listing weapons first? Doesn't it come later, alphabetically speaking?" The answer, my dear random inquisitive person that thinks about things in intriguing detail is that it's a roleplaying tradition. Some of the people who play roleplaying games want to play out stories of powerful characters who slice through thousands of enemies soldiers whom are just trying to feed their families in order to destroy an evil overlord or evil artifact in order to save the small village of twenty people they so champion. (I, as the author, must strongly resist the urge to psychoanalyze or mathematically analyze this desire, as an fyi.) 

And so, for a number who play these games, the most important thing to their character is the weapon they wield (...must...resist...urge...to...psychoanalyze....), and so they expect the weapon section of the book to be the most easily accessible.... as in the first section when they look at the possessions part of the book. Despite the fact its not in alphabetical order. So, without further adieu, the various things with which you may bash, smash, hit, slam, shoot, and otherwise cause a living breathing thing to stop that living and breathing bullsh... er.... here's your weapons. (Ya insane blood-crazed loonies.)

In the weapons, each entry has a few special key points beyond normal possessions.

**Reach:** This is how far the melee weapon can go beyond the character's reach (most characters' natural reach is 1 meter).

**Drop Range:** Every time a ranged weapon goes this distance, it's damage and Vital Strike chance goes down by 1%. If it's base damage (not including any bonuses from the player) reach 0, it goes no further.

**Recovery Rate:** For reusable ammo, this is how many out of a set you are usually going to find in one piece.

**Weight:** This is the how much the weapon weighs. Usually used for determining if you can lift a weapon or how much inventory space it takes.

**Damage Effect:** Any time you'd do the weapons damage (usually winning an arms contest in combat), this is how much damage you do.

**Vital Strike Chance:** There is a percentage chance each time you hit an excellent effect (usually max roll, such as two sixes on 2D6 dice) that you deal the damage directly to Being instead of body. If you do so, the character immediately goes unconscious, and has body reduced to 0%. A Vital Strike on someone who's unconscious, kills them outright.

**Follow-through:** If you vital strike an enemy or otherwise reduce them past 0% body damage, you may strike another character in the line of attack (adjacent enemies for slashing weapons, enemies behind the enemy you're attacking for stabbing weapons.) If you reduce that character below 0% body or vital strike them, you may continue on to the next up to the follow-through value. This is all considered one action. The most common values are 0, 1, and infinite (usually for slashing weapons where a single swing can hit multiple opponents, meaning the limit isn't based on the weapon attack direction, but the number of enemies in range and the total damage delt.)

**Damage Reduction:** This is how much damage the weapon can take in one strike before it's damaged. If you can use the weapon defensively, this is how much damage it can block. If the weapon has damage directly dealt to it, it breaks if it goes past this amount.

Note: Many weapons can be used in more than one way. (Such being used as either a stabbing or a slashing, or as a fast or a heavy, but not both simultaneously.) These will have the differences based on the categories separated out as if they were different weapons. Unless stated otherwise, you may simply choose which version you use during a given action.

### Melee Weapons
**Swords, Knives, Maces, and more!**

|Name | Categories (Sub-categories) | Traits | Reach | Damage Effect | Vital Strike Chance | Follow-through | Damage Reduction | Tech Level | Creation Difficulty | Primary Material | Weight | Day's Wage Cost |
|---|---|---|---|---|---|---|---|---|---|---|---|---|
Club|Heavy, Melee (Bludgeon, Stick)|Two-Handed, Blunt, Makeshift|1 M| +Tou +Mig||0|7.00%|-|1|Wood|2 KG|0
|Staff|Heavy, Melee (Bludgeon, Stick)|Styled Weapon, One-and-a-half handed, Blunt, Makeshift,  +11 to Ability rolls using Staff|2 M|20% +Tou +Mig|2.00%|0|7.00%|-|1|Wood|2.3 KG|0
||Fast, Melee (Bludgeon, Stick)|Styled Weapon, One-and-a-half handed, Double-edged, Blunt, Makeshift,  May strike two targets on opposite sides of you at once. +6 to  Ability rolls using Staff|1 M|20% +Mig| 5% +Spe|0||||||
||Heavy, Melee (Bludgeon, Stick)|Styled Weapon, One-and-a-half handed, Stabbing, Blunt, Makeshift |2 M|20% +Tou +Mig|45.00%|0||||||
||Fast, Melee (Bludgeon, Stick)|Styled Weapon, One-and-a-half handed, Stabbing, Double-edged, Blunt, Makeshift, May strike two targets on opposite sides of you at once.|1 M|20% +Mig|45% +Spe|0||||||
|Gladius|Fast, Melee (Sword, Slashing)|Double-edged, Slashing|.5 M|10% +Mig|27% +Spe|∞|9.00%|TL 9/ 100 BCE|5|Bronze|1 KG|2.5/ 5.4
||Fast, Melee (Sword, Stabbing)|Stabbing||11% +Mig|45% +Spe|1||||||
|Leuterit Viking Sword|Heavy, Melee (Sword, Slashing)|Double-edged, Hilt 1, Slashing|1 M|10% +Tou +Mig|18.00%|∞|16.00%|TL 10/ 750 CE|6|Steel|1.1 KG|3.1/ 6
||Heavy, Melee (Sword, Stabbing)|Hilt 1, Stabbing||11% +Tou +Mig|45.00%|1||||||
|Seax & Tanto|Fast, Melee (Knife, Slashing)|Slashing|0 M|4% +Mig|25% +Spe|∞|16.00%|TL 10/ 750 CE|3|Steel|.5 KG|1.4/ 2.8
||Fast, Melee (Knife, Stabbing)|Stabbing||5% +Mig|45% +Spe|0||||||
|Viking Hand Axe|Fast, Melee (Axe, Slashing)|Slashing|0 M|7% +Mig|45% +Spe|∞|16.00%|TL 10/ 750 CE|1|Steel|.8 KG|.8/ 1.7
|Mayan J ade Hand Club|Fast, Melee (Bludgeon)|Styled Weapon, Excellent Effect: instead of normal vital strike, instead renders target unconscious. May strike two targets on opposite sides of you at once.|0 M| 5%+Mig|18% +Spe|0|2.00%|TL 10.2/ 900 CE|1|Jade|1 KG|.6/ 1.1
|Mayan-Aztec Macuahuitl|Heavy, Melee (Sword, Slashing)|Slashing, Two-Handed, Double-edged, Heavy Headed 6|1 M|20% +Tou +Mig|45.00%|∞|7.00%|TL 10.6/ 1200 CE|4|Wood|4 KG|3/ 5.4
|Claymore|Heavy, Melee (Sword, Slashing)|Hilt 2, Double-edged, Two-Handed, Slashing , Shock Effect, Heavy Headed 3, +1 to ability rolls|1.5 M|26% +Tou +Mig|29.00%|∞|16.00%|TL 10.6/ 1200 CE|7|Steel|3 KG|4.4/ 8.1
||Heavy, Melee (Sword, Stabbing)|Hilt 2, Two-Handed, Stabbing, Heavy Headed 3, +1 to ability rolls using Claymore||30%+Tou +Mig|45.00%|2||||||
|Belt Dagger|Fast, Melee (Knife, Slashing)|Double Edge, Hilt 2, Slashing|0 M|3% +Mig|17% +Spe|∞|16.00%|TL 10.8/ 1250 CE|2|Steel|.3 KG|1/ 1.8
||Fast, Melee (Knife, Stabbing)|Hilt 2, Stabbing||4% +Mig|45% +Spe|0||||||
|Falchion|Heavy, Melee (Sword, Slashing)|Hilt 2, Slashing, Heavy Headed 3|1 M|14% +Tou +Mig|24.00%|∞|16.00%|TL 10.8/ 1250 CE|3|Steel|1.4 KG|2.1/ 3.9
||Heavy, Melee (Sword, Stabbing)|Hilt 2, Stabbing, Heavy Headed 3||12% +Tou +Mig|45.00%|1||||||
|Katana|Heavy, Melee (Sword, Slashing)|Hilt 1, One-and-a-half handed, Slashing, Heavy Headed 2|1 M|11% +Tou +Mig|20.00%|∞|16.00%|TL 11.1/ 1392 CE|1|Steel|1.1 KG|1/1.8
||Heavy, Melee (Sword, Stabbing)|Hilt 1, One-and-a-half handed, Stabbing, Heavy Headed 2||13% +Tou +Mig|45.00%|1||||||
|Odachi|Heavy, Melee (Sword, Slashing)|Hilt 1, Two-handed, Slashing, Shock Effect, Heavy Headed 4, +1 to ability rolls using Odachi|1.5 M|26% +Tou +Mig|27.00%|∞|16.00%|TL 11.1/ 1400 CE|17|Steel|2.8 KG|10/ 17.8
||Heavy, Melee (Sword, Stabbing)|Hilt 1, Two-handed, Stabbing, Heavy Headed 4, +1 to ability rolls using Odachi||31% +Tou +Mig|45.00%|2||||||
|Assassin Spring Blade|Fast, Melee (Knife, Stabbing, Mechanical)|Styled Weapon, Double Edge, Requires 20 minutes maintenance per day of use. May declare as a punching attack, and change to spring-strike after it resolves. Requires 2 actions to prepare spring blades (may be done during maintenance period.)|0 M|39% +Mig|45.00%|0|16.00%|TL 11.3/ 1450 CE|2|Steel|1.7 KG|1.4/ 2.5
||Fast, Melee (Knife, Slashing)|Styled Weapon, Double Edge, Requires 20 minutes maintenance per day of use. ||15% +Mig|45.00%|0||||||
||Fast, Melee (Knife, Stabbing)|Styled Weapon, Requires 20 minutes maintenance per day of use. ||29% +Mig|45.00%|0||||||
|Longsword|Heavy, Melee (Sword, Slashing)|Hilt 2, Double-Edged, One-and-a-half handed, Slashing|1 M|13% +Tou +Mig|19.00%|∞|16.00%|TL 11.3/ 1450 CE|5|Steel|1.5 KG|2.9/ 4.9
||Heavy, Melee (Sword, Stabbing)|Hilt 2, Double-Edged, One-and-a-half handed, Stabbing||15% +Tou +Mig|45.00%|1||||||
|Bastard Sword|Heavy, Melee (Sword, Slashing)|Hilt 2, Double-Edged, One-and-a-half handed, Slashing, Heavy Headed 3|1 M|13% +Tou +Mig|26.00%|∞|16.00%|TL 11.4/ 1510 CE|4|Steel|1.6 KG|2.5/ 4.3
||Heavy, Melee (Sword, Stabbing)|Hilt 2, Double-Edged, One-and-a-half handed, Stabbing, Heavy Headed 3||15% +Tou +Mig|45.00%|1||||||
|Eight-blade Mace|Heavy, Melee (Slashing)|Heavy Headed 5, Slashing|0 M|33% +Tou +Mig|45.00%|0|16.00%|TL 11.6/ 1550 CE|2|Steel|.9 KG|1.1/ 1.9
|Rapier|Fast, Melee (Sword, Slashing)|Hilt 4, Bladecatcher 2, Slashing|1 M|10% +Mig|14% +Spe|∞|16.00%|TL 11.7/ 1590 CE|5|Steel|1.1 KG|3.1/ 5.1
||Fast, Melee (Sword, Stabbing)|Hilt 4, Bladecatcher 2, Stabbing||15% +Mig|45% +Spe|2||||||
|Cutlass|Fast, Melee (Sword, Slashing)|Hilt 3, Slashing|.5 M|9% +Mig|17% +Spe|∞|16.00%|TL 11.7/ 1600 CE|3|Steel|.9 KG|1.9/ 3.1
||Fast, Melee (Sword, Stabbing)|Hilt 3, Stabbing||7% +Mig|45% +Spe|1||||||
|Sgian Dubh (Boot Dagger)|Fast, Melee (Knife, Slashing)|Double-Edged, +2 to Hide Weapon, Slashing|0 M|1% +Mig|8% +Spe|∞|16.00%|TL 11.7/ 1600 CE|1|Steel|.1 KG|.5/ .8
||Fast, Melee (Knife, Stabbing)|Double-Edged, +2 to Hide Weapon, Stabbing||1% +Mig|36% +Spe|0||||||
|Cutlass Bayonet|Fast, Melee (Sword, Slashing)|In addition to being used as a cutlass, may be attached to a rifle weapon, and used as an alternate attack for that weapon (adding +10% Damage and removing slashing attack, but giving a -3 to the rifle's ability contest roll.)  Hilt 3, Slashing|.5 M|10% +Mig|15% +Spe|∞|16.00%|TL 11.8/ 1660 CE|2|Steel|1.1 KG|1.6/ 2.7
||Fast, Melee (Sword, Stabbing)|In addition to being used as a cutlass, may be attached to a rifle weapon, and used as an alternate attack for that weapon (adding +10% Damage and removing slashing attack, but giving a -3 to the rifle's ability contest roll.)  Hilt 3, Stabbing||11.00%|45% +Spe|1||||||
|Scimitar|Fast, Melee (Sword, Slashing)|Hilt 2, Slashing|1 M|9% +Mig|11% +Spe|∞|16.00%|TL 12.1/ 1700 CE|4|Steel|.7 KG|2.8/ 4.4
||Fast, Melee (Sword, Stabbing)|Hilt 2, Stabbing,||4% +Mig|45% +Spe|1||||||
|Cane Mace|Fast, Melee (Bludgeon, Stick)|Styled Weapon, Not treated as a weapon by society. (May be taken into most weapon-restricted areas.) Excellent Effect: instead of normal vital strike, instead renders target unconscious.|0 M|5% +Mig|8% +Spe|0|7.00%|TL 12.6/ 1800 CE|1|Wood|.6 KG|.5/ .7
|Bowie Knife|Fast, Melee (Knife, Slashing)|Hilt 1, Slashing|.5 M|6% +Mig|16% +Spe|∞|16.00%|TL 12.8/ 1830 CE|1|Steel|.6 KG|1.0/ 1.5
||Fast, Melee (Knife, Stabbing)|Hilt 1, Stabbing||5% +Mig|45% +Spe|0||||||
|Calvary Saber|Heavy, Melee (Sword, Slashing)|Hilt 3, Slashing, Heavy Headed 3|1 M|12% +Tou +Mig|14.00%|∞|16.00%|TL 13.1/ 1860 CE|2|Steel|1 KG|1.3/ 1.9
||Heavy, Melee (Sword, Stabbing)|Hilt 3, Stabbing, Heavy Headed 3||9% +Tou +Mig|45.00%|1||||||

### Polearms
One of the nice things (which earns them their own section) about a polearm weapon is that it's length is adjustable by switching out the head of the weapon. Each "weapon" is actually an attachment to a long pole, which can be treated as an enhancement to the staff weapon in the previous table. The standard polearm length is 2 meters (plus a person's normal range, giving a reach of up to 3 meters.) For every meter increase, beyond this, subtract 5 from ability rolls. Cutting down to 1 meter, however, will add 5 to ability rolls, and allow the weapon to be thrown. A single attachment can be added, replacing both the heavy attacks of the staff and removing the special feature of the double-attacks of the staff. However, adding a second attachment decreases the weapon's reach by half and removes remaining original staff attacks.

|Attachment Name | Categories (Sub-categories) | Traits | Damage Effect | Vital Strike Chance | Follow-through | Damage Reduction | Tech Level | Creation Difficulty | Primary Material | Weight | Day's Wage Cost |
|---|---|---|---|---|---|---|---|---|---|---|---|---|
|Butt Cap|Heavy, Melee, (Polearm, Stabbing)|Does not cause the reduction of reach caused by a second attachment. If Butt Cap is the only attachment to the staff, this attack replaces the heavy stabbing attack of the staff. Throwable (if no other kinds of attachments)|5% +Tou +Mig|45.00%|5|8.00%|TL 8.6/ 850 BCE|1|Iron|+.25 KG|.3/ .6
|Trident|Heavy, Melee, (Polearm, Stabbing)|Bladecatcher 8, Stabbing, Throwable|12% +Tou +Mig|45.00%|0|8.00%|TL 9.9/ 700 CE|3|Iron|+1.13 KG|1.8/ 3.4
|Viking Spear Head|Fast, Melee (Spear, Slashing)|Slashing|4% +Mig|24% +Spe|∞|16.00%|TL 10/ 750 CE|1|Steel|+.5 KG|.4/ .8
||Fast, Melee (Spear, Stabbing)|Stabbing, Throwable|4% +Mig|45% +Spe|0||||||
|Battle Axe|Heavy, Melee (Axe, Slashing)|Slashing|6% +Tou +Mig|45.00%|∞|16.00%|TL 10.5/ 1100 CE|2|Steel|+1 KG|1.3/ 2.4
||Heavy, Melee (Axe, Slashing)|Slashing, For character being attacked, treat as a stabbing attack. Follow-through cannot exceed 1.|8% +Tou +Mig|45.00%|1||||||
![Melee Weapons](./img/Melee Weapons.jpg)

## Ranged Weapons - Bows & Arrows
 **Bows**
 
|Name|Categories (Sub-categories)|Traits|Reload Time|Drop Range|Damage Effect|Vital Strike Chance|Follow-through|Damage Reduction|Tech Level/ Year|Creation Difficulty|Primary Material|Weight|DW C/P
|---|---|---|---|---|---|---|---|---|---|---|---|---|---|
|Shortbow|Fast, Ranged (Bow)| Prerequisite: -2 Might or more otherwise automatic Fatal failure.|1 Action|50 M|1.00%|45% +Spe +Fin|0|7.00%|TL 3.1/ 80,000 BCE|7|Wood|.5 KG|1.3/ 8.1
|Yumi (Japanese Longbow)|Fast, Ranged (Bow)|Prerequisite: 0 Might or more otherwise automatic Fatal failure.|1 Action|150 M|1.00%|45% +Spe +Fin|0|7.00%|TL 8.5/ 300 BCE|3|Wood|1 KG|1.5/ 3.5
|Child's Bow|Fast, Ranged (Bow)||1 Action|40 M|1.00%|19% +Spe +Fin|0|7.00%|TL 3.2/ 60,000 BCE|1|Wood|.5 KG|.3/ 1.8
|Adult Longbow/Selfbow|Fast, Ranged (Bow)|Perquisite: 0 Might or more otherwise automatic Fatal failure.|1 Action|75 M|2.00%|45% +Spe +Fin|1|7.00%|TL 3.4/ 8000BCE|7|Wood|1 KG|1.5/ 8.6
|Heavy Longbow/Selfbow|Fast, Ranged (Bow)|Perquisite: 25 Might or more otherwise automatic Fatal failure.|1 Action|125 M|3.00%|45% +Spe +Fin|2|7.00%|TL 3.4/ 8000BCE |8|Wood|1.5 KG|1.7/ 10
|Pistol Crossbow|Fast, Ranged (Pistol)||2 Actions|40 M|1.00%|45% +Spe +Fin|0|7.00%|TL 9.5/ 181 CE|5|Wood|3 KG|.3/ .6
|Full Crossbow|Heavy, Ranged (Rifle)|Two-Handed, Tou gives Damage Effect Bonus up to double base damage effect. Prerequisite: Toughness equal to base damage effect or more otherwise automatic Fatal failure.|3 Actions|125 M|4.00%|45% +Fin|3|7.00%|TL 8.4/ 400 BCE|8|Wood|8 KG|3.5/ 8.1
|Light Compound Bow|Fast, Ranged (Bow)|Prerequisite: -5 Might or more otherwise automatic Fatal failure.|1 Action|150 M|1.00%|45% +Spe +Fin|0|10.00%|1960 CE|1|Composite|1.5 KG|0.6
|Medium Compound Bow|Fast, Ranged (Bow)|Prerequisite: 0 Might or more otherwise automatic Fatal failure.|1 Action|182 M|2.00%|45% +Spe +Fin|1|10.00%|1960 CE|3|Composite|2 KG|4
|Heavy Compound Bow|Fast, Ranged (Bow)|Prerequisite: 25 Might or more otherwise automatic Fatal failure.|1 Action|170 M|3.00%|45% +Spe +Fin|2|10.00%|1960 CE|6|Composite|2.5 KG|7
|Compound Crossbow|Heavy, Ranged (Rifle)|Two-Handed, Tou gives Damage Effect Bonus up to double base damage effect. Prerequisite: Toughness equal to base damage effect or more otherwise automatic Fatal failure.|2 Action|125 M|3.00%|48% +Fin|2|10.00%|1980 CE|4|Composite|3.5 KG|5

**Arrows**

For guns and bullets, being that they are primarily used for games in the current era, have had the tech level and year portion dropped.

|Name|Categories (Sub-categories)|Traits|Recovery Rate|Tech Level/ Year|Creation Difficulty|Primary Material|Weight|DW C/P
|---|---|---|---|---|---|---|---|---|
|Blunt Arrow (6)|(Arrow, Ammo)|No change in effect (Default Arrow)|05/06/24|TL 3.1/ 80,000 BCE|1|Wood|.12 KG|.02/ .1
|Rigid Flint Broadhead (6)|(Arrow, Ammo)|+5% Damage, -1 drop range|05/06/24|TL 3.1/ 80,000 BCE|1|Flint|.3 KG|.03/ .2
|Rigid Bronze Broadhead (6)|(Arrow, Ammo)|+10% Damage, -1 drop range|05/06/24|TL 8.6/ 850 BCE|1|Bronze|.4 KG|.04/ .1
|Tri-Blade Broadhead (6)|(Arrow, Ammo)|+15% Damage, -3 drop range|05/06/24|TL 11.7/ 1600 CE|1|Steel|.5 KG|.4/ .7
|Mechanical Broadhead (6)|(Arrow, Ammo)|+12% Damage|04/06/24|TL 11.8/ 1660 CE|1|Steel|.3 KG|.4/ .7


## Ranged Weapons - Guns & Bullets
**Bullets**

In the case of guns & bullets, the bullet type determines the damage and effect more than the gun itself does (being what contains the charge, controls the surface impact area, etc.) Further, the bullet is destroyed in use, making damage reduction pointless, and so it is not included on the table. To abuse a common phrase, "Guns don't kill people, bullets do."

Bullet Type|Categories (Sub-categories)|Traits|Drop Range|Damage Effect|Vital Strike Chance|Follow-through|Creation Difficulty|Primary Material|Weight|Day's Wage Cost
|---|---|---|---|---|---|---|---|---|---|---|
|.22 Short (20 rounds)|(Firearm, Ammo)|Shock Effect, -6 to sneak on use|1609 M|1.00%|45.00%|0|5|Brass|.02 KG|0.15
|.22 Long (50 rounds)|(Firearm, Ammo)|Shock Effect, -6 to sneak on use|2414 M|1.00%|45.00%|1|5|Brass|.1 KG|0.06
|.30 Carbine (50 rounds)|(Firearm, Ammo)|Shock Effect, -8 to sneak on use|1196 M|3.00%|45.00%|4|5|Brass|.35 KG|0.24
|.30-06 (20 rounds)|(Firearm, Ammo)|Shock Effect, -15 to sneak on use|582 M|10.00%|45.00%|15|5|Brass|.2 KG|0.21
|.38 Short (500 rounds)|(Firearm, Ammo)|Shock Effect, -7 to sneak on use|346 M|2.00%|45.00%|1|5|Brass|3.5 KG|0.38
|.40 Short (100 rounds)|(Firearm, Ammo)|Shock Effect, -15 to sneak on use|198 M|10.00%|45.00%|8|5|Brass|1.1 KG|0.29
|.45 ACP (250 rounds)|(Firearm, Ammo)|Shock Effect, -8 to sneak on use|496 M|3.00%|45.00%|1|5|Brass|3 KG|0.96
|.45 Remington (50 rounds)|(Firearm, Ammo)|Shock Effect, -22 to sneak on use|236 M|17.00%|45.00%|11|5|Brass|.8 KG|0.28
|.50 AE (10 rounds)|(Firearm, Ammo)|Shock Effect, -26 to sneak on use|304 M|21.00%|45.00%|11|5|Brass|.2 KG|0.35
|5.56x42mm  (20 rounds)|(Firearm, Ammo)|Shock Effect, -13 to sneak on use|412 M|8.00%|45.00%|24|5|Brass|.08 KG|0.12
|7.62x25mm (20 rounds)|(Firearm, Ammo)|Shock Effect, -9 to sneak on use|1408 M|4.00%|45.00%|5|5|Brass|.12 KG|0.18
|9mm  (20 rounds)|(Firearm, Ammo)|Shock Effect, -13 to sneak on use|274 M|8.00%|45.00%|8|5|Brass|.14 KG|0.34
|12 gauge slug (5)|(Firearm, Ammo)|Shock Effect, automatically fail any sneak attempt on use, Prerequisite: 10 Toughness or more otherwise automatic Fatal failure.|10 M|109.00%|45.00%|7|5|Brass|.2 KG|0.04

**Guns**

Name|Categories (Sub-categories)|Traits|Clip Size|Reload Time|Vital Strike Chance|Damage Reduction|Creation Difficulty|Primary Material|Weight|DWC
|---|---|---|---|---|---|---|---|---|---|---|
|Pipe & Hammer|Ranged|Makeshift, Choose the ammo type it can use on purchase.|1|1 Action|+Fin|14.00%|1|Steel||0.5
|Standard Pistol|Ranged, Fast (Pistol)|Choose the ammo type it can use on purchase. |6|3 Actions|+Spe, +Fin|14.00%|3|Steel|1|4
|Standard Rifle|Ranged, Heavy (Rifle)|Two-Handed, Choose the ammo type it can use on purchase. Tou gives Damage Effect Bonus up to double base damage effect. Prerequisite: Toughness equal to base damage effect or more otherwise automatic Fatal failure.|1|1 Action|+Fin|14.00%|4|Steel|3|5
|Standard Shotgun|Ranged, Heavy (Rifle)|Two-Handed, Choose the ammo type it can use on purchase.Tou gives Damage Effect Bonus up to double base damage effect.Prerequisite: Toughness equal to base damage effect or more otherwise automatic Fatal failure.|1|1 Action|+Fin|14.00%|3|Steel|3|6
|Standard Semi-Automatic|Ranged, Fast (Pistol)|One-and-a-half handed, Choose the ammo type it can use on purchase, May fire two shots in a single action. Prerequisite: Toughness equal to base damage effect or more otherwise automatic Fatal failure.|30|1 Action|+Spe, +Fin|14.00%|4|Steel|3|6
|Standard Combat Rifle|Ranged, Heavy (Rifle)|Two-Handed, Choose the ammo type it can use on purchase. Tou gives Damage Effect Bonus up to double base damage effect. Prerequisite: Toughness equal to base damage effect or more otherwise automatic Fatal failure.|20|1 Action|+Fin|14.00%|5|Steel|4|7
|Standard Combat Shotgun|Ranged, Heavy (Rifle)|Two-Handed, Choose the ammo type it can use on purchase. Tou gives Damage Effect Bonus up to double base damage effect. Prerequisite: Toughness equal to base damage effect or more otherwise automatic Fatal failure.|6|3 Actions|+Fin|14.00%|5|Steel|3|8

## Enhancements
Sooo, you may have thought those previous numbers were fighting with sharp weapons? Nope! All the stats in this game are for high quality weapons, true, but as they would come from the forge, before being sharpened, before being filed to points, and more. Everything you buy comes out blunt and vanilla. Enhancements are what gives your weapon that extra bite, that bit of customization getting it to just that deadly edge you want. Do you want to leave it blunt so it can stand up to more wear and tear over the course of a long battle, or do you sharpen it to a razor edge, guaranteeing that you'll slice through the enemy like butter... but also guaranteeing your weapon won't have a long lifespan. The following are but some enhancements you can add to your weapons.

Name|Effect|Can add more than once?|Cost
|---|---|---|---|
|Cheap|Reduce damage and/or damage reduction (must be applicable) by one quarter.|Yes|Reduce by 1/4th
|Double Barrel|For Ranged weapons, increases the weight by half, but allows twice the ammo loaded at once (if a gun, you may choose a different ammo type for the other barrel.)|Yes|Double Cost of weapon
|Extended Magazine|For firearms, double the clip size.|Yes|Increase by 1/5th
|Piercing|Add the stabbing trait to ammo|No|Double ammo cost
|Recoil Compensator|Half the toughness prerequisite for a Rifle or Pistol (if this would reduce prerequisite to less than 1, reduce it to 0)|Yes|Increase by 1/2
|Sharpen|Add +5% to your weapons damage, and +3% to your vital strike chance ... but at the cost of -1% Damage Reduction.|Yes|Free
|Counterweight|Add 10% to the weight of the weapon to remove one level of heavy-headed by adding a counter-weight to the hilt. (this can either be a stable weight, a weight on a chain, or more elaborate hilt or handle). Further, adding more levels of counterweight than the heavy-headed level gives you that many levels in near-balanced. |Yes|Increase by 1/10th
|Hardcore|Add 5% to the weight of the weapon to increase the damage reduction by 2%. For melee weapons, also add an additional 1% damage.|Yes|Increase by 1/10th
|Weight holes|Reduce the weight by 2% and decrease damage reduction by 2%|Yes|Free


## Weapon Traits
We told you we'd let you know what these were in the introduction to possessions, so here they are for the weapons! Various traits, and what they do!

**Bladecatcher:** For every level a weapon has in bladecatcher, the wielder receives a similar bonus to attempts to disarm.

**Blunt:** Excellent Effect:** instead of normal vital strike effect, vital strike instead renders target unconscious. 

**Double-Edged:** A double-edged weapon doubles the range where excellent effect happens, but also doubles the range in which fatal failure happens. (For most, this means excellent effect happens on the top two highest possible rolls instead of one, and fatal failure happens on the bottom possible two rolls instead of the bottom one.)

**Heavy-headed:** The weapon gets a bonus to damage equal to the levels in heavy-headed, but at a cost of a similar loss to the initial contest roll. 

**Hilt:** Hilt gives a bonus to deflection, but a similar minus to the contest rolls. However, the hilt is removable, removing this combination of help/hindrance.

**Makeshift:** This is a weapon that's usually made by the wielder. As such, it usually has no price. If a player wishes to buy one instead of make it, there is not set price for the item, and the GM is encouraged to give it for whatever they want to pay (if the salesman is honest) or have fun with it (if the salesman is dishonest and sees the character as an easy mark.)

**Near-balanced:** The weapon gets a bonus to the initial contest roll, for an equal amount lost to damage.

**One-and-a-half handed:** The weapon can be used with either one hands or two. Using two increases the ability bonuses to damage and vital strike by half. 

**Shock Effect:** The weapon causes an intense fear effect just before or at the time of damage, emphasizing it's effect. This causes it to get two vital strike chances. One against the target's Body, the other against the target's mind. If the weapon is made subtle in any way (such as silencing), the shock effect is removed. 

**Slashing:** Slashing attacks can ignore half of the target's damage reduction.

**Stabbing:** If any of the attack's damage gets past the target's damage reduction, all of it does.

**Styled Weapon:** The weapon is built to be compatible with a style of unarmed martial art. If you are using this weapon, you may switch out any martial art effect for the effect of this weapon when using the martial art ability.

**Throwable:** For these weapons, the ability "Throw" may be used instead of the related arms ability. If done so, the attack may go your throw range. However, note that the weapon is now where you threw it and you don't have it on hand anymore when you do this!

**Two-Handed:** The weapon requires two hands to use. However, this doubles ability bonuses to damage and vital strike chance.

## Armor

Most Armors are created by a combination of smaller armor pieces. This is made more complex by the fact that many types of armor may be mixed and matched, more vital areas being protected by additional layers armor, and less vital areas being protected by armor that allows more freedom of movement, or not covered at all. As such, instead of forcing players to buy complex armor sets, getting each piece individually, or just selling whole armor sets, ignoring customization, we instead have general regions of armor representing many different areas that could be covered, and layers of each. Buying armor for each region has a certain effect per layer. (Multiple layers can be bought, representing increasing thickness and coverage of armor.)
The result is one custom suit of armor with custom stats, which you can name whatever you like. (A few examples will be included after the table.)

**Damage Reduction (DR) and Armor**
Damage Reduction is a bit more complicated with armor than with weapons. Damage Reduction is how much damage a possession can take before it becomes damage. With most things, going past this point bends it or deforms it, rendering it useless, but anything that doesn't pass the damage reduction is ignored. However, with armor, it's shape is less important to its function, and most covering of the armor's covering remains even after it's been penetrated, allowing the armor to be used again even after it's been "broken" initially. As such, armor has it's own body trait, with this, a player may treat subtract body damage from their armor before taking it from their character whenever damage gets past the armor's damage reduction.

> **Optional GM Variant:** The previous rule on damage reduction is assigned to help show the effect of armor and it's material for the individual wearing it. Although it is not suggested for most games (as it would slow down gameplay immensely), a GM highly concerned with realism should instead use the following rule: Armor's body score does not reduce the damage to the player in any way. Instead, reduce armor's vital protection for each 1% body damage it's sustained. Each time the damage reduction is passed by an attack, the armor takes 1% body damage. The armor is not useless and can still be repaired until it has reached it's body damage. However, once this point is reached, the armor is no longer usable.

**Armour Category:** Armour parts come in two categories: Heavy, and Light. However, this leads to the question of if the resulting combination (if mixed and matched) is heavy or light. Simply, if there are two or more regions/layers that have heavy armor, it counts as heavy armor. If you have six or more regions/layers of light armor, it becomes heavy armor.

|Name(Armor Type / Material / Work Cost)|DaysWages Current/Period per level|TL / Year|DR per layer|Body per layer|Weight per layer|Vitals Region Vital Protection (VP) Effects|Senses Region Vital Protection (VP) Effects|Limbs Region Effects|Joints Region Damage Effects
|---|---|---|---|---|---|---|---|---|---|
|Padded Armour (Light / Straw / 1)|.14/ 2.3|TL 1.2|2.00%|1.00%|1 KG|VP: 5% Blunt weapons treat as DR 5%|VP: 5% Blunt weapons treat as DR 5%|Blunt weapons treat as DR 5%|Blunt weapons treat as DR 5%
|Scale/Lamellar (Heavy / Bronze / 19)|9.9/ 22.4|TL 8.6/ 900 BCE|9.00%|30.00%|6 KG|VP: 85%|Scale is unavailable for Sense, Limbs, or Joints Regions||
|Splint/Banded (Heavy / Steel / 9)|6.1/ 13.0|TL 9.1/ 400 BCE|16.00%|18.00%|6 KG|VP: 85% -1 Fin|Splint is unavailable for Sense Region|DR: 16% -1 Fin|Splint is unavailable for Sense Region
|Chain (Light / Steel / 4)|2.9/ 6.1|TL 9.3/ 200 BCE|16.00%|13.00%|4 KG|VP: 80% -1 Fin|VP: 15% -2 to all observe rolls|-1 Fin|-1 Fin
|Solid Plate (Heavy / Steel / 10)|7.5/ 13.4|TL 10.9/ 1350 CE|16.00%|26.00%|7 KG|VP: 85% -1 Spe|VP: 15% -8 to all observe rolls|-1 Spe|-1 Spe
|Ballistic (Light / Armid Fabrics/ 8)|7.9/ 8.1|TL 19/ 1970 CE|241.00%|183.00%|2 KG|VP: 60%|VP: 10%||

**Shields**: 

|Name|Categories (Sub-categories)|Traits|Reach|Damage Effect|Vital Strike Chance|Follow-through|Damage Reduction/ Body|Tech Level/ Year|Creation Difficulty|Primary Material|Weight|DaysWages Current/ Period 
|---|---|---|---|---|---|---|---|---|---|---|---|---|
|Viking Shield|Heavy, Melee (Bludgeon, Shield)|Blunt, Vital Protection Receives +54% VP, Add Fin to VP, Add 7% + Tou to DR on successful deflection|1 M|52% +Tou +Mig|37.00%|0|7% / 6%|TL 10/ 750 BCE|3|Wood|6 KG|2/4
|Italian Buckler Fist Shield|Heavy, Melee (Bludgeon, Shield)|Blunt, Vital Protection Receives +5% VP, Add Fin to VP, Add 8%  + Tou to DR on successful deflection|1 M|7%  +Tou +Mig|2.00%|0|8%/11%|TL 10.5/ 1100 CE|2|Iron|1 KG|1.5 / 2.8
|Riot Shield|Heavy, Melee (Bludgeon, Shield)|Blunt, Vital Protection Receives +46% VP, Add Fin to VP, 241% + Tou to DR on successful deflection|1 M|20% +Tou +Mig|45.00%|1|241% / 183%|TL 19/ 1970 CE|2|Ballistic |2 KG|1.1 / 1.2

Some example Armor Sets:

|Package|Contents|Net Effect|Day's Wage Cost Current/Period
|---|---|---|---|
|Japanese-style Conscript Armour|Lamellar Vitals, Lamellar Limbs, Padded Joints|13 KG DR 20%, 61% Body, VP 85%, Blunt weapons treat as 23%.|19.9 /47.1 (900 BCE)
|Full Plate Medieval Armour|Armor: Plate Vitals, Plate Senses, Plate Limbs, Joints Region|27 kg DR 64%, 104% Body, VP 85%, -3 Speed (and lower abilities), -8 Observe|30 / 53.6 (1350 CE)
|Modern combat Armour|Ballistic Armid Fabrics Vitals, Ballistic Armid Fabrics Senses|4 kg DR 482%, 366% Body, VP 70%|15.8 / 16.2 (1970 CE)

## Equipment

Possession Name|Traits |Tech Level / Year|Creation Difficulty|Primary Material|DWC C/P
|---|---|---|---|---|---|
|Torch/Lantern/Flashlight|Remove any negatives that may be caused by a lack of light up to a number of meters equal to the Tech Level the item was created in. After that point, lack-of-light negatives increase at -1 per meter until they reach the same as the environment.|3.3|1|Varies|0.2
|Rope (per 10 ft)|There are neigh uncountable number of uses for rope.  I will NOT be listing them all here.|3.5/ 15,000 BCE|1|Plant Matter|.1/ .6
|Horse|Has an upkeep cost of 2 Day's Wage cost a month, and 20 minutes a day. If used in place of traveling on foot, travel distance is doubled.  (For reference: a typical person can travel 50 miles on foot a day, and sprint 35 kph) A horse can carry 2 people.|6.7/ 3500 BCE|-|-|47
|Adventuring/Camping Gear|Standard camping gear for the Tech Level for one person. (Typical items may includes 10 ft of rope, pack, bed roll, local maps, compass, water jug, fire starter kit, a bug deterrent, 1-person tent, a quick reference guide, and mallet). (Price does not change through Tech Levels, components are just updated.) The gear gives a bonus to survival equal to the Tech level the item was created in.|9.8/ 600 BCE|2|Assorted|2.5
|Multi-tool|Tool includes knife (not optimized for combat), tweezers. toothpick, can opener, screwdrivers, scissors, and a few other minor tools. Overall, the tool gives a +2 bonus to craft and survival.|13.3/ 1880 CE|1|Steel|.2/ .3
|1-prop Plane|Has an upkeep cost of 12 Day's Wage cost a month, and 15 minutes a day. The 1-prop plane can travel at 200 kph, and can carry four people, and 90 kg.|15/1915 CE|400|Assorted|705/ 908
|Compact Car|Has an upkeep cost of 10 Day's Wage cost a month, and 5 minutes a day. The compact car can travel at 70 kph, and can carry four people and 200 kg.|16/ 1920 CE|150|Assorted|470/ 567
|Space Suit|This allows a character to survive the otherwise unsurvivable conditions of open space.|19/1950 CE|11000|Assorted|12000
|Laptop|Provides a +1 to any Mind ability check that doesn't require intense time or fatigue.|19.2/1975 CE|10|Assorted|7/ 7.1

(Note: These are just some example items. To get day wage cost for many other items, see TMI on finding cost of items at the end of this chapter.)

## Homes, Bases, & Buildings

Unlike most other possessions, the price of buildings doesn't change as much (mainly due to the cost of land that rises as tech level rises, and rising demands and restrictions on buildings that grow along with tech level.) As such, there is only one cost for each. Each room created can comfortably hold 4 people at once.

Primary Material (Frame)|Damage Reduction|Upkeep per year (of total cost)|Tech Level|Creation Difficulty|Day's Wage Cost Per Room
|---|---|---|---|---|---|
|Dirt|15.00%|8.00%|1.1|20|39
|Stone|120.00%|4.00%|2.2|62|119
|Wood Planks|45.00%|5.00%|3.4|45|107
|Brick|95.00%|3.00%|8.6|50|110
|Concrete|90.00%|2.00%|9|80|156
|Steel|160.00%|1.00%|11.7|110|234

**Room Additions**
Note: Additions to a house also include appropriate fixtures, furniture, etc.

|Addition|Traits |Tech Level / Year|Room Cost,  upkeep, & Creation Difficulty Adjustment
|---|---|---|---|
|Modern Conveniences|Conventional Plumbing, electricity, restrooms (or outhouses), Insulation, Heating, closets, and more are all covered by this addition as appropriate for the time period. The house is limited to a number of bedrooms equal to the times this addition has been bought. Each bedroom can house two people comfortably. Any attempt to reduce fatigue in a bedroom has any costs (besides time) reduced by half. Time costs may be reduced by one fourth.| - |x2
|Gathering Room|(Ballrooms, throne rooms, sitting rooms, etc.) Any social modifiers made by the owner of the building targeting others in the room are increased by 5 in this room. Further, number of people who can comfortably use the room at once is tripled.|-|x4
|Reinforced|Double the room's damage reduction.|-|x4
|Big|Double the number of people the room can comfortably hold at once. (May be bought more than once.)|-|x2
|Tiny|Half the number of people the room can comfortably hold at once. (May be bought more than once)|-|x.5
|Kitchen|Cooking rolls are tripled for any character in this room.|900 CE|x3
|Turret|Any ranged combat contest made from the building towards a location outside the building has all rolls doubled, a second parapet triples the effect, and so on, up to the number of defenders in the building, drop ranges are doubled.|1100 CE|x6
|Generator Room|The room produces enough electricity for 6 standard-sized rooms. (Big rooms count double, tiny rooms count for half). This reduces upkeep costs for all those rooms to one quarter.|1832 CE|x10

**Packages**

|Possession Name|Traits |Tech Level |Creation Difficulty|Primary Material|Day's Wage Cost
|---|---|---|---|---|---|
|Mud Hut|Tiny Room w/ modern conveniences|1.1|20|Dirt|39
|Pleasant Log Cabin|Reinforced (representing treated logs instead of planks) Modern Conveniences, Reinforced Kitchen, Reinforced Gathering Room Big x2|3.4|2340|Wood|5564
|Castle|6 Big Reinforced Turrets, 3 Reinforced Big x7 Gathering Rooms, 5 Reinforced Big x 4 Modern Conveniences, 2 Reinforced Kitchen, |10.4/ 1100 CE|439208|Stone|842996
|Small Mansion|Big Modern Conveniences x4, x2 Big x3 Gathering Rooms, Kitchen x2|10.2/ 900 CE|4300|Brick|9460
|Condo Apartment |Modern Conveniences, Kitchen, Gathering Room Big x2|11.7/ 1625 CE|1040|Steel|2028

## Raw Materials
*(TMI on stating out Raw Materials at the end of this chapter.)*

|Material Name|Categories|Traits |Density KG:Liter (Decimeter ^3^) aka grams : cm^3^|Damage Resistance/ Body per KG|Tech Level|Harvest Difficulty|Rarity|Day's Wage Cost per Kilogram
|---|---|---|---|---|---|---|---|---|
|Brick (Clay)|Natural Material|Where found: Areas with frequent low-intensity fires. However, much simpler to use if created from a decent grade of clay (KG per Hour based on making your own) - requires immediately available brick molds - production limited to available brick molds. Bricks must solidify for a day in heat before becoming usable.|02:01:00 AM|1% / 1%|3.3|3|1|0.01
|Bronze|Alloy (Copper)|How made: Combining copper with an alloy (often tin)|09:01:00 AM|9% / 30%|6.5|-|5|0.08
|Dirt (Topsoil)|Natural Material|Where found: The ground, where plants grow.|06:05:00 AM|0% / 0%|1.1|1|1|.0004
|
|Gold|Scientific Element, Alchemical Elemnt|Where found: Ore veins (Ore weight: +1d4 kg per kilogram of gold)|07:01:00 PM|0% / 6%|6.2|19|200|283
|Iron|Scientific Element|Where found: Ore veins|02:01:00 AM|8% /11%|6.7|10|1|0.22
|Standard Steel|Alloy (Iron)|How made: Iron is combined with a percentage of raw carbon or coal.|08:01:00 AM|16% / 26%|6.8|-|2|0.25
|Wood|Natural Material|Where found: In forests, as part of the trees.|05:01:00 AM|7% / 1%|1.1|12|3|0.5


## Supplments

Supplements are items that are taken to modify the creature taking them in some way. To make the effects semi-permanenet and reduce cost, the characters can take them with the Supplementary Health Aspect.

Each Supplement has an A Category and a B Category. A character who takes two supplements that an share an interaction category will automatically trigger a random potential side effect in both. 

Dosage cost is a way to quickly tally Day's Wage cost among multiple supplements, and takes into account the "buying in bulk" discount, and should only be used if buying in mass or as part of the Supplementary Health Aspect. To convert Dosage Cost into Day's Wage Cost, add all dosage costs together and divide by 100.

If bought with the straight Day's Wage Cost, the supplement should be considered a single dose. 

In General Categories, Foods and Vitamins generally have no Common Side Effects and fairly benign (or even desirable) Potential Side Effects. Drugs and Medicines nearly always have side effects, and serious potential side effects. Cultures may treat different categories differently as well. 

|Supplement Name|General Category | Interaction Categories | Effect | Common Side Effects | Potential Side Effects | Tech Level |Creation Difficulty|Day's Wage Cost | Dosage Cost
|---|---|---|---|---|---|---|---|---|---|
| Chocolate | Food | Blood Pressure, Complex | User heals 1 body fatigue | None | User takes 1 body Fatigue | 0.0 | 0.0 | .025 | 1
| Milk | Food | Stimulent, Complex | If user would roll to gain a character point, they may reroll it. | None | User rolls 1d100. If they roll a 1, they lose a level in skinny (if they have any) or gain a level of obesity. | 0.0 | 0.0 | .015 | 1
| Willow Bark | Herb | Depressent, Blood Pressure | If user would roll to gain a character point, they may reroll it. | None | User rolls 1d100. If they roll a 1, they lose a level in skinny (if they have any) or gain a level of obesity. | 0.0 | 0.0 | .03 | 2


> T.M.I. - Too much information
TMI on Tech Level: Finding Tech Level
For those of you more science oriented types wanting to know what Tech level is based on, it's calculated by the total energy consumption of society (in watts) multiplied by the energy efficiency of the best among its commonly used technology. This is rated on a pseudo-logarithmic scale. In other words, the first number is the power of ten of the number is, and the first few actual digits of the number go after the decimal place. For example, 25,000 would become 4.25, because there are 4 digits after the 2, and 2 and 5 are the first digits. Throughout the game, this is what a Tech Level represents.

![Historical Tech](./img/historicalTechLevels.jpg)![Modern Tech](./img/modernTechLevels.jpg)

> Further geeky notes: 
> When the first edition of this book was written in 2010, humanity had a tech level of 13.3.  
> 
> The world's medieval eras had a tech level of about 8-10 (depending on part of the world). 
> 
> The Renaissance had a tech level of about 10.6. The era of pirates was a tech level of about 11. 
> 
> Far back, just after we began to regularly use fire, we had a tech level of about 3.3 (about 50,000 BCE). Before fire, humanity had a tech level of 1.2 (about 400,000 BCE), and a herd of deer has a tech level of about 1.1
> 
> As far as future projections go, the earth is capable of supporting, sustainably, a maximum tech level of 20.8, assuming the Earth is 100% covered with 100% efficient solar collectors, and everyone lives underground and doesn't waste a watt of electricity. At our current rate of growth, assuming we stop our current rate of growth-acceleration and things even out to past growth rates, we'll hit this point around 2190. 
> 
> If we had keep our current rate of growth-acceleration we've had since we discovered generated power from the time this was written, we would have hit this point in 2015. Fortunately, by 2022 (most recent update of the neccesarry numbers at time of this edit in 2024), we’re only at a tech level of 14.1. 
> 
> Hitting this crucial tipping point doesn’t necessarily mean we will be covered with solar panels, it just means it is only a matter of time before we completely run out of resources, and the growth of our civilization comes to a grinding halt or we have to be colonizing space in mass. In short, after we hit tech level 20.8, an Earth-like civilization NEEDS to be an interplanetary civilization, or its effectively doomed long term.
> 
> Further, taking this into account, a multi-world civilization is required for a tech level of 20.9. Otherwise, at this point, all borrowed time is spent up, and any energy reserves (Coal, oil, etc.) have run out and the civilization collapses one way or another (lasting until the end when resources die out, society fighting internally over the few resources that are left, wiping itself out, falling into a dark age, etc.)
> 
> A decent sized interstellar civilization would be expected around tech level 25. 
> 
> A Dyson sphere (a super-structure that completely blots out a sun and makes use of all the energy it puts off) or large interstellar civilization is a tech level 33.3
> 
> ***TMI on Day's Wage Cost: Finding Day's Wage Cost and Creation Difficulty***
> It should be noted that a Day's Wage Cost is the base price of an item, and usually characters don't find it at these prices unless they buy straight from the people who make it. Stores or shipping from manufacturer will usually bump up the price significantly . A GM is not required to sell you items at the Day's Wage Cost and may adjust it as they see fit for the campaign they run. A typical markup for a store can range from an additional 30% to 150%. (Although stores may also offer things like return policies, store rewards, guarantees, etc.)
>
>Finding the Day's Wage Cost: Look up the cost of the item from manufacturer (or, if easier, use online and ignore shipping costs). Then divide that by the median hourly wage for the area (For example,  2010: £120 in the U.K.,  $112 per day in the U.S.) So: 
>
> **Real Life Cost / Median Daily Wage = Day's Wage Cost**
>
>In general, objects are sold (assuming you buy it from the seller, and not a store) follow a pattern of adding the cost of the materials and labor, and then marking up by 10%. However, cost of labor and price of materials both go down as technology improves, making the crafting process and the harvesting of materials both easier. The result is an equation we use for our system that looks like the following:
>
> **((Materials + Labor) X 1.1) / Tech Level = Day's Wage Cost**
>
>And we run KG per Hour for raw materials as such:
> (Note: Many manufacturers, even if they automate the creation process, rarely drop the 'cost of labor' addition, just turning it into more profit. Some will drop a marginal amount to give them an edge on their competitors, but it usually remains a decent algorithm.)
>
>So, with the addition of the info here plus the , do the following:
>
> ** (((Real Life Cost X Tech Level) / (Median Daily Wage X 1.1)) - (Daily  Wage Cot of Primary Material X Kilograms of Primary Material))/5.75 = Creation Difficulty**
> 
> (In case you're wondering, the final Division by 5.75 is the value to translate the value so the Creation Difficulty actually gives a relatively accurate Craft time for the item creation, bringing closer to in line with ability scores.)
> 
> **TMI on Finding Day's Wage Cost for a particular real-world item:**
> As it so happens, I'm not all-seeing (or have that many lifetimes to spend on this project). It is likely that there will be items you want for your character that I have not included in the possessions section of the game. 
>
> To find the in-game cost of any real-life item, follow the following method: 
> 1. Get the price of the item in real life at present.
> 2. Divide by the median income for your society per day (at the time of writing, this is about 64 Euros, 85 USD, 7000 yen, or 500 yuan.) 
> 3. Multiply that value by 19.3 (our modern Tech Level) 
> 4. Divide the cost by the tech level you're playing in (although you can't buy an item before it's invented!)
> 5. You're done.
>
> **TMI on KG per Hour: Determining KG per Check and Harvest Difficulty**
> 
> Harvest Difficulty is simply 300 divided by the KG per Check, rounded down.
>
> Getting the KG per check is easy if you can find how much an average person can harvest/mine/etc. in a given time, and just convert that time to 10 minutes.
> 
> For reference, a person dig about 300 kg of dirt in ten minutes, assuming it's loose dirt and they're of average strength. 
> 
> Day's wage cost is calculated by the following formula:
> 
> **Day's Wage Cost = (Cost for a kilogram X tech level / average daily wage)**
> 
> Rarity percent is figured by dividing the KG per Hour by the Day's Wage Cost.

# <a name="faq"></a> FAQ

**Q. With the Grand Rush and the Perfect rush abilities, they're the same ability, why the heck should I get the second one?**

A. Grand rush and Perfect rush can each be used only once per roll. However, they're nothing preventing you from using both of them on the same roll if you have both. Using both make it so that instead of reducing prep time to half, you can reduce it all the way down to one fourth. For that matter, there's nothing preventing you from using the Rush and Focus aspects on the same action as well. 

**Q. What are the empty circles for on the character sheet next to the higher abilities?**

A. Since higher abilities are determined by how much of their lower abilities are applied to them, but it's on a 4-to-1 rate (or 2-to-1 for being), the dots are there to mark your excess until you hit the next level. Technically, you could instead write your abilities as "Might: 12.50" or "Creativity: 8.75", but we thought this option would make it easier for some people. It's just a way to tally the fractions quickly and easily.

**Q. Can I write and publish my own stuff for the TOY TTRPG system?**

A. Look up the license later in this book for more details, but generally, the answer is "yes." Just don’t train AI off of it.

**Q. Can I help you write some of your books?**

A. Again, look at the license at the end. If you want to actually help me expand the book series in an official way, send me an email and we can work out a deal. 

**Q. Roleplaying games... is that some kind of evil cult or something?**

A. I've heard this question a few times, mainly due to a badly researched documentary back sometime around the 80's, which then got passed around some of the creepy religious cults for awhile trying to accuse others of being in creepy religious cults. Those rumors started around the time of those old school films that said hiding under your school desk will protect you from an atomic bomb, and they're about as accurate.

**Q. You keep using "BCE" for time instead of "BC", are you promoting atheism?**

A. I thought about using Mayan calendar system since my game's first edition came out in 2012, but it'd be a joke that would only last the rest of the year. My personal view is that unless whatever (G)god(ess)((e)s) people believe in are willing to come and talk to me in person (which they should be able to do if they are all powerful) and logically explain to me why being selective towards one group of people's way of doing things is the right one, that I should take the more accepting route. I actually wanted to just use Tech Levels only, but since most people wouldn't get where they relate to real-world time, I had to include a year system, so I went the more academic route... it just felt better to do so. So choose "Yes" or "no" to answer that question, whichever makes you happy.

**Q. Why does your system use the metric system?**

A. Because the math is a billion times easier than having to remember how many cubic feet make a quarter ton water, which is however much in pints and a thousand other really weird conversions that non-metric systems have to use. Metric says everything's a multiple of ten, and 1 kilogram of water takes up one liter of space and is 1 cubic decimeter volume and different scales of units can be changed by adding or removing zeros is just pretty darn convenient. 

**Q. Do I need Steady Income [1] to gain Steady Income [2] or can I just spend 20 points to gain it?** 

A. No, you don't. Unless an aspect mentions other abilities as a prerequisite, or a note under a special section, they don't have limitations on what you assign your X value as when buying an aspect.

**Q. How can I gain destiny points? Is it only by some aspects?**

There are special aspects called "background and destiny". These either give, or allow you to spend destiny points. These are the only way to get and spend destiny points unless your GM directly grants you them.

**Q. Is first Tier higher or lower Tier than second tier?**

First tier is the highest Tier.

# <a name="glossary"></a>Glossary

**Ability**: A core trait of a character that can increase in levels that can easily be compared from character to character. All abilities may have aspects applied to them, assuming your character is capable of it.

**Ability Aspects**: Aspects specifically associated with an ability. Ability aspects can never be taken unless you have at least one level in the associated ability.

**Ability Level**: This is the amount of levels that you have bought for that specific ability. This is not to be confused with total score. The only thing part of an ability level is the levels bought for that specific ability. (Or for Being, Body & Mind, and Attributes, the bonuses gained from lower abilities plus species bonuses)

**Ability Roll Result**: This is the total number of bonuses that an ability has (bonuses gained from the roll, bonuses from use of other abilities and possessions to boost it, situational effects, etc.) plus its level.

**Ability Score**: An ability's level plus any permanent bonuses bought (specifically, a specialized skill's bonus from higher abilities and species bonuses).

**Aspects**: Aspects are special traits that change how a character work, often adding special situations or altering the way abilities work, the character functions, or the character's interactions with the world in some way.

**Being**: The highest tier ability, the 1st Tier ability. A character loses all this, they die. It's level is the average of Mind and Body.

**Body**: A character's primary physical ability, one of the two 2nd Tier abilities. If it's reduced down to 0%, the character is generally immobile.

**Skill**: A purchasable ability that adds its level to both upper and lower abilities.

**Campaign**: The collection of (mis)adventures the protagonists of the story will have overall. A Campaign is the complete and total story that takes place during gaming.

**Campaign setting**: This is the world in which the campaign takes place. (Examples include 'Earth – now', 'Terrea – world of magic and fantasy', 'The Alliance – a space age collection of worlds', 'Duran High – School grounds', or nearly anything else)

**Character Points**: The currency of character creation, most parts of a character are 'bought' with these. Often shortened to 'CP'.

**Common World Cost**: How much something would cost in our world. (Theoretically, if it does not actually exist).

**CP**: See 'Character Points'.

**Cre**: See 'Creativity'.

**Creativity**: A character's ability to think outside the box and come up with novel solutions. It's a 3rd Tier Mind-based ability.

**Derived Ability**: An ability that cannot be bought with character points, but instead has its level determined by other abilities. These are, in most games, Being, Body, Mind, Might, Toughness, Flexibility, Speed, IQ, Creativity,  Memory, and Sanity.

**DW**: Short for “Day’s Wages.” It shows how many days a low-income worker saving up all of their money exclusively would need to buy it.

**TOY**: A short form of saying “Tales of Yours”. Did I really need to put that in the glossary? Really? If you’re looking at this glossary entry, I guess I did.

**Dice Category**: In gaming, there are some standardized dice sizes. A category is a set of numbers that can be rolled with these dice. The categories are D2 (a coin), D3 (a d6, divide by 2, round up), D4 (a d4), D6 (a d6), D8 (a d8), D10 (a d10), D12 (a d12), D20 (a d20), and D100 (percentile & d10). Although you may never see them, there is the off chance that you will see other dice categories called for. The following are some:
D40 (a d4 for tens place & a d10 for one's place), D60 (a d6 & a d10), D80 (a d8 & a d10), D120 (a d12 & a d10), D200 (a d20 & a d10), D400 (a d4, a percentile, and a d10), D600 (a d6, a percentile, and a d10), a D800 (a d8, a percentile, and a d10), D1000(a marked d10, a percentile, a d10), etc.

**Dice Size**: This is the number of sides on a dice.

**Dictum**: A special rule about the way a particular Campaign will run set down by the DM. 

**Excellent**: An effect that is more powerful, generally due to rolling some dice  at maximum. Usually this doubles some number of the effect, or makes something that would be a failure into a success.

**Fatal**: An effect that is generally bad, and usually the result of rolling the minimum on dice.

**Fin**: See 'Finesse'.

**Finesse**: A character's ability to make accurate, controlled, and detailed movements. It's a 3rd Tier Body-based ability.

**GM**: Game Mediator – a game mediator is the referee and storyteller of a Tales Of Yours game. Most games will have a GM (with the possible exception of player-vs-player combat games). The Game Mediator typically designs the world and runs almost (if not all) of the NPCs, random events of the world, etc.

**Inside Joke**: If you have to look it up, you'll never get it.

**IQ:** A character's ability to do think quickly. It's a 3rd Tier Mind-based ability.

**Mem**: See 'Memory'.

**Memory**: A character's ability to recall information and to do and recall brute-force memorization. It's a 3rd Tier Mind-based ability.

**Mig**: See 'Might'.

**Might**: A character's ability to do physical move weight. It's a 3rd Tier Body-based ability.

**Mind**: A character's primary mental ability, one of the two 2nd Tier abilities. If it's reduced down to 0%, the character is generally unconscious.

**NPC**: Non-Player Character – an NPC means any character in the world that is not a PC. This includes shopkeepers, the antagonist to the main characters of the story, the deities which rule over a fantasy world, a PC's pet, etc. For the most part, NPCs are controlled by the GM (however, a GM will sometimes let a Player control a trained pet owned by a PC except in extreme circumstances, and occasionally another NPC as situations warrent). 

**Permanent Bonuses**: These are bonuses that stay on a character semi-permanently or permanently. (From things like aspects or special items.) They effectively increase the level of an ability, meaning that they act as a normal level in that it increases the amount an ability can be fatigued, and increases the ability's score for determining results. However, they are unlike ability levels in two very important ways. They don't cause in increase in higher or lower abilities, and they don't change the cost of buying another ability in the level. (For example, if you have an ability at level 5 you pay the same for level 6 regardless of whether or not you have you have any bonuses.)

**San**: See 'Sanity'.

**Sanity**: A character's ability to do mentally endure and resist. It's a 3rd Tier Mind-based ability.

**Skill**: A purchasable ability.

**Social Modifiers**: Social modifiers are a special modifier number without a specific ability or roll that they are attached to. Also, almost every single one has qualifiers and special circumstances involving it. Social Modifiers apply only in situations of social interaction unless specified otherwise.

**Spe**: See 'Speed'.

**Specialization**: An ability that is also a skill, but does not increase the level of derived abilities. It gains a bonus from it’s parent abilities, but it’s level cannot exceed the level of its lowest parent ability.

**Speed**: A character's ability to move quickly. It's a 3rd Tier Body-based ability.

**Square**: When using a grid system of movement, 1 square meter is simply referred to as “a square”.

**Tier ability**: Abilities are divided in multiple tiers (occasionally referenced this way by various aspects that affect certain tiers of abilities). 1st Tier is Being, 2nd Tier abilities are Mind and Body. 3rd Tier are Speed, Finesse, Might, Toughness, IQ, Creativity, Memory, Sanity. 4th Tier is the general skill abilities, and 5th Tier and on are all specializations. (Specializations of 5th Tier abilities are 6th Tier specializations, etc.)

**Tou**: See 'Toughness'.

**Toughness**: A character's ability to psychically endure and resist. It's a 3rd Tier Body-based ability.

**Turn**: An arbitrary unit of time used during intense times. When converted to 'real time', it's usually done as 1/10th of a minute (or 6 seconds), although faster characters can do more than 1 turn's actions in 6 seconds.