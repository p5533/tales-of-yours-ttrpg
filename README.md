# Tales of Yours TTRPG
![Tales of Yours](./img/TOYVertTItle.jpg)

# Complete Books
(Books are considered in a usable state, updates will either be fixing existing issues or adding new content; any changes are likely to be backwards compatible)

[Player's Guide - Lite](./TalesOfYours-Lite.rtf) : A stand-alone bare-bones version of the game, great for beginning players and one-shot campaigns. The rules are highly simplified to emphasize ease of play over balance. 

[Player's Guide - Full](./FullPlayers.md) : A Core Guide for players to build their characters with. It is setting-agnostic, and most of the resources here should be applicable to most campaign settings. Game masters who are experienced and creative may also find this is enough to run a game with.

# In Progress Books
(Books are considered incomplete, rules systems may change, are missing large swaths of sections that may be considered needed for the material in the book, may be in an incorrect format, etc. Updates will not only include fixing issues and adding new content, but addressing the afformentioned while also having the chance for sections to have complete rewrites. Changes may not be backwards compatible with previous edits)

[Master's Guide](./MastersGuide.fodt) : A Core Guide for game masters to increase the complexity at which they run games, and additional tools for advanced players. 

[Species Guide](./SpeciesGuide.fodt) : A Core Guide for including the complexity of many different types of creatures and species, both for players and game masters. All species are designed to be player-playable and to have background information important to worldbuilding and social structure (useful for both game masters and players interested in in-depth character roleplaying).

[Genre Guide - Fantasy](./FantasyGenre.md) : A Supplementary Guide meant to assist with games in a fantasy setting. 

[Campaign Setting - Mythic Space](./404) : The default campaign setting of Tales of Yours. (Book not made public yet until placeholder art replaced with proper art)

# Potential Books
(These books aren't viewable yet, as there is no content, but merely an idea of books to come)

("Attitude & Feel" Genre Guides)

Genre Guide - Supers : Supplementary Guide for unexplicable powers for rules that seem purely their own

Genre Guide - Science Fiction : Supplementary Guide for fiction inspired by, not not necceserrily bound to science

Genre Guide - Horror : Supplementary Guide for making games more terrifying, threatening, or just spooky.

Genre Guide - Mundane : Supplementary Guide for managing every day live. Good for slice-of-life stories.

Genre Guide - Information wars : Supplementary Guide for creating clues and tailing suspects and cat & mouse games of stealth and spying, from detectives to spy games

Genre Guide - Ultra-sized :  Supplementary Guide specific for including extreme size variances in game (from Kaiju to Titans to Giant Robots)

Genre Guide - The Divinities : Supplementary Guide for the interaction and conflict of religions, theologies, and the beyond-human forces behind them.

("Human Epoch" Genre Guides)

Genre Guide - Pre-history : Supplementary Guide for eras before civilization

Genre Guide - Classical History : Supplementary Guide for the early days of civilization

Genre Guide - Rennissance : Supplementary Guide for an era where science reigns in its early days of unexplored quirkiness

Genre Guide - Modern Times :  Supplementary Guide for an era set in something close to when these books are written

Genre Guide - Near Future :  Supplementary Guide for an era set in the twilight tech years before diaspora into space (from hard sci-fi to dystopia)

Genre Guide - Far Future :  Supplementary Guide for an era set is the great diaspora among the stars

("Steller Epoch" Genre Guides)

Genre Guide - Siste Notche :  Supplementary Guide for an era set approaching and after the heat death of the universe

Genre Guide - Warm Skies : Supplementary Guide for an early space era where space was room temperature and planets were raging infernos.

("Outside of Epochs" Genre Guides)
Genre Guide - Beyond Time : Supplementary Guide for settings heavily focusing on time travel
Genre Guide - Beyond Space : Supplementary Guide for settings with parallel realities and strange realms that follow different rules

(Campaign Settings & Setting Expansions)

Campaign Setting - War of the StoryTellers

Campaign Setting - Beyond Underworld

## Name
Tales of Yours - Tabletop RPG

## Description
The rules for a TTRPG (Table-Top Roleplaying Game), also can serve as a logical design for digital RPGs systems.


## Support
Please post issues on this git repo

## Roadmap
- Finish Lite Game Player's Guide [Done]
    - Convert Lite Version to .md format [ToDo]
- Finish Full Game Player's Guide [Done]
    - Finish removing legacy formating artifacts [Done]
- Finish Species Guide [20% Done]
    - Finish Humans as guide for other species
    - Develop Other species
    - Develop Gene guide
    - Develop example characters for each species
    - Develop each species' culture
- Finish Master's Guide [15% Done]
- Finish Genre Guides [1% Done]
    - Science Fantasy
    - Hard Sci Fi
    - Fantasy
    - Mythological
    - Super hero
    - Noir Mystery
    - Horror (Classic & Eldritch)
    - Romance, Drama, & Slice of Life
- Finish Mythic Space Setting [8% Done]
- Create Historical Settings [0% Done]
- Create TalesOfYours code libraries to allow easy integration into video games [0% Done]
    - Internal Character JSON format
    - Internal Species JSON format
    - Internal Culture JSON format
    - Internal Include exporting to JSON and PDF so players can move characters from game to game
    - Situational bonus tracking
    - Roll systems
    - Intense time calculations
    - Various skill test triggers
    - Creature-gen
    - Breeding algorithms
    - Culture shift algorithms
    - Support importing characters from JSON

## Contributing
This project is open to contributions

## Authors and acknowledgment
Harmony Petersen - Primary Author 

Co-authors/editors: Rodney Petersen, Jared Voshall, Leigh Anne Petersen, Elizabeth Michelle Petersen

Playtesters: Ben Anderson, Kris Brown, Max Chao, Victor Paul Haskins, Emily “Danni” Harris, Jason Heitzman, Matt Hicks, Elizabeth "Katame" Petersen, Hannah “Hanners” Holloway, Joshua Ishikawa, Tony Srimongkolkul, Dyre Steele, David “Professor Science” Troeh, Ashley “Raven” Wheeler, Jason Gevargizian, Ambyrlee “CelticWolfies” Rose Gatchel, Rylly “How much is a” Duckworth


Help via Random Discussion on Good & Evil: Adam "Zippomage" Holler

Additional Thanks to:
All the open-source creators who made libreoffice (the program this book was written with) and the earlier versions of open office.

NextCloud, where edits and updates were created between author and co-authors

All the GMs, creative players, and designers who will make additional content in the system, 
improving it for others.

## License
MIT-Like

## Project status
Basic game is uploaded. Full game needs some minor fixes then to be uploaded. Other books are still heavily in-progress.
