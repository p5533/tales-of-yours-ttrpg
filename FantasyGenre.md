# Tales of Yours: Genre Guide - Fantasy

Fantasy is probably the most common genre for tabletop RPGs. 
So, to help you run a campaign in a Fantasy setting, we've got a number of goodies in this guide 

# Abilities

## 2^nd^ Tier Abilities

### Spirit

**Higher Ability:**
Being

**Damage 1%-99% Damage Effect (also known as “Spirit Strain”):**

Whenever you take an action where you roll to see if you succeed, if the related ability is Spirit or has Spirit as a higher ability, roll a percentile dice. You must roll higher than your percentage damage or the action fails.

*Flavor & Gameplay Note: Body Damage is called damage just to help people with familiarity with game terms. This type of damage is not directly caused by an external source, but caused over-exertion from avoiding damage (willing away possessions, over-channeling, resisting soul rot ) If your player is spiritually restrained, such as in a summoning circle, it is possible for the GM to rule that damage bypasses your Spirit and goes straight to Being.*

**100% Damage effect:**

You are incapacitated, and can no longer use abilities that have Spirit as a higher ability (even if those uses of those abilities don’t require an action).

**Healing Spirit  Damage:**
Generally, characters heal Spirit Strain using the free Rest aspect. 

**Description:**
Spirit is the you that is beyond you.

Some things you can do with Spirit
    • Meditate, Pray, touch Magic
    
**Available Ability Aspects in Full Player’s Guide:** 
Generic Aspects ([Ability] Skill X aspects 0 through 50), [Ability] Skill [X] – Synergy)

**Available Ability Aspects in this Guide:** 

## 3^rd^ Tier Abilities

### Manifest (Man)
**Higher Ability:**
Spirit

**Description:**

Manifest is Spirit’s Quickness. Manifest determines how fast you can project your spirit.

Some things you can do with Manifest

    • Astral Project
    
Available Ability Aspects in Full Player’s Guide: 

**Generic Aspects ([Ability] Skill X aspects 0 through 50), [Ability] Skill [X] – Synergy)**


### Chi (Chi)
**Higher Ability:**
Spirit

**Description:** 
Chi is Spirit’s flexibility and deftness. 

Some things you can do with Chi

    • Feel the unseen

**Available Ability Aspects in Full Player’s Guide:** 

Generic Aspects ([Ability] Skill X aspects 0 through 50), [Ability] Skill [X] – Synergy)


### Will (Will)
**Higher Ability:**
Spirit

**Description:**

Will is Spirit’s raw power. Spirit determines how the raw spiritual force you can bring to bear.

Some things you can do with Were-[species]

        • Astral Project
        
**Available Ability Aspects in Full Player’s Guide:** 

Generic Aspects ([Ability] Skill X aspects 0 through 50), [Ability] Skill [X] – Synergy)

### Aura (Aur)

**Higher Ability:**
Spirit

**Description:**
Aura is Spirit’s Durability. Aura determines how much punishment your spirit can handle.

Some things you can do with Spirit

    • Resist possession, maintain self
    
**Available Ability Aspects in Full Player’s Guide:** 

Generic Aspects ([Ability] Skill X aspects 0 through 50), [Ability] Skill [X] – Synergy)

## 4^th^ Tier Abilities (General Skills)

### Were-[Species] 
**Higher Ability:**
Toughness, Aura

**Requirements:** 

You must have done one of the following:

- Been bitten by a were-[species] and survived
- Performed a successful soul-merging ritual with the creature in question
- Performed a great task for a ruling spirit of the species in question and be granted were-ability
- Be the child of a were-[species]

**Description:**

You transform into the Were-[Species] at the rising of the Full Moon, and you transform into your original species during the New Moon. If you do not have access to a Lunar cycle (for example, on a world without a moon), you are treated as perpetually on a waxing half moon (meaning you naturally remain in your non-were form). If you are on a moon, it is treated as a full moon with triple difficulty to resist its effects.

Some things you can do with Were-[Species]

    • Transform into [speices] at specific times during the Lunar cycle
    
Available Ability Aspects in Full Player’s Guide: 

**Generic Aspects ([Ability] Skill X aspects 0 through 50), [Ability] Skill [X] – Synergy)**

# Aspects

## Awakened Magic
**Category:** Magic

**Point Cost:** 10

**Prerequisite:** Must have come in physical contact with something magical during the course of your life

**Effect:** You gain access to the Spirit Tier 2 Ability, and ability to unlock Tier 3 and Tier 4 lower abilities. 

## Were-[species] Skill 1 - Resist Moon
**Category:** Magic, Ability Skill

**Point Cost:** 1

**Prerequisite:**  1 level in Were-[Species], Sanity 1

**Action:**

> #### Resist Moon
>
>**Preparation:** Fatigue Were-[species]
>
>**Check:** Were-[species] vs Phase of moon:
>
> New Moon & Full Moon: Difficulty 30
>
> Waxing Gibbous & Waning Crescent: Difficulty 12
>
> Half Moon: Difficulty 8
>
> Waxing Crescent & Waning Gibbous: Difficulty 3
>
>**Duration:** 24 hours
>
>**Target:** Self
> 
>**Effect:** Don't transform when you would otherwise. (This *does* apply to non-were[species] transformations as well!)

## Were-[species] Skill 3 - [species] speaking
**Category:** Magic, Ability Skill

**Point Cost:** 3

**Prerequisite:**  3 levels in Were-[Species]

**Effect:** You may understand the communication of [Speices]. You may speak to [Species] while in your were form, but cannot speak to your base speices language. You may speak both in a hybrid form if you have it.

## Were-[species] Skill 5 - Embrace Moon
**Category:** Magic, Ability Skill

**Point Cost:** 5

**Prerequisite:**  5 levels in Were-[Species], Sanity 1

**Action:**

> #### Embrace Moon
>
>**Preparation:** Fatigue Were-[species], 1 minute
>
>**Check:** Were-[species] vs Phase of moon:
>
> New Moon & Full Moon: Difficulty 30
>
> Waxing Gibbous & Waning Crescent: Difficulty 12
>
> Half Moon: Difficulty 8
>
> Waxing Crescent & Waning Gibbous: Difficulty 3
>
>**Duration:** 24 hours
>
>**Target:** Self
> 
>**Effect:** Choose either your base form or your Were-[species] form, and you may change into it. 

## Were-[species] Skill 7 - Infect Bite
**Category:** Magic, Ability Skill

**Point Cost:** 7

**Prerequisite:**  7 levels in Were-[Species]

**Effect:** When you bite (or otherwise partially consume, depending on the [speices]) another creature, you may choose one of the following: 

- They automatically unlock the were-[species] skill and they gain one level in were-[species] without needing to spend the character points to do so. If they don't wish to change, they must successfully contest their Toughness against your were-[speices] level

- The bite doesn't meet the pre-requisite for them to unlock the were-[speices] skill. 

- Do neither

## Were-[species] Skill 10 - Half Moon
**Category:** Magic, Ability Skill

**Point Cost:** 10

**Prerequisite:**  10 levels in Were-[Species], Sanity 1

**Action:**

> #### Half Moon
>
>**Preparation:** Fatigue Were-[species], 1 minute
>
>**Check:** Were-[species] vs Phase of moon:
>
> New Moon & Full Moon: Difficulty 30
>
> Waxing Gibbous & Waning Crescent: Difficulty 12
>
> Half Moon: Difficulty 8
>
> Waxing Crescent & Waning Gibbous: Difficulty 3
>
>**Duration:** 24 hours
>
>**Target:** Self
> 
>**Effect:** You may transform into a hybrid of your two [species] forms. All numerical stats between the two are averaged. Any stat exclusive to one form is halved.

## Were-[species] Skill 15 - Moon Phasing
**Category:** Magic, Ability Skill

**Point Cost:** 15

**Prerequisite:**  15 levels in Were-[Species], Sanity 1

**Effect:** If you use Were-[species] Action with a 1 minute preparation time, you may ignore the preparation time, and transform instantaneously. 

## Were-[species] Skill 16 - Moon Waning
**Category:** Magic, Ability Skill

**Point Cost:** 16

**Prerequisite:**  16 levels in Were-[Species], Sanity 1

**Effect:** If you use Were-[species] Action that requires a check against a moon phase, you auto-pass the check. 

## Were-[species] Skill 18 - Silvered
**Category:** Magic, Ability Skill

**Point Cost:** 18

**Prerequisite:**  18 levels in Were-[Species], Sanity 2

**Effect:** Your fur/scales/etc. turns silver, and you gain bonuses for killing vampires. 

| **Vampires**             | **Benefit**                                                                                                                                                                                                                                                                                                                                                                                                                                                              |
|--------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 1                        | You do not age                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| 10                       | You gain a +5 to all rolls if you have killed a vampire in the past month.                                                                                                                                                                                                                                                                                                                                                                                               |
| 25                       | Unlock Shadow Jump:  If you step on a shadow, as a 1 meter move action, you may teleport to another shadow in sight within  a hundred meters                                                                                                                                                                                                                                                                                                                             |
| 35                       | You gain the awakened magic aspect for free                                                                                                                                                                                                                                                                                                                                                                                                                              |
| 50                       | Unlock Silver Aura:   Your aura is poisonous to the undead. An undead  creature touching you automatically takes your were-[species] level in Spirit damage.                                                                                                                                                                                                                                                                                                             |
| 75                       | For every 10 level in Were-[species], you may add to up to 1 additional meter to your height when you transform into your were-[species] or hybrid form. For each meter you add, take a -1 to flexability and a +1 to might                                                                                                                                                                                                                                              |
| 100 and  every 100 after | Unlock split tail:  Every time you unlock this ability, you grow an extra tail. Each tail grants you a reserve of health of a 2nd tier ability of your choice.  If you completely deplete the health of a 2nd tier ability, before it rolls over to being damage, it depletes the health of one of the tails. Tail health does not restore when you otherwise restore health nor regenerate. When a tail grows, you must decide then what health stat the tail supports. |

## Were-[species] Skill 20 - Waxing Moon
**Category:** Magic, Ability Skill

**Point Cost:** 20

**Prerequisite:**  20 levels in Were-[Species], Sanity 1

**Action:**

> #### Waxing Moon
>
>**Preparation:** Fatigue Were-[species]
>
>**Duration:** 24 hours
>
>**Target:** Self
> 
>**Effect:** Between your were form and your base form, your inactive form. If you are in a hybrid form, you may add together bonus stats instead of averaging them. 


## Were-[species] Skill 25 - Multi-Were
**Category:** Magic, Ability Skill

**Point Cost:** 25

**Prerequisite:**  25 levels in Were-[Species], At least 5 levels in a were-[different speices] skill

**Effect:** Your hybrid form doesn't have to be between Were-[Species] and your base species, you can do it between [Species] and [different species] as well. Further, you may use any aspects you have for Were-[Species] applying to [different species] as well. (Note, this does not increase the number of tails for a silvered form). 

## Were-[species] Skill 30 - Greater Multi-Were
**Category:** Magic, Ability Skill

**Point Cost:** 30

**Prerequisite:**  30 levels in Were-[Species], At least 5 levels in a were-[different speices] skill, any Were-[any species] Skill 25 - Multi-Were aspect.

**Effect:** You may add another species to your avaialble Were- forms without having to add another skill. From this point on, each additional species only requires meeting the requirement for gaining that were-[new species] skill,
and spending 5 character points; at which point you may treat it as if you have were-[new species] at level equal to your any of your Were-[any species] skills & aspects.

## Were-[species] Skill 40 - Spawning
**Category:** Magic, Ability Skill

**Point Cost:** 40

**Prerequisite:**  40 levels in Were-[Species]

**Effect:** When you transform into [Species] or your hybrid form, you spawn a number of [species] around you equal to 1/5th of your Were-[species] level (rounded down). These obey your command. When you leave your [species] form or hybrid form, they disappear. 
