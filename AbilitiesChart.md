# 1st Tier Ability
| **Name** | **Short Name** | **Can Take Damage** |  **Can Be Fatigued** | **Derived From** |***Source*** |
|:--- |:---: | :---: | :---: | :---: | --- |
|Being| Being | Yes | Yes | 1/2 Body + 1/2 Mind | Full Player's Guide

# 2nd Tier Abilities
| **Name** | **Short Name** | **Can Take Damage** |  **Can Be Fatigued** | **Derived From** | ***Source*** |
|:--- |:---: | :---: | :---: |:---: | ---: |
|Body| Body | Yes | Yes | 1/4 Spe + 1/4 Fin + 1/4 Mig + 1/4 Tou | Full Player's Guide
|Mind| Mind | Yes | Yes | 1/4 IQ + 1/4 Cre + 1/4 Mem + 1/4 San | Full Player's Guide

# 3rd Tier Abilities 
| **Name** | **Short Name** | **Can Take Damage** |  **Can Be Fatigued** | **Derived From** |***Source*** |
|:--- |:---: | :---: | :---: |:---: | ---: |
|Speed| Spe | No | Yes | 1/4 of all Speed Designated Skill Points | Full Player's Guide
|Finesse| Fin | No | Yes | 1/4 of all Finesse Designated Skill Points | Full Player's Guide
|Might| Mig | No | Yes | 1/4 of all Might Designated Skill Points | Full Player's Guide
|Toughness| Tou | No | Yes | 1/4 of all Toughness Designated Skill Points | Full Player's Guide
|IQ| IQ | No | Yes | 1/4 of all IQ Designated Skill Points | Full Player's Guide
|Creativity|Cre | No | Yes | 1/4 of all Creativity Designated Skill Points | Full Player's Guide
|Memory| Mem | No | Yes | 1/4 of all Memory Designated Skill Points | Full Player's Guide
|Sanity| San | No | Yes | 1/4 of all Sanity Designated Skill Points | Full Player's Guide




# 4th Tier Abilities: Skills

| **Skill** | ***Spe*** | ***Fin*** | ***Mig*** | ***Tou*** | ***IQ*** | ***Cre*** | ***Mem*** | ***San*** | ***Source*** |
| :----- | :---: | :---: | :---: | :---: | :---:| :---: | :---: | :---: | -----: |
| Accuracy | | x|  |  | |  |  |  | Full Player's Guide |
| Archaeology | | |  |  | |  | x | x | Full Player's Guide |
| Argue | | |  |  | | x |  | x | Full Player's Guide |
| Armor | | | x | x | |  |  |  | Full Player's Guide |
| Arms [Fast] | x| |  |  | |  |  |  | Full Player's Guide |
| Arms [Heavy] | | |  | x | |  |  |  | Full Player's Guide |
| Arms [Melee] | | | x |  | |  |  |  | Full Player's Guide |
| Arms [Ranged] | |x |  |  | |  |  |  | Full Player's Guide |
| Artistry | | |  |  | | x |  |  | Full Player's Guide |
| Conversation | | |  |  | |  | x | x | Full Player's Guide |
| Cooking | | |  |  | | x |  |  | Full Player's Guide |
| Craft | | |  |  | x|  |  |  | Full Player's Guide |
| Dance | | x|  |  | | x |  |  | Full Player's Guide |
| Debate | | |  |  | | x | x |  | Full Player's Guide |
| Deduction | | |  |  | |  | x | x | Full Player's Guide |
| Deflection |x | x|x  |  | |  |  |  | Full Player's Guide |
| Design | | |  |  | x|  x|  |  | Full Player's Guide |
| Drive [Vehicle Category] | x| |  |  | |  |  | x | Full Player's Guide |
| Endurance | | |  | x | |  |  |  | Full Player's Guide |
| Flexibility | |x |  |  | |  |  |  | Full Player's Guide |
| Handling | | | x |  | |  | x |  | Full Player's Guide |
| Hold | | | x |  | |  |  |  | Full Player's Guide |
| Investigate | | |  |  | x |  |  | x | Full Player's Guide |
| Martial Arts |x | x | x |  | |  |  |  | Full Player's Guide |
| Mathematics | | |  |  |x |  | x |  | Full Player's Guide |
| Morality | | |  |  | |  | x | x | Full Player's Guide |
| Music | x |x |  |  | | x |  |  | Full Player's Guide |
| Observe [Sense] | | x|  |  |x |  |  |  | Full Player's Guide |
| Personality | | |  |  | | x | x | x | Full Player's Guide |
| Procedure | | |  |  | |  | x | x | Full Player's Guide |
| Regeneration | | |  | x | |  |  |  | Full Player's Guide |
| Resolution | | |  |  | |  |  | x | Full Player's Guide |
| Rush | x | | x |  | |  |  |  | Full Player's Guide |
| Sneak | | x |  |  | x |  |  |  | Full Player's Guide |
| Sprint |x | |  |  | |  |  |  | Full Player's Guide |
| Survival | | |  | x | |  |  | x | Full Player's Guide |
| Tactics | | |  |  | x | x |  |  | Full Player's Guide |
| Throw | | x | x |  | |  |  |  | Full Player's Guide |
| Treatment | | x |  | x | x |  |  |  | Full Player's Guide |
| Travel | x | |  | x  | |  |  |  | Full Player's Guide |
| Vigilance | | |  | x  | x |  |  |  | Full Player's Guide |
| Wellness | | |  | x | |  |  |  | Full Player's Guide |
| Wisdom | | |  |  | | x | x |  | Full Player's Guide |

# 5th+ Tier Abilities: Specializations

| **Specialization** | **Tier**| ***Parent Abilties*** | ***Source*** |
| :--- | :---: | :---: | ---: |
|Beauty | 5th | Finesse, Wellness | Full Player's Guide |
|Cartography | 5th | Accuracy, Survival | Full Player's Guide |
|Exercise | 5th | Travel, Martial Arts, Endurance | Full Player's Guide |
|Forage | 5th | Observe, Survival | Full Player's Guide |
|Grapple | 5th | Martial Arts, Endurance, Hold | Full Player's Guide |
|Identification | 5th | Observe, Memory | Full Player's Guide |
|Infiltrate | 5th | Sneak, Tactics | Full Player's Guide |
|Inspire | 5th | Conversation, Creativity | Full Player's Guide |
|Learn | 5th | Argue, Deduction, Mathematics | Full Player's Guide |
|Locksmith | 6th | Infiltrate, Sneak | Full Player's Guide |
|Philosophy | 5th | Deduction, Morality | Full Player's Guide |
|Politics | 6th | Argue, Conversation, Inspire | Full Player's Guide |
|Romance | 6th | Beauty, Conversation | Full Player's Guide |
|Teach | 5th | Argue, Resolution, Wisdom | Full Player's Guide |
|Track | 5th | Survival | Full Player's Guide |
|Train | 5th | Conversation, Survival | Full Player's Guide |
|Weapon Mastery | 5th | Arms [Weapon Category] | Full Player's Guide |